//Link to Boost
#define BOOST_TEST_DYN_LINK

//Define our Module name (prints at testing)
#define BOOST_TEST_MODULE "Steamcalc"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
