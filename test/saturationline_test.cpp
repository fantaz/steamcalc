// //Link to Boost
// #define BOOST_TEST_DYN_LINK
// 
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
//
#include "test.h"
#include "regions/saturationline.h"

const double err {
	1e-06
}; // absolute error used for tests

BOOST_AUTO_TEST_SUITE(SaturationLine)
BOOST_AUTO_TEST_CASE( saturationline_test )
{
	steamcalc::SaturationLine sl;
	steamcalc::units::Pressure p1(0.1*mega*pascal);
	steamcalc::units::Pressure p2(1*mega*pascal);
	steamcalc::units::Pressure p3(10*mega*pascal);
	steamcalc::units::Pressure _p1(0.353658941e-2*mega*pascal);
	steamcalc::units::Pressure _p2(0.263889776e1*mega*pascal);
	steamcalc::units::Pressure _p3(0.123443146e2*mega*pascal);
	steamcalc::units::Temperature T1(300*kelvin);
	steamcalc::units::Temperature T2(500*kelvin);
	steamcalc::units::Temperature T3(600*kelvin);
	steamcalc::units::Temperature _T1(0.372755919e3*kelvin);
	steamcalc::units::Temperature _T2(0.453035632e3*kelvin);
	steamcalc::units::Temperature _T3(0.584149488e3*kelvin);
	
	//Test p(T)
	BOOST_REQUIRE_CLOSE( _p1.value() , sl(T1).value(), err );
	BOOST_REQUIRE_CLOSE( _p2.value(), sl(T2).value(), err );
	BOOST_REQUIRE_CLOSE( _p3.value(), sl(T3).value(), err );
	
	//Testing T(p)
	BOOST_REQUIRE_CLOSE( _T1.value(), sl(p1).value(), err );
	BOOST_REQUIRE_CLOSE( _T2.value(), sl(p2).value(), err );
	BOOST_REQUIRE_CLOSE( _T3.value(), sl(p3).value(), err );
}
BOOST_AUTO_TEST_SUITE_END()
