// //Link to Boost
// #define BOOST_TEST_DYN_LINK
// 
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "b23.h"

BOOST_AUTO_TEST_SUITE(B23)
BOOST_AUTO_TEST_CASE( TP_test )
{
    steamcalc::B23 b23_test;
    steamcalc::units::Pressure p(0.165291643e2*mega*pascal);
	steamcalc::units::Temperature T(623.15*kelvin);
	steamcalc::units::Pressure p_result = b23_test(T);
	steamcalc::units::Temperature T_result = b23_test(p);

	
	BOOST_REQUIRE_CLOSE_FRACTION( p.value() , p_result.value() , std_err );
	BOOST_REQUIRE_CLOSE_FRACTION( T.value(), T_result.value(), std_err );
}
BOOST_AUTO_TEST_SUITE_END()
