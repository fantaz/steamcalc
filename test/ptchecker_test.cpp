// //Link to Boost
// #define BOOST_TEST_DYN_LINK
// 
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "ptchecker.h"
#include "regions/saturationline.h"
#include "regions/one.h"
#include "regions/two.h"
#include "regions/three.h"
#include "regions/five.h"

// Dummy test function checkRegion
// Testing true when correct region found, else false if no region found
enum Region {
	One = 1,
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	None = 0
};

int  checkRegion(const steamcalc::units::Pressure& _pressure, const steamcalc::units::Temperature& _temperature){
	if (steamcalc::PTChecker<steamcalc::One>(_temperature).check(_pressure,_temperature))
		return Region::One;
	if (steamcalc::PTChecker<steamcalc::Two>(_temperature).check(_pressure,_temperature))
		return Region::Two;
    if (steamcalc::PTChecker<steamcalc::Three>(_temperature, _pressure).check(_pressure, _temperature))
		return Region::Three;
	if (steamcalc::PTChecker<steamcalc::Four>().check(_pressure,_temperature))
		return Region::Four;
	if (steamcalc::PTChecker<steamcalc::Five>().check(_pressure, _temperature))
		return Region::Five;
	return Region::None;
}

// Set of test quantities
const steamcalc::units::Pressure p_none(0*pascal);
const steamcalc::units::Pressure p1(10*1e6*pascal);
const steamcalc::units::Pressure p2_a(0.0035*1e6*pascal);//0<p2_a<=p_saturation(T)
const steamcalc::units::Pressure p2_b(13.823587*1e6*pascal);//0<p2_b<=p_b23(T)
const steamcalc::units::Pressure p2_c(97.555512*1e6*pascal);// 0< p < 100Mpa
const steamcalc::units::Pressure p3(50*1e6*pascal);
const steamcalc::units::Pressure p4(0.263889776e1*1e6*pascal);
const steamcalc::units::Pressure p5(25*1e6*pascal);
const steamcalc::units::Temperature T_none(0*kelvin);
const steamcalc::units::Temperature T1(300*kelvin);
const steamcalc::units::Temperature T2_a(350*kelvin);//273.15K ≤ T ≤ 623.15K
const steamcalc::units::Temperature T2_b(650*kelvin);//273.15K < T ≤ 863.15K
const steamcalc::units::Temperature T2_c(1000*kelvin);//863.15K < T ≤ 1073.15K
const steamcalc::units::Temperature T3(650*kelvin);
const steamcalc::units::Temperature T4(500*kelvin);//273.15K<=T<=647.096K
const steamcalc::units::Temperature T5(2000*kelvin);//1073.15K<=T<=2273.15K


BOOST_AUTO_TEST_SUITE(PTChecker)
BOOST_AUTO_TEST_CASE( Test_Region_One )
{

	BOOST_CHECK(checkRegion(p1,T1)==Region::One);
	BOOST_CHECK(checkRegion(p2_a,T2_c)!=Region::One);
	
}
BOOST_AUTO_TEST_CASE( Test_Region_Two )
{
	
	BOOST_CHECK(checkRegion(p2_a,T2_a)==Region::Two);
	BOOST_CHECK(checkRegion(p2_b,T2_b)==Region::Two);
	BOOST_CHECK(checkRegion(p2_c,T2_c)==Region::Two);
	BOOST_CHECK(checkRegion(p3,T5)!=Region::Two);
	
}
BOOST_AUTO_TEST_CASE( Test_Region_Three )
{

	BOOST_CHECK(checkRegion(p3,T3)==Region::Three);
	BOOST_CHECK(checkRegion(p2_b,T2_b)!=Region::Three);
	
}
BOOST_AUTO_TEST_CASE( Test_Region_Four )
{
	// Not important for now
//  	BOOST_CHECK(checkRegion(p4,steamcalc::SaturationLine().backward(p4)) == Region::Four);
//     BOOST_CHECK(checkRegion(steamcalc::SaturationLine().basic(T1), T1)!=Region::Four);
	
}
BOOST_AUTO_TEST_CASE( Test_Region_Five )
{
	
	BOOST_CHECK(checkRegion(p5,T5)==Region::Five);
	BOOST_CHECK(checkRegion(p2_a,T2_c)!=Region::Five);
	
}
BOOST_AUTO_TEST_CASE( Test_Region_None )
{

	BOOST_CHECK(checkRegion(p_none,T_none)!=Region::One);
	BOOST_CHECK(checkRegion(p_none,T_none)!=Region::Two);
	BOOST_CHECK(checkRegion(p_none,T_none)!=Region::Three);
	BOOST_CHECK(checkRegion(p_none,T_none)!=Region::Four);
	BOOST_CHECK(checkRegion(p_none,T_none)!=Region::Five);
	
}
BOOST_AUTO_TEST_SUITE_END()
