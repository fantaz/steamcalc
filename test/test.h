#ifndef TEST_TEST_H
#define TEST_TEST_H

#include <boost/test/floating_point_comparison.hpp>
#include <boost/iterator/iterator_concepts.hpp>

const double std_err {
    1e-6
  };                                                          // absolute error used for tests

const double backward_err {
    1e-3
  }; 

#endif