/*
 * Using BOOST TEST TOOLS
 * Test for region three
 */
// //Link to Boost
// #define BOOST_TEST_DYN_LINK
//
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "steamcalc.h"
#include "backwarder.h"
#include "regions/three.h"

const double err {
    1e-6
};// absolute error used for tests

steamcalc::units::Temperature T3_1 ( 650 * kelvin ), T3_2 ( 750*kelvin );
steamcalc::units::MassDensity ro3_1 ( 500 * kilogram_per_cubic_meter ), ro3_2 ( 200 * kilogram_per_cubic_meter );

const steamcalc::units::Pressure
p_a ( 0.255837018e2 * mega * pascal ),
p_b ( 0.222930643e2  * mega * pascal ),
p_c ( 0.783095639e2 * mega * pascal );

const steamcalc::units::SpecificEnergy
h_a ( 0.186343019e4 * kilo * joule_per_kilogram ),
h_b ( 0.237512401e4 * kilo * joule_per_kilogram ),
h_c ( 0.225868845e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificEnergy
u_a ( 0.181226279e4 * kilo * joule_per_kilogram ),
u_b ( 0.226365868e4 * kilo * joule_per_kilogram ),
u_c ( 0.210206932e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificHeatCapacity
s_a ( 0.405427273e1 * kilo * Joule_per_kilogram_and_kelvin ),
s_b ( 0.485438792e1 * kilo * Joule_per_kilogram_and_kelvin ),
s_c ( 0.446971906e1 *  kilo * Joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificHeatCapacity
cp_a ( 0.138935717e2 * kilo * joule_per_kilogram_and_kelvin ),
cp_b ( 0.446579342e2 * kilo * joule_per_kilogram_and_kelvin ),
cp_c ( 0.634165359e1 * kilo * joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificHeatCapacity
cv_a ( 0 * joule_per_kilogram_and_kelvin ),
cv_b ( 0 * joule_per_kilogram_and_kelvin ),
cv_c ( 0 * joule_per_kilogram_and_kelvin );

const steamcalc::units::Velocity
w_a ( 0.502005554e3 * meter_per_second ),
w_b ( 0.383444594e3 * meter_per_second ),
w_c ( 0.760696041e3 * meter_per_second );


BOOST_AUTO_TEST_SUITE ( RegionThree )
BOOST_AUTO_TEST_CASE ( BasicTestThree ) {
    
    steamcalc::Steamcalc r_test_a ( ro3_1, T3_1 ),  r_test_b ( ro3_2, T3_1 ),  r_test_c ( ro3_1, T3_2 );

    double pressure_a = p_a.value();
    double pressure_b = p_b.value();
    double pressure_c = p_c.value();
    double pressure_test_a = r_test_a.pressure().value();
    double pressure_test_b = r_test_b.pressure().value();
    double pressure_test_c = r_test_c.pressure().value();
    
    double spec_enthalpy_a = h_a.value();
    double spec_enthalpy_b = h_b.value();
    double spec_enthalpy_c = h_c.value();
    double spec_enthalpy_test_a = r_test_a.specificEnthalpy().value();
    double spec_enthalpy_test_b = r_test_b.specificEnthalpy().value();
    double spec_enthalpy_test_c = r_test_c.specificEnthalpy().value();

    double spec_entropy_a = s_a.value();
    double spec_entropy_b = s_b.value();
    double spec_entropy_c = s_c.value();
    double spec_entropy_test_a = r_test_a.specificEntropy().value();
    double spec_entropy_test_b = r_test_b.specificEntropy().value();
    double spec_entropy_test_c = r_test_c.specificEntropy().value();

    double spec_intern_energ_a = u_a.value();
    double spec_intern_energ_b = u_b.value();
    double spec_intern_energ_c = u_c.value();
    double spec_intern_energ_test_a = r_test_a.specificInternalEnergy().value();
    double spec_intern_energ_test_b = r_test_b.specificInternalEnergy().value();
    double spec_intern_energ_test_c = r_test_c.specificInternalEnergy().value();

    double spec_isobaric_heat_cap_a = cp_a.value();
    double spec_isobaric_heat_cap_b = cp_b.value();
    double spec_isobaric_heat_cap_c = cp_c.value();
    double spec_isobaric_heat_cap_test_a = r_test_a.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_b = r_test_b.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_c = r_test_c.specificIsobaricHeatCapacity().value();

    double speed_of_sound_a = w_a.value();
    double speed_of_sound_b = w_b.value();
    double speed_of_sound_c = w_c.value();
    double speed_of_sound_test_a = r_test_a.speedOfSound().value();
    double speed_of_sound_test_b = r_test_b.speedOfSound().value();
    double speed_of_sound_test_c = r_test_c.speedOfSound().value();

//     double spec_volume_a = v_a.value();
//     double spec_volume_b = v_b.value();
//     double spec_volume_c = v_c.value();
//     double spec_volume_test_a = r_test_a.specificVolume().value();
//     double spec_volume_test_b = r_test_b.specificVolume().value();
//     double spec_volume_test_c = r_test_c.specificVolume().value();

    // First set
    BOOST_REQUIRE_CLOSE ( pressure_a , pressure_test_a, err );
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_a , spec_enthalpy_test_a, err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_a, spec_entropy_test_a, err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_a , spec_intern_energ_test_a, err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_a , spec_isobaric_heat_cap_test_a, err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_a , speed_of_sound_test_a, err );
//     BOOST_REQUIRE_CLOSE ( spec_volume_a , spec_volume_test_a, err );

    // Second set
    BOOST_REQUIRE_CLOSE ( pressure_b , pressure_test_b, err );
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_b, spec_enthalpy_test_b, err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_b, spec_entropy_test_b, err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_b , spec_intern_energ_test_b, err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_b , spec_isobaric_heat_cap_test_b, err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_b , speed_of_sound_test_b, err );
//     BOOST_REQUIRE_CLOSE ( spec_volume_b, spec_volume_test_b, err );

    // Last set
    BOOST_REQUIRE_CLOSE ( pressure_c, pressure_test_c, err );
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_c, spec_enthalpy_test_c, err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_c, spec_entropy_test_c, err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_c, spec_intern_energ_test_c, err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_c, spec_isobaric_heat_cap_test_c, err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_c, speed_of_sound_test_c, err );
//     BOOST_REQUIRE_CLOSE ( spec_volume_c, spec_volume_test_c, err );
}

BOOST_AUTO_TEST_CASE ( pT_TestThree ) {
    steamcalc::Backwarder <steamcalc::Three, steamcalc::Property::Temperature, steamcalc::Property::Pressure, 0, steamcalc::DensityBackwarder> r_test_pt_a(T3_1, p_a);
    
    double density_a = ro3_1.value();
    double density_test_a = r_test_pt_a.backward().value();
    
    BOOST_REQUIRE_CLOSE ( density_a, density_test_a, err );

}
BOOST_AUTO_TEST_SUITE_END()
