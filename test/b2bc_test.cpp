// //Link to Boost
// #define BOOST_TEST_DYN_LINK
// 
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "b2bc.h"

BOOST_AUTO_TEST_SUITE(B2bc)
BOOST_AUTO_TEST_CASE( ph_test )
{
    steamcalc::B2BC b2bc_test;
    steamcalc::units::Pressure p(100*mega*pascal);
    steamcalc::units::SpecificEnergy h( 0.3516004323e4*kilo*joules_per_kilogram);
    steamcalc::units::Pressure p_result = b2bc_test(h);
    steamcalc::units::SpecificEnergy h_result = b2bc_test(p);


    BOOST_REQUIRE_CLOSE_FRACTION( p.value() , p_result.value() , std_err );
    BOOST_REQUIRE_CLOSE_FRACTION( h.value(), h_result.value(), std_err );
}
BOOST_AUTO_TEST_SUITE_END()
