/*
 * Using BOOST TEST TOOLS
 * Test for region one
 */
// //Link to Boost
// #define BOOST_TEST_DYN_LINK
//
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "region.h"
#include <steamproperties.h>
#include "regions/one.h"
#include <ptchecker.h>
#include "boost/date_time.hpp"
#include "boost/math/tools/minima.hpp"
#include <boost/math/tools/roots.hpp>
#include "backwarder.h"
#include "temperaturebackwarder.h"
#include "steamcalc.h"

const steamcalc::units::Temperature t1 ( 300 * kelvin ), t2 ( 300 * kelvin ), t3 ( 500 * kelvin );
const steamcalc::units::Pressure p1 ( 3 * mega*pascal ), p2 ( 80 * mega * pascal ), p3 ( 3 * mega * pascal );
const steamcalc::units::SpecificEnergy h1 ( 500 * kilo * joule_per_kilogram ), h2 ( 1500 * kilo * joule_per_kilogram );
const steamcalc::units::SpecificHeatCapacity s1 ( 0.5*kilo*joule_per_kilogram_and_kelvin ),s2 ( 3*kilo*joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificEnergy h1_gpT ( 0.115331273e3 * kilo * joule_per_kilogram ), h2_gpT ( 0.184142828e3 * kilo * joule_per_kilogram ), h3_gpT ( 0.975542239e3 * kilo * joule_per_kilogram );
const steamcalc::units::SpecificEnergy u1_gpT ( 0.112324818e3 * kilo * joule_per_kilogram ), u2_gpT ( 0.106448356e3 * kilo * joule_per_kilogram ), u3_gpT ( 0.971934985e3 * kilo * joule_per_kilogram );
const steamcalc::units::SpecificHeatCapacity s1_gpT ( 0.392294792 * kilo * Joule_per_kilogram_and_kelvin ), s2_gpT ( 0.368563852 * kilo * Joule_per_kilogram_and_kelvin ), s3_gpT ( 0.258041912e1 *  kilo * Joule_per_kilogram_and_kelvin );
const steamcalc::units::SpecificVolume v1_gpT ( 0.100215168e-2 * cubic_meter_per_kilogram ), v2_gpT ( 0.971180894e-3 * cubic_meter_per_kilogram ), v3_gpT ( 0.120241800e-2 * cubic_meter_per_kilogram );
const steamcalc::units::SpecificHeatCapacity cp1_gpT ( 0.417301218e1 * kilo * joule_per_kilogram_and_kelvin ), cp2_gpT ( 0.401008987e1 * kilo * joule_per_kilogram_and_kelvin ), cp3_gpT ( 0.465580682e1 * kilo * joule_per_kilogram_and_kelvin );
const steamcalc::units::SpecificHeatCapacity cv1_gpT ( 0 * joule_per_kilogram_and_kelvin ), cv2_gpT ( 0 * joule_per_kilogram_and_kelvin ), cv3_gpT ( 0 * joule_per_kilogram_and_kelvin );
const steamcalc::units::Velocity w1_gpT ( 0.150773921e4 * meter_per_second ), w2_gpT ( 0.163469054e4 * meter_per_second ), w3_gpT ( 0.124071337e4 * meter_per_second );

const steamcalc::units::Temperature t1_tph ( 0.391798509e3 * kelvin ), t2_tph ( 0.378108626e3 * kelvin ), t3_tph ( 0.611041229e3 * kelvin );

const steamcalc::units::Temperature t1_tps ( 307.842258*kelvin ),t2_tps ( 309.979785*kelvin ),t3_tps ( 565.899909*kelvin );

BOOST_AUTO_TEST_SUITE ( RegionOne )
BOOST_AUTO_TEST_CASE ( BasicTest ) {
    steamcalc::Region<steamcalc::One> r1(p1, t1);

    steamcalc::SteamProperties steam_data;

    steamcalc::One one ( p1,t1 );

    std::unique_ptr<steamcalc::SteamProperties> data = r1.properties ();

// 	/**************************************************************************/
// 	std::cout<< "********************************************************************"
// 	<< std::endl
// 	<< "Test Region One:" <<std::endl
// 	<< "Minimal sizeofs of used objects in this test: " << std::endl;
// 	unsigned int i = 0;
// 	std::cout<< "- Region: " << sizeof(r1) <<" bytes" <<std::endl;
// 	i += sizeof(r1);
// 	std::cout<< "- unique_ptr to SteamProperties: " << sizeof(data) <<" bytes" <<std::endl;
// 	i += sizeof(data);
// 	std::cout<< "- SteamProperties: " << sizeof(steam_data) <<" bytes" <<std::endl;
// 	i += sizeof(steam_data);
// 	std::cout<< "- One: " << sizeof(one) <<" bytes" <<std::endl;
// 	i += sizeof(one);
// 	std::cout<< "Total: " << i <<" bytes" <<std::endl;
// 	std::cout<< "********************************************************************" << std::endl;
// 
// 	boost::posix_time::ptime time_start(boost::posix_time::microsec_clock::local_time());
// 
// 	std::vector<std::unique_ptr<steamcalc::SteamProperties>> vec_r1;
// 	unsigned int t = 0;
// 	while (t++ < size_t(1000000)){
//      vec_r1.push_back(steamcalc::Region<steamcalc::One>(p1,t1).properties());
// 	}
// 
// 	boost::posix_time::ptime time_end(boost::posix_time::microsec_clock::local_time());
// 	boost::posix_time::time_duration duration(time_end - time_start);
// 	std::cout
// 	<<"Creation of 100 000 regions one: "<< duration <<"h:m:s"<< std::endl;
// 	std::cout<< "********************************************************************" << std::endl;

    double spec_enthalpy = h1_gpT.value();
    double spec_enthalpy_test = data->m_spec_enthalpy.value();

    double spec_entropy = s1_gpT.value();
    double spec_entropy_test = data->m_spec_entropy.value();

    double spec_intern_energ = u1_gpT.value();
    double spec_intern_energ_test = data->m_spec_internal_energy.value();

    double spec_isobaric_heat_cap = cp1_gpT.value();
    double spec_isobaric_heat_cap_test = data->m_spec_isobaric_heat_cap.value();

    double speed_of_sound = w1_gpT.value();
    double speed_of_sound_test = data->m_speed_of_sound.value();

    double spec_volume = v1_gpT.value();
    double spec_volume_test = data->m_spec_volume.value();

    BOOST_REQUIRE_CLOSE ( spec_enthalpy , spec_enthalpy_test, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy, spec_entropy_test, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ , spec_intern_energ_test, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap , spec_isobaric_heat_cap_test, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound , speed_of_sound_test, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume , spec_volume_test, std_err );
}

BOOST_AUTO_TEST_CASE ( Tph ) {
    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEnthalpy, 0, steamcalc::TemperatureBackwarder > ( p1,h1 ).backward();
    t2_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEnthalpy, 0, steamcalc::TemperatureBackwarder > ( p2,h1 ).backward();
    t3_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEnthalpy, 0, steamcalc::TemperatureBackwarder > ( p2,h2 ).backward();
    double temp1 = t1_tph.value();
    double temp1_test = t1_test.value();
    double temp2 = t2_tph.value();
    double temp2_test = t2_test.value();
    double temp3 = t3_tph.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, backward_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, backward_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, backward_err );
}

BOOST_AUTO_TEST_CASE ( Tps ) {
    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEntropy, 0, steamcalc::TemperatureBackwarder > ( p1,s1 ).backward();
    t2_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEntropy, 0, steamcalc::TemperatureBackwarder > ( p2,s1 ).backward();
    t3_test = steamcalc::Backwarder<steamcalc::One, steamcalc::Property::Pressure, steamcalc::Property::SpecificEntropy, 0, steamcalc::TemperatureBackwarder > ( p2,s2 ).backward();
    double temp1 = t1_tps.value();
    double temp1_test = t1_test.value();
    double temp2 = t2_tps.value();
    double temp2_test = t2_test.value();
    double temp3 = t3_tps.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, backward_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, backward_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, backward_err );
}

BOOST_AUTO_TEST_CASE ( Tpv ) {
    steamcalc::Steamcalc tpv_a(p1,  v1_gpT);
    
    BOOST_REQUIRE_CLOSE ( t1.value() , tpv_a.temperature().value(), backward_err );
}

BOOST_AUTO_TEST_CASE ( pTs ) {
    steamcalc::Steamcalc pts_a(t1,  s1_gpT);

    BOOST_REQUIRE_CLOSE ( p1.value() , pts_a.pressure().value(), backward_err );
  }
  
BOOST_AUTO_TEST_CASE ( pTh ) {
    steamcalc::Steamcalc pth_a(t1,  h1_gpT);

    BOOST_REQUIRE_CLOSE ( p1.value() , pth_a.pressure().value(), backward_err );
  }
  
BOOST_AUTO_TEST_CASE ( pTu ) {
    steamcalc::Steamcalc ptu_a( u1_gpT, t1);

    BOOST_REQUIRE_CLOSE ( p1.value() , ptu_a.pressure().value(), backward_err );
  }
  
BOOST_AUTO_TEST_CASE ( pTw ) {
    steamcalc::Steamcalc ptw_a(t1,  w1_gpT);

    BOOST_REQUIRE_CLOSE ( p1.value() , ptw_a.pressure().value(), backward_err );
  }

BOOST_AUTO_TEST_CASE ( Tpv_toms_748 ) {
	
	//Ovako cemo do granica
	steamcalc::PTChecker<steamcalc::One> pt ( 0 ); //t_min ( 273.15*kelvin ),t_max ( 623.15*kelvin );
	boost::math::tools::eps_tolerance<double> tol ( 30 );
	boost::uintmax_t max_iter=500;
	double root = steamcalc::functions::find_root(
		[&] ( const double& _temperature ) {
			steamcalc::units::SpecificVolume v = steamcalc::One ( 
				p1, 
				steamcalc::units::Temperature ( _temperature*kelvin ) 
			).specVolume();
			return v1_gpT.value() - v.value();
		},
		pt.getTemperatureBounds().first.value(), // minimalna vrijednost
		pt.getTemperatureBounds().second.value(), //maksimalna vrijednost
		tol,
		max_iter
		); // tolerancija
		
    std::cout<<std::endl<<"Using Toms 748 method to find function root:" <<std::endl
    << "v(p="<< p1 <<", T="<< t1 <<" )="
    <<  steamcalc::One ( p1,t1 ).specVolume()
    <<std::endl
    << "toms T(p="<< p1 <<",v="<< v1_gpT <<")="
    << root <<"K"
    << std::endl<<std::endl;
    BOOST_REQUIRE_CLOSE ( t1.value() , root, backward_err );
}

BOOST_AUTO_TEST_SUITE_END()
