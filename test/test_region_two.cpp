/*
 * Using BOOST TEST TOOLS
 * Test for region two
 */
// //Link to Boost
// #define BOOST_TEST_DYN_LINK
//
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "steamcalc.h"
#include "boost/date_time.hpp"
#include "boost/math/tools/minima.hpp"
#include <boost/math/tools/roots.hpp>

steamcalc::units::Temperature T1 ( 300 * kelvin ), T2 ( 700*kelvin );
steamcalc::units::Pressure p1 ( 0.0035 * mega * pascal ), p2 ( 30 * mega * pascal );

const steamcalc::units::SpecificEnergy h1 ( 500 * kilo * joule_per_kilogram ), h2 ( 1500 * kilo * joule_per_kilogram );
const steamcalc::units::SpecificHeatCapacity s1 ( 0.5*kilo*joule_per_kilogram_and_kelvin ),s2 ( 3*kilo*joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificEnergy
h_a ( 0.254991145e4 * kilo * joule_per_kilogram ),
    h_b ( 0.333568375e4 * kilo * joule_per_kilogram ),
    h_c ( 0.263149474e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificEnergy
u_a ( 0.241169160e4 * kilo * joule_per_kilogram ),
    u_b ( 0.301262819e4 * kilo * joule_per_kilogram ),
    u_c ( 0.246861076e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificHeatCapacity
s_a ( 0.852238967e1 * kilo * Joule_per_kilogram_and_kelvin ),
    s_b ( 0.101749996e2 * kilo * Joule_per_kilogram_and_kelvin ),
    s_c ( 0.517540298e1 *  kilo * Joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificVolume
v_a ( 0.394913866e2 * cubic_meter_per_kilogram ),
    v_b ( 0.923015898e2 * cubic_meter_per_kilogram ),
    v_c ( 0.542946619e-2 * cubic_meter_per_kilogram );

const steamcalc::units::SpecificHeatCapacity
cp_a ( 0.191300162e1 * kilo * joule_per_kilogram_and_kelvin ),
     cp_b ( 0.208141274e1 * kilo * joule_per_kilogram_and_kelvin ),
     cp_c ( 0.103505092e2 * kilo * joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificHeatCapacity
cv_a ( 0 * joule_per_kilogram_and_kelvin ),
     cv_b ( 0 * joule_per_kilogram_and_kelvin ),
     cv_c ( 0 * joule_per_kilogram_and_kelvin );

const steamcalc::units::Velocity
w_a ( 0.427920172e3 * meter_per_second ),
    w_b ( 0.644289068e3 * meter_per_second ),
    w_c ( 0.480386523e3 * meter_per_second );

const steamcalc::units::Temperature
t_ph_2a_a ( 0.534433241e3 * kelvin ),
t_ph_2a_b ( 0.575373370e3 * kelvin ),
t_ph_2a_c ( 0.101077577e4 * kelvin ),
t_ph_2b_a ( 0.801299102e3 * kelvin ),
t_ph_2b_b ( 0.101531583e4 * kelvin ),
t_ph_2b_c ( 0.875279054e3 * kelvin ),
t_ph_2c_a ( 0.743056411e3 * kelvin ),
t_ph_2c_b ( 0.791137067e3 * kelvin ),
t_ph_2c_c ( 0.882756860e3 * kelvin ),
t_ps_2a_a ( 0.399517097e3 * kelvin ),
t_ps_2a_b ( 0.514127081e3 * kelvin ),
t_ps_2a_c ( 0.103984917e4 * kelvin ),
t_ps_2b_a ( 0.600484040e3 * kelvin ),
t_ps_2b_b ( 0.106495556e4 * kelvin ),
t_ps_2b_c ( 0.103801126e4 * kelvin ),
t_ps_2c_a ( 0.697992849e3 * kelvin ),
t_ps_2c_b ( 0.854011484e3 * kelvin ),
t_ps_2c_c ( 0.949017998e3 * kelvin );

const steamcalc::units::SpecificEnergy
h_ph_2a_a ( 3000 * kilo * joules_per_kilogram ),
          h_ph_2a_b ( 3000 * kilo * joules_per_kilogram ),
          h_ph_2a_c ( 4000 * kilo * joules_per_kilogram ),
          h_ph_2b_a ( 3500 * kilo * joules_per_kilogram ),
          h_ph_2b_b ( 4000 * kilo * joules_per_kilogram ),
          h_ph_2b_c ( 3500 * kilo * joules_per_kilogram ),
          h_ph_2c_a ( 2700 * kilo * joules_per_kilogram ),
          h_ph_2c_b ( 2700 * kilo * joules_per_kilogram ),
          h_ph_2c_c ( 3200 * kilo * joules_per_kilogram );

const steamcalc::units::Pressure
p_ph_2a_a ( 0.001 * mega * pascals ),
p_ph_2a_b ( 3 * mega * pascals ),
p_ph_2a_c ( 3 * mega * pascals ),
p_ph_2b_a ( 5 * mega * pascals ),
p_ph_2b_b ( 5 * mega * pascals ),
p_ph_2b_c ( 25 * mega * pascals ),
p_ph_2c_a ( 40 * mega * pascals ),
p_ph_2c_b ( 60 * mega * pascals ),
p_ph_2c_c ( 60 * mega * pascals );

const steamcalc::units::Pressure
p_ps_2a_a ( 0.1 * mega * pascals ),
          p_ps_2a_b ( 0.1 * mega * pascals ),
          p_ps_2a_c ( 2.5 * mega * pascals ),
          p_ps_2b_a ( 8 * mega * pascals ),
          p_ps_2b_b ( 8 * mega * pascals ),
          p_ps_2b_c ( 90 * mega * pascals ),
          p_ps_2c_a ( 20 * mega * pascals ),
          p_ps_2c_b ( 80 * mega * pascals ),
          p_ps_2c_c ( 80 * mega * pascals );

const steamcalc::units::SpecificHeatCapacity
s_ps_2a_a ( 7.5 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2a_b ( 8 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2a_c ( 8 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2b_a ( 6 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2b_b ( 7.5 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2b_c ( 6 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2c_a ( 5.75 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2c_b ( 5.25 * kilo * joules_per_kilogram_and_kelvin ),
          s_ps_2c_c ( 5.75 * kilo * joules_per_kilogram_and_kelvin );

BOOST_AUTO_TEST_SUITE ( RegionTwo )
BOOST_AUTO_TEST_CASE ( BasicTestTwo ) {
    steamcalc::Steamcalc r_test_a ( p1, T1 ),  r_test_b ( p1, T2 ),  r_test_c ( p2, T2 );

    double spec_enthalpy_a = h_a.value();
    double spec_enthalpy_b = h_b.value();
    double spec_enthalpy_c = h_c.value();
    double spec_enthalpy_test_a = r_test_a.specificEnthalpy().value();
    double spec_enthalpy_test_b = r_test_b.specificEnthalpy().value();
    double spec_enthalpy_test_c = r_test_c.specificEnthalpy().value();

    double spec_entropy_a = s_a.value();
    double spec_entropy_b = s_b.value();
    double spec_entropy_c = s_c.value();
    double spec_entropy_test_a = r_test_a.specificEntropy().value();
    double spec_entropy_test_b = r_test_b.specificEntropy().value();
    double spec_entropy_test_c = r_test_c.specificEntropy().value();

    double spec_intern_energ_a = u_a.value();
    double spec_intern_energ_b = u_b.value();
    double spec_intern_energ_c = u_c.value();
    double spec_intern_energ_test_a = r_test_a.specificInternalEnergy().value();
    double spec_intern_energ_test_b = r_test_b.specificInternalEnergy().value();
    double spec_intern_energ_test_c = r_test_c.specificInternalEnergy().value();

    double spec_isobaric_heat_cap_a = cp_a.value();
    double spec_isobaric_heat_cap_b = cp_b.value();
    double spec_isobaric_heat_cap_c = cp_c.value();
    double spec_isobaric_heat_cap_test_a = r_test_a.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_b = r_test_b.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_c = r_test_c.specificIsobaricHeatCapacity().value();

    double speed_of_sound_a = w_a.value();
    double speed_of_sound_b = w_b.value();
    double speed_of_sound_c = w_c.value();
    double speed_of_sound_test_a = r_test_a.speedOfSound().value();
    double speed_of_sound_test_b = r_test_b.speedOfSound().value();
    double speed_of_sound_test_c = r_test_c.speedOfSound().value();

    double spec_volume_a = v_a.value();
    double spec_volume_b = v_b.value();
    double spec_volume_c = v_c.value();
    double spec_volume_test_a = r_test_a.specificVolume().value();
    double spec_volume_test_b = r_test_b.specificVolume().value();
    double spec_volume_test_c = r_test_c.specificVolume().value();

    // First set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_a , spec_enthalpy_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_a, spec_entropy_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_a , spec_intern_energ_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_a , spec_isobaric_heat_cap_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_a , speed_of_sound_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_a , spec_volume_test_a, std_err );

    // Second set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_b, spec_enthalpy_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_b, spec_entropy_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_b , spec_intern_energ_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_b , spec_isobaric_heat_cap_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_b , speed_of_sound_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_b, spec_volume_test_b, std_err );

    // Last set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_c, spec_enthalpy_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_c, spec_entropy_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_c, spec_intern_energ_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_c, spec_isobaric_heat_cap_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_c, speed_of_sound_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_c, spec_volume_test_c, std_err );
}

BOOST_AUTO_TEST_CASE ( Tph_2a ) {
    steamcalc::units::Temperature 
    t1_test, 
    t2_test, 
    t3_test;
    t1_test = steamcalc::Steamcalc ( p_ph_2a_a, h_ph_2a_a ).temperature();
    t2_test = steamcalc::Steamcalc ( p_ph_2a_b, h_ph_2a_b ).temperature();;
    t3_test = steamcalc::Steamcalc ( p_ph_2a_c, h_ph_2a_c ).temperature();;
    double temp1 = t_ph_2a_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ph_2a_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ph_2a_c.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
}
BOOST_AUTO_TEST_CASE ( Tph_2b ) {

    steamcalc::units::Temperature 
    t1_test, 
    t2_test, 
    t3_test;
    t1_test = steamcalc::Steamcalc ( p_ph_2b_a, h_ph_2b_a ).temperature();
    t2_test = steamcalc::Steamcalc ( p_ph_2b_b, h_ph_2b_b ).temperature();;
    t3_test = steamcalc::Steamcalc ( p_ph_2b_c, h_ph_2b_c ).temperature();;
    double temp1 = t_ph_2b_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ph_2b_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ph_2b_c.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
}
BOOST_AUTO_TEST_CASE ( Tph_2c ) {

    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Steamcalc ( p_ph_2c_a, h_ph_2c_a ).temperature();
    t2_test = steamcalc::Steamcalc ( p_ph_2c_b, h_ph_2c_b ).temperature();;
    t3_test = steamcalc::Steamcalc ( p_ph_2c_c, h_ph_2c_c ).temperature();;
    double temp1 = t_ph_2c_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ph_2c_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ph_2c_c.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
}

BOOST_AUTO_TEST_CASE ( Tps_2a ) {

    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Steamcalc(p_ps_2a_a, s_ps_2a_a).temperature();
    t2_test = steamcalc::Steamcalc(p_ps_2a_b, s_ps_2a_b).temperature();;
    t3_test = steamcalc::Steamcalc(p_ps_2a_c, s_ps_2a_c).temperature();;
    double temp1 = t_ps_2a_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ps_2a_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ps_2a_c.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
}

BOOST_AUTO_TEST_CASE ( Tps_2b ) {

    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Steamcalc(p_ps_2b_a, s_ps_2b_a).temperature();
    t2_test = steamcalc::Steamcalc(p_ps_2b_b, s_ps_2b_b).temperature();;
    t3_test = steamcalc::Steamcalc(p_ps_2b_c, s_ps_2b_c).temperature();;
    double temp1 = t_ps_2b_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ps_2b_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ps_2b_c.value();
    double temp3_test = t_ps_2b_c.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
}
BOOST_AUTO_TEST_CASE ( Tps_2c ) {

    steamcalc::units::Temperature t1_test, t2_test, t3_test;
    t1_test = steamcalc::Steamcalc(p_ps_2c_a, s_ps_2c_a).temperature();
    t2_test = steamcalc::Steamcalc(p_ps_2c_c, s_ps_2c_b).temperature();;
    t3_test = steamcalc::Steamcalc(p_ps_2c_c, s_ps_2c_c).temperature();;
    double temp1 = t_ps_2c_a.value();
    double temp1_test = t1_test.value();
    double temp2 = t_ps_2c_b.value();
    double temp2_test = t2_test.value();
    double temp3 = t_ps_2c_c.value();
    double temp3_test = t3_test.value();

    BOOST_REQUIRE_CLOSE ( temp1 , temp1_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp2 , temp2_test, std_err );
    BOOST_REQUIRE_CLOSE ( temp3 , temp3_test, std_err );
    ;
}

BOOST_AUTO_TEST_SUITE_END()
