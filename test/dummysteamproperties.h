/*
 *
 */

#ifndef DUMMYSTEAMPROPERTIES_H
#define DUMMYSTEAMPROPERTIES_H

#include "units/custom_type_definitions.h"
#include <boost/math/tools/precision.hpp>
using namespace steamcalc::units;

class DummySteamProperties
{
public:
    
    /**
     * \brief Constructor load steam properties values
     */
    DummySteamProperties( Pressure _pressure, Temperature _temperature, SpecificEnergy _spec_enthalpy, SpecificHeatCapacity _spec_entropy, SpecificVolume _spec_volume, SpecificEnergy _spec_internal_energy, SpecificHeatCapacity _spec_isobaric_heat_cap, SpecificHeatCapacity _spec_isochoric_heat_cap, Velocity _speed_of_sound );
    
    bool operator==(const DummySteamProperties &other);
    
    //! Get pressure value (p)
    const Pressure& pressure()const;

    //! Get temprature value (t)
    const Temperature& temperature() const;

    //! Get specific enthalpy value (h)
    const SpecificEnergy& specificEnthalpy()const;

    //! Get specific entropy value (s)
    const SpecificHeatCapacity& specificEntropy()const;

    //! Get specific volume value (v)
    const SpecificVolume& specificVolume()const;

    //! Get specific internal energy value (u)
    const SpecificEnergy& specificInternalEnergy()const;

    //! Get Specific Isobaric Heat Capacity (cp)
    const SpecificHeatCapacity& specificIsobaricHeatCapacity()const;

    //! Get Specific Isochoric Heat Capacity (cv)
    const SpecificHeatCapacity& specificIsochoricHeatCapacity()const;

    //! Get Speed of sound (w)
    const Velocity& speedOfSound()const;
    
private:
    
  //! Pressure value
  Pressure m_pressure;
  
  //! Temperature value
  Temperature m_temperature;
  
  //! Specific enthalpy value
  SpecificEnergy m_spec_enthalpy;
  
  //! Specific entropy value
  SpecificHeatCapacity m_spec_entropy;
  
  //! Specific volume
  SpecificVolume m_spec_volume;
  
  //! Specific internal energy
  SpecificEnergy m_spec_internal_energy;
  
  //! Specific Isobaric Heat Capacity (cp)
  SpecificHeatCapacity m_spec_isobaric_heat_cap;
  
  //! Specific Isochoric Heat Capacity (cv)
  SpecificHeatCapacity m_spec_isochoric_heat_cap;
  
  //! Speed of sound (w)
  Velocity m_speed_of_sound;

};

#endif // DUMMYSTEAMPROPERTIES_H
