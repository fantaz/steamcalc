/*
 * Using BOOST TEST TOOLS
 * Test for region four
 */
// //Link to Boost
// #define BOOST_TEST_DYN_LINK
//
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "region.h"
#include <steamproperties.h>
#include "regions/one.h"
#include <ptchecker.h>
#include "boost/date_time.hpp"
#include "boost/math/tools/minima.hpp"
#include <boost/math/tools/roots.hpp>

const double err
{
    1e-6
};                                                          // absolute error used for tests
BOOST_AUTO_TEST_SUITE ( RegionFour )
BOOST_AUTO_TEST_CASE ( BasicTest ) {
  }
BOOST_AUTO_TEST_SUITE_END()
