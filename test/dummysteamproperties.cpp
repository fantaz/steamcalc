/*
 *
 */

#include "dummysteamproperties.h"

DummySteamProperties::DummySteamProperties(Pressure _pressure, Temperature _temperature, SpecificEnergy _spec_enthalpy, SpecificHeatCapacity _spec_entropy, SpecificVolume _spec_volume, SpecificEnergy _spec_internal_energy, SpecificHeatCapacity _spec_isobaric_heat_cap, SpecificHeatCapacity _spec_isochoric_heat_cap, Velocity _speed_of_sound) :
m_pressure(_pressure),
m_temperature(_temperature),
m_spec_enthalpy(_spec_enthalpy),
m_spec_entropy(_spec_entropy),
m_spec_volume(_spec_volume),
m_spec_internal_energy(_spec_internal_energy),
m_spec_isobaric_heat_cap(_spec_isobaric_heat_cap),
m_spec_isochoric_heat_cap(_spec_isochoric_heat_cap),
m_speed_of_sound(_speed_of_sound)
{
    //empty
}


bool DummySteamProperties::operator==(const DummySteamProperties &other)
{
    if
    (
        m_pressure.value() == other.pressure().value() &&
        m_temperature.value() == other.temperature().value() &&
        m_spec_enthalpy.value() == other.specificEnthalpy().value() &&
        m_spec_entropy.value() == other.specificEntropy().value() &&
        m_spec_internal_energy.value() == other.specificInternalEnergy().value() &&
        m_spec_volume.value() == other.specificVolume().value() &&
        m_spec_isobaric_heat_cap.value() == other.specificIsobaricHeatCapacity().value() &&
        /*m_spec_isochoric_heat_cap.value() == other.specificIsochoricHeatCapacity().value() &&*/
        m_speed_of_sound.value() == other.speedOfSound().value()
        
    )
    {
        return true;
    }
    return false;
}

const Pressure& DummySteamProperties::pressure()const{ return m_pressure; }

const Temperature& DummySteamProperties::temperature()const{ return m_temperature; }

const SpecificEnergy& DummySteamProperties::specificEnthalpy()const{ return m_spec_enthalpy; }

const SpecificHeatCapacity& DummySteamProperties::specificEntropy()const{ return m_spec_entropy; }

const SpecificVolume& DummySteamProperties::specificVolume()const{ return m_spec_volume; }

const SpecificEnergy& DummySteamProperties::specificInternalEnergy()const{ return m_spec_internal_energy; }

const SpecificHeatCapacity& DummySteamProperties::specificIsobaricHeatCapacity()const{ return m_spec_isobaric_heat_cap; }

const SpecificHeatCapacity& DummySteamProperties::specificIsochoricHeatCapacity()const{ return m_spec_isochoric_heat_cap; }

const Velocity& DummySteamProperties::speedOfSound()const{ return m_speed_of_sound; }