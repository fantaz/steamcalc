/*
 * Using BOOST TEST TOOLS
 * Test for region five
 */
// //Link to Boost
// #define BOOST_TEST_DYN_LINK
//
// //Define our Module name (prints at testing)
// #define BOOST_TEST_MODULE "BaseClassModule"

//VERY IMPORTANT - include this last
#include <boost/test/unit_test.hpp>
#include "test.h"
#include "steamcalc.h"
#include "boost/date_time.hpp"
#include "boost/math/tools/minima.hpp"
#include <boost/math/tools/roots.hpp>

steamcalc::units::Temperature T5_1 ( 1500 * kelvin ), T5_2 ( 2000*kelvin );
steamcalc::units::Pressure p5_1 ( 0.5 * mega * pascal ), p5_2 ( 30 * mega * pascal );

const steamcalc::units::SpecificEnergy h1 ( 500 * kilo * joule_per_kilogram ), h2 ( 1500 * kilo * joule_per_kilogram );
const steamcalc::units::SpecificHeatCapacity s1 ( 0.5*kilo*joule_per_kilogram_and_kelvin ),s2 ( 3*kilo*joule_per_kilogram_and_kelvin );


const steamcalc::units::SpecificVolume
v_a ( 0.138455090e1 * cubic_meter_per_kilogram ),
    v_b ( 0.230761299e-1 * cubic_meter_per_kilogram ),
    v_c ( 0.311385219e-1 * cubic_meter_per_kilogram );

const steamcalc::units::SpecificEnergy
h_a ( 0.521976855e4 * kilo * joule_per_kilogram ),
    h_b ( 0.516723514e4 * kilo * joule_per_kilogram ),
    h_c ( 0.657122604e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificEnergy
u_a ( 0.452749310e4 * kilo * joule_per_kilogram ),
    u_b ( 0.447495124e4 * kilo * joule_per_kilogram ),
    u_c ( 0.563707038e4 * kilo * joule_per_kilogram );

const steamcalc::units::SpecificHeatCapacity
s_a ( 0.965408875e1 * kilo * Joule_per_kilogram_and_kelvin ),
    s_b ( 0.772970133e1 * kilo * Joule_per_kilogram_and_kelvin ),
    s_c ( 0.853640523e1 *  kilo * Joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificHeatCapacity
cp_a ( 0.261609445e1 * kilo * joule_per_kilogram_and_kelvin ),
     cp_b ( 0.272724317e1 * kilo * joule_per_kilogram_and_kelvin ),
     cp_c ( 0.288569882e1 * kilo * joule_per_kilogram_and_kelvin );

const steamcalc::units::SpecificHeatCapacity
cv_a ( 0 * joule_per_kilogram_and_kelvin ),
     cv_b ( 0 * joule_per_kilogram_and_kelvin ),
     cv_c ( 0 * joule_per_kilogram_and_kelvin );

const steamcalc::units::Velocity
w_a ( 0.917068690e3 * meter_per_second ),
    w_b ( 0.928548002e3 * meter_per_second ),
    w_c ( 0.106736948e4 * meter_per_second );

BOOST_AUTO_TEST_SUITE ( RegionFive )
BOOST_AUTO_TEST_CASE ( BasicTestFive ) {
    steamcalc::Steamcalc r_test_a ( p5_1, T5_1 ),  r_test_b ( p5_2, T5_1 ),  r_test_c ( p5_2, T5_2 );

    double spec_enthalpy_a = h_a.value();
    double spec_enthalpy_b = h_b.value();
    double spec_enthalpy_c = h_c.value();
    double spec_enthalpy_test_a = r_test_a.specificEnthalpy().value();
    double spec_enthalpy_test_b = r_test_b.specificEnthalpy().value();
    double spec_enthalpy_test_c = r_test_c.specificEnthalpy().value();

    double spec_entropy_a = s_a.value();
    double spec_entropy_b = s_b.value();
    double spec_entropy_c = s_c.value();
    double spec_entropy_test_a = r_test_a.specificEntropy().value();
    double spec_entropy_test_b = r_test_b.specificEntropy().value();
    double spec_entropy_test_c = r_test_c.specificEntropy().value();

    double spec_intern_energ_a = u_a.value();
    double spec_intern_energ_b = u_b.value();
    double spec_intern_energ_c = u_c.value();
    double spec_intern_energ_test_a = r_test_a.specificInternalEnergy().value();
    double spec_intern_energ_test_b = r_test_b.specificInternalEnergy().value();
    double spec_intern_energ_test_c = r_test_c.specificInternalEnergy().value();

    double spec_isobaric_heat_cap_a = cp_a.value();
    double spec_isobaric_heat_cap_b = cp_b.value();
    double spec_isobaric_heat_cap_c = cp_c.value();
    double spec_isobaric_heat_cap_test_a = r_test_a.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_b = r_test_b.specificIsobaricHeatCapacity().value();
    double spec_isobaric_heat_cap_test_c = r_test_c.specificIsobaricHeatCapacity().value();

    double speed_of_sound_a = w_a.value();
    double speed_of_sound_b = w_b.value();
    double speed_of_sound_c = w_c.value();
    double speed_of_sound_test_a = r_test_a.speedOfSound().value();
    double speed_of_sound_test_b = r_test_b.speedOfSound().value();
    double speed_of_sound_test_c = r_test_c.speedOfSound().value();

    double spec_volume_a = v_a.value();
    double spec_volume_b = v_b.value();
    double spec_volume_c = v_c.value();
    double spec_volume_test_a = r_test_a.specificVolume().value();
    double spec_volume_test_b = r_test_b.specificVolume().value();
    double spec_volume_test_c = r_test_c.specificVolume().value();

    // First set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_a , spec_enthalpy_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_a, spec_entropy_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_a , spec_intern_energ_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_a , spec_isobaric_heat_cap_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_a , speed_of_sound_test_a, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_a , spec_volume_test_a, std_err );

    // Second set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_b, spec_enthalpy_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_b, spec_entropy_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_b , spec_intern_energ_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_b , spec_isobaric_heat_cap_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_b , speed_of_sound_test_b, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_b, spec_volume_test_b, std_err );

    // Last set
    BOOST_REQUIRE_CLOSE ( spec_enthalpy_c, spec_enthalpy_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_entropy_c, spec_entropy_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_intern_energ_c, spec_intern_energ_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_isobaric_heat_cap_c, spec_isobaric_heat_cap_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( speed_of_sound_c, speed_of_sound_test_c, std_err );
    BOOST_REQUIRE_CLOSE ( spec_volume_c, spec_volume_test_c, std_err );
}

BOOST_AUTO_TEST_CASE(Five_pTh) {
    steamcalc::Steamcalc pth(T5_1, h_a);
    
    steamcalc::units::Pressure p_test = pth.pressure();
    
    BOOST_REQUIRE_CLOSE ( p5_1.value(), p_test.value(), backward_err );

}

BOOST_AUTO_TEST_CASE(Five_Tph) {
    steamcalc::Steamcalc pth(p5_1, h_a);

    steamcalc::units::Temperature t_test = pth.temperature();

    BOOST_REQUIRE_CLOSE ( T5_1.value(), t_test.value(), backward_err );

  }

BOOST_AUTO_TEST_CASE(Five_pTw) {
    steamcalc::Steamcalc pth(T5_1, w_a);

    steamcalc::units::Pressure p_test = pth.pressure();

    BOOST_REQUIRE_CLOSE ( p5_1.value(), p_test.value(), backward_err );

  }

BOOST_AUTO_TEST_SUITE_END()
