#include <iostream>
#include "steamcalc.h"

int main()
{
    using namespace steamcalc::units;
    using namespace steamcalc;
    Temperature t{400.*kelvin};
    Pressure p{0.1*mega*pascal};
    
    // define initial state
    Steamcalc state01(p,t);
    
    // extract entropy
    SpecificHeatCapacity s  = state01.specificEntropy();
    
    std::cout<<"Entropy at initial state: "<<s<<std::endl;
    
    // construct state pair (pressure, entropy)
    Steamcalc state02(p,s);
    
    // get the results
    t = state02.temperature();
    p = state02.pressure();
    s = state02.specificEntropy();
    
    std::cout<<"At p="<<p<<" and s="<<s<<", T="<<t<<std::endl;
    
    return 0;
}
