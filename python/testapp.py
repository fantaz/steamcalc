"""
Steam calculator using _steamcalc module based on Steamcalc C++ library

"""

import wx
import os
import numpy as np
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
import matplotlib.gridspec as gridspec
from pylab import *
from matplotlib.backends.backend_wxagg import NavigationToolbar2Wx as Toolbar
from matplotlib.font_manager import FontProperties
# importing steamcalc lib
from _steamcalc import *

# global values
property_names = (  " p = "," T = ",   " h = ",   " u = ", " s = ", " cp = ", " cv = ", " v = ", " rho = ", " w = ")
property_names_cl = ( "h",   "u", "s", "cp", "v", "rho", "w")
property_units = (  "[Mpa]",  "[K]", "[kJ/kg]", "[kJ/kg]", "[kJ/kgK]", "[kJ/kgK]", "[kJ/kgK]", "[m^3/kg]", "[kg/m^3]", "[m/s]")
property_pairs = (  "p,T" , "rho,T",     "T,h",  "T,s","T,u","T,v", "T,w", "T,cp", "p,h","p,s","p,u","p,cp","p,v", "p,w", "p,rho")
#############################################################################        
def message(parent, msg ):
    dlg = wx.MessageDialog(parent, msg, "Warning", wx.OK)
    dlg.ShowModal() # Shows it
    dlg.Destroy()
#############################################################################        

#############################################################################
#class Calculator():
    #def __init__(self):
        
#############################################################################        

#############################################################################        
class NumCtrl(wx.TextCtrl):
    def __init__(self, parent, size):
        wx.TextCtrl.__init__(self,parent=parent, size=size)
        self.Bind(wx.EVT_CHAR, self.checkFloat)
        self.Bind(wx.EVT_KEY_UP, self.checkDecimalPoint)

    def checkFloat(self, e):
        keycode = e.GetKeyCode()
        if keycode < 255:
            # allowing events
            if keycode == wx.WXK_BACK or keycode == wx.WXK_LBUTTON or keycode == wx.WXK_RBUTTON or keycode == wx.WXK_DELETE or keycode == 46 or keycode >= 48 and keycode <= 57 :
                # Valid alphanumeric character
                e.Skip()
    
    def checkDecimalPoint(self, e):
        if self.Value == ".":
            self.Value = "0."
            self.SetInsertionPointEnd()
#############################################################################        

#############################################################################        
class CalcPanel(wx.Panel):
    def __init__(self,parent_):
        # size (sirina, visina)
        wx.Panel.__init__(self, parent = parent_)#, size=(300,300))
        self.lPanel = wx.Panel(self, style = wx.RAISED_BORDER)#, size = (80,300) )
        self.mPanel = wx.Panel(self, style = wx.RAISED_BORDER)#, size = (190,300))
        #self.rPanel = wx.Panel(self, style = wx.RAISED_BORDER )    
        
    
        self.sizer = wx.BoxSizer( wx.HORIZONTAL )
        self.setInputFields()
        self.setCalcInput()
        self.sizer.Add( self.lPanel,-1, wx.EXPAND )
        self.sizer.Add( self.mPanel,-1, wx.EXPAND )
        self.SetSizer( self.sizer )
        self.SetAutoLayout(1)
        #self.FitInside()
        
    def setCalcInput(self):
        bsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer = wx.BoxSizer(wx.HORIZONTAL)
        bsizer.Add(wx.StaticText(self.lPanel, 0, label="Set value pair:", size = (80,30),style = wx.EXPAND))
        self.combo = wx.ComboBox(self.lPanel, pos=(80, 30), value = property_pairs[0], choices=property_pairs, style=wx.CB_READONLY, size = (80,20) )
        bsizer.Add(self.combo)
        self.input_first = NumCtrl( self.lPanel, size = (80,20) )
        self.input_second = NumCtrl( self.lPanel, size = (80,20) )
        bsizer.Add( self.input_first )
        bsizer.Add( self.input_second )
        
        button = wx.Button(self.lPanel, 0, label="Calculate", size = (80,30),style = wx.EXPAND)
        bsizer.Add(button)
        #self.Bind(wx.EVT_BUTTON, self.claculate, button)
        button.Bind(wx.EVT_BUTTON, self.claculate)

        self.lPanel.SetSizer(bsizer)
        self.lPanel.Layout()
        self.lPanel.Fit()

    def setInputFields(self):
        bsizer = wx.BoxSizer(wx.VERTICAL)
        self.values = []
        sizers = []
        names = []
        units = []
        bsizer.Add(wx.StaticText(self.mPanel, 0, label="Property values:",style = wx.EXPAND))
        for i in range(0, 10):
            names.append( wx.StaticText(self.mPanel, -1, label=property_names[i], size = (45,20),style = wx.EXPAND))
            self.values.append (wx.StaticText(self.mPanel, -1, size = (80,20) , label = "0.0",style = wx.ALIGN_RIGHT))
            units.append( wx.StaticText(self.mPanel, -1, label=property_units[i],size = (65,20),style = wx.EXPAND))
            sizers.append(wx.BoxSizer(wx.HORIZONTAL))
            sizers[i].Add(names[i], 0)
            sizers[i].Add(self.values[i], 0)
            sizers[i].Add(units[i], 0)
            bsizer.Add(sizers[i], 0, border=10)
            
        self.mPanel.SetSizer(bsizer)
        self.mPanel.Layout()
        self.mPanel.FitInside()
        
            
    def claculate(self, e):
        val1 = 0.0
        val2 = 0.0
        try:       
            val1 = float(self.input_first.GetValue())
            val2 = float(self.input_second.GetValue())
            
            pair = self.combo.GetValue()
            if pair == "p,T":
                self.property_values = steamcalc_pt(val1,val2)
            elif pair == "rho,T":
                self.property_values = steamcalc_rhot(val1,val2)
            elif pair == "T,h":
                self.property_values = steamcalc_th(val1,val2)
            elif pair == "T,s":
                self.property_values = steamcalc_ts(val1,val2)
            elif pair == "T,u":
                self.property_values = steamcalc_tu(val1,val2)
            elif pair == "T,v":
                self.property_values = steamcalc_tv(val1,val2)
            elif pair == "T,w":
                self.property_values = steamcalc_tw(val1,val2)
            elif pair == "T,cp":
                self.property_values = steamcalc_tcp(val1,val2)
            elif pair == "p,h":
                self.property_values = steamcalc_ph(val1,val2)
            elif pair == "p,s":
                self.property_values = steamcalc_ps(val1,val2)
            elif pair == "p,u":
                self.property_values = steamcalc_pu(val1,val2)
            elif pair == "p,cp":
                self.property_values = steamcalc_pcp(val1,val2)
            elif pair == "p,v":
                self.property_values = steamcalc_pv(val1,val2)
            elif pair == "p,w":
                self.property_values = steamcalc_pw(val1,val2)
            elif pair == "p,rho":
                self.property_values = steamcalc_prho(val1,val2)
            else:
                self.property_values =  steamcalc_none()
                        
            self.setValues(e)
        except ValueError:
            message(self,"Only number allowed!")
        

        
    def setValues(self, e):
        
        if self.property_values[0] == 0 and self.property_values[1] == 0:
            message(self, "Values out of range!\n\nPressure must be 0 MPa < p < 100 MPa\nand Temperature 273.15 K < T < 2273.15 K\nor greater than 0!")
        else:
            pass

        for i in range(0, 10):
            string_value = str(round(self.property_values[i], 3))
            #print string_value
            self.values[i].SetLabel( string_value )
            #print self.values[i].GetLabel()
#############################################################################        

#############################################################################          
class GraphPanel(wx.Panel):
    p_values=[]
    t_values=[]
    
    def __init__(self,parent_):
        # size (sirina, visina)
        wx.Panel.__init__(self, parent = parent_)#, size=(300,300))
        
        # init values
        self.p_values = np.arange(0.01, 100, 0.01)
        self.t_values = np.zeros(self.p_values.size)
        
        self.lPanel = wx.Panel(self, style = wx.RAISED_BORDER)#, size = (80,300) )
        self.rPanel = wx.Panel(self, style = wx.RAISED_BORDER , size = (150,300) )
        
        self.sizer = wx.BoxSizer( wx.VERTICAL )
        self.figInit()
        self.constSet()
        self.sizer.Add( self.lPanel,1, wx.EXPAND )
        self.sizer.Add( self.rPanel,1, wx.EXPAND )
        self.SetSizer( self.sizer )
        
    def constSet(self):
        bsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer = wx.BoxSizer(wx.HORIZONTAL)
        bsizer.Add(wx.StaticText(self.lPanel, 0, label="Constant value:", size = (80,30),style = wx.EXPAND))
        self.combo = wx.ComboBox(self.lPanel, pos=(80, 30), value = property_names_cl[0], choices=property_names_cl, style=wx.CB_READONLY, size = (80,20) )
        bsizer.Add(self.combo)
        self.input_first = NumCtrl( self.lPanel, size = (80,20) )
        bsizer.Add( self.input_first )
        
        button = wx.Button(self.lPanel, 0, label="Calculate", size = (80,30),style = wx.EXPAND)
        bsizer.Add(button)
        self.Bind(wx.EVT_BUTTON, self.calculate, button)

        self.lPanel.SetSizer(bsizer)
        
    def calculate(self, e):
        val = float(self.input_first.GetValue())
        i=0
        property = self.combo.GetValue()
        if property == "h":
            for p in self.p_values:
                self.t_values[i] = steamcalc_ph(p,val)[1]
                i=i+1
        elif property == "s":
            for p in self.p_values:
                self.t_values[i] = steamcalc_ps(p,val)[1]
                i=i+1
        elif property == "u":
            for p in self.p_values:
                self.t_values[i] = steamcalc_pu(p,val)[1]
                i=i+1
        elif property == "v":
            for p in self.p_values:
                self.t_values[i] = steamcalc_pv(p,val)[1]
                i=i+1
        elif property == "cp":
            for p in self.p_values:
                self.t_values[i] = steamcalc_pcp(p,val)[1]
                i=i+1
        elif property == "w":
            for p in self.p_values:
                self.t_values[i] = steamcalc_pw(p,val)[1]
                i=i+1
        elif property == "rho":
            for p in self.p_values:
                self.t_values[i] = steamcalc_prho(p,val)[1]
                i=i+1
        else:
            for p in self.p_values:
                self.t_values[i] = 0
                i=i+1
            
        print self.t_values
        self.t_values = sort(self.t_values)
        self.draw(e)
            
    def draw(self, e):   
        self.rPanel.axes.cla()
        self.rPanel.axes.set_title("p-T graph diagram")        
        self.rPanel.axes.plot( self.t_values, self.p_values,"b-")
        self.rPanel.axes.set_xlim(273.15,2273.15)
        self.rPanel.axes.set_ylim(0,100)

        self.rPanel.canvas.draw()
        
    def figInit(self):
        self.rPanel.figure = Figure()#figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')#figsize=(5, 10))
        
        self.rPanel.canvas = FigureCanvas(self.rPanel,-1,self.rPanel.figure)
        
        gs1 = gridspec.GridSpec(1, 1)
        
        self.rPanel.axes = self.rPanel.figure.add_subplot(gs1[0])
        self.rPanel.axes.cla()
        self.rPanel.axes.set_title("p-T graph diagram")
        self.rPanel.axes.set_xlim(273.15,2273.15)
        self.rPanel.axes.set_ylim(0,100)
        
        gs1.tight_layout(self.rPanel.figure)
        
        self.rPanel.canvas.draw()
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.rPanel.canvas, 1, border=10)
        
        self.rPanel.SetSizer(sizer)
        self.rPanel.Layout()
        self.rPanel.Fit()
#############################################################################        



#############################################################################        
class SteamCalcPanel(wx.Frame):
    
    property_values = ()
    
    def __init__(self):
        # size (sirina, visina)
        wx.Frame.__init__(self, parent = None)#, size=(0,0))
        self.calc = CalcPanel(self)#, size = (80,300) )
        self.graph = GraphPanel(self)#, size = (190,300))
        
        # Setting up the menu.
        filemenu= wx.Menu()
        menuAbout= filemenu.Append(wx.ID_ABOUT, "&About"," Info")
        menuExit = filemenu.Append(wx.ID_EXIT,"&Exit"," Exit")

        # Create status bar
        statusBar = wx.StatusBar(self, style = wx.RAISED_BORDER )
        self.SetStatusBar(statusBar)

        # File Events.
        self.Bind(wx.EVT_MENU, self.OnExit, menuExit)
        self.Bind(wx.EVT_MENU, self.OnAbout, menuAbout)
        
        viewmenu = wx.Menu()
        menuCalc = viewmenu.Append(-1,"&CalcPanel"," CalcPanel")
        menuGraphPanel = viewmenu.Append(-1,"&GraphPanel"," GraphPanel")
        
        # View Events.
        self.Bind(wx.EVT_MENU, self.onCalc, menuCalc)
        self.Bind(wx.EVT_MENU, self.onGraphPanel, menuGraphPanel)
        
        # Creating the menubar.
        menuBar = wx.MenuBar( style = wx.RAISED_BORDER )
        menuBar.Append(filemenu,"&File") # Adding the "filemenu" to the MenuBar
        menuBar.Append(viewmenu,"&View") # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.

        self.sizer = wx.BoxSizer( wx.VERTICAL )
        self.sizer.Add( self.calc,1, wx.EXPAND )
        self.sizer.Add( self.graph,1, wx.EXPAND )
        self.SetSizer( self.sizer )
        
        self.graph.Hide()
        self.calc.Show()
        
    def OnAbout(self,e):
        # Create a message dialog box
        dlg = wx.MessageDialog(self, " Steam calculator \n in wxPython", "About", wx.OK)
        dlg.ShowModal() # Shows it
        dlg.Destroy() # finally destroy it when finished.

    def OnExit(self,e):
        self.Close(True)  # Close the frame.
        
    def onCalc(self, e):
        self.graph.Hide()
        self.calc.Show()
        self.Layout()
        
    def onGraphPanel(self, e):
        self.calc.Hide()
        self.graph.Show()
        self.Layout()

#############################################################################        

# Frame je prozor, svi elementi su naslijedeni od Window-a tipa botun itd.
class App(wx.App):
    def OnInit(self):
        frame = SteamCalcPanel()
        return True

if __name__ == '__main__':     
        app = wx.App()
        frame=SteamCalcPanel()
        frame.Show()
        app.MainLoop()