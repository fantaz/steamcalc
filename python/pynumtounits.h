#ifndef STEAMCALC_PYTHON_PYNUMTOUNITS_H
#define STEAMCALC_PYTHON_PYNUMTOUNITS_H
# include "assert.h"

namespace steamcalc {
    namespace python {
        /// \brief Minimal pressure value
        const double PRESS_MIN
        {
            0.
        };
        /// \brief Maximal pressure value
        const double PRESS_MAX
        {
            100.
        };
        
        /// \brief Minimal temperature value
        const double TEMP_MIN
        {
            273.15
        };

        /// \brief Maximal temperature value
        const double TEMP_MAX
        {
            2273.15
        };

        /// \brief Absolute minimal value for all units
        const double ABS_MIN
        {
            0.
        };


        /**
        * \brief Check for pressure
        */
        bool checkPressure(const double& _value) {

            if (_value < PRESS_MIN && _value > PRESS_MAX) return true;

            return false;
        }

        /**
        * \brief Check for temperature
        */
        bool checkTemperature(const double& _value) {

            if (_value < TEMP_MIN && _value > TEMP_MAX) return true;

            return false;
          }

        /**
        * \brief Check for other units
        */
        bool checkOther(const double& _value) {

            if (_value < ABS_MIN) return true;

            return false;
          }
    }
}

#endif // STEAMCALC_PYTHON_PYNUMTOUNITS_H
