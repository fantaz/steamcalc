#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python.hpp>
#include <Python.h>
#include "steamcalc.h"
#include "unitstopynum.h"
#include "pynumtounits.h"

/**
 * \brief Function to fill values into tuple object
 */
boost::python::tuple getValues ( steamcalc::Steamcalc& sc ) {
    return boost::python::make_tuple (
               sc.pressure(),
               sc.temperature(),
               sc.specificEnthalpy(),
               sc.specificInternalEnergy(),
               sc.specificEntropy(),
               sc.specificIsobaricHeatCapacity(),
               sc.specificIsochoricHeatCapacity(),
               sc.specificVolume(),
               sc.density(),
               sc.speedOfSound()
           );
}

/**
 * \brief Calculate p,T pair using Steamcalc lib
 */
boost::python::tuple steamcalc_none () {

    steamcalc::Steamcalc none;

    return getValues ( none );
}

/**
 * \brief Calculate p,T pair using Steamcalc lib
 */
boost::python::tuple steamcalc_pt ( const double& _p, const double& _t ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkTemperature ( _t )) return steamcalc_none ();

    steamcalc::Steamcalc pt ( steamcalc::units::Pressure ( _p * mega * pascals ), steamcalc::units::Temperature ( _t * kelvin ) );

    return getValues ( pt );
}

/**
 * \brief Calculate density - temperature pair using Steamcalc lib
 */
boost::python::tuple steamcalc_rhot ( const double& _rho, const double& _t ) {

    if (steamcalc::python::checkOther ( _rho ) || steamcalc::python::checkTemperature ( _t )) return steamcalc_none ();

    steamcalc::Steamcalc rhot ( steamcalc::units::MassDensity ( _rho * kilogram / ( meter * meter * meter ) ), steamcalc::units::Temperature ( _t * kelvin ) );

    return getValues ( rhot );
}

/**
 * \brief Calculate pressure - density pair using Steamcalc lib
 */
boost::python::tuple steamcalc_prho ( const double& _p, const double& _rho ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _rho )) return steamcalc_none ();

    steamcalc::Steamcalc prho ( steamcalc::units::Pressure ( _p * mega * pascals ), steamcalc::units::MassDensity ( _rho * kilogram / ( meter * meter * meter ) ) );

    return getValues ( prho );
}

/**
 * \brief Calculate pressure - specific enthalpy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_ph ( const double& _p, const double& _h ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _h )) return steamcalc_none ();

    steamcalc::Steamcalc ph ( steamcalc::units::Pressure ( _p * mega * pascals ), steamcalc::units::SpecificEnergy ( _h * kilo * joule/kilogram ) );

    return getValues ( ph );
}

/**
 * \brief Calculate pressure - specific entropy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_ps ( const double& _p, const double& _s ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _s )) return steamcalc_none ();

    steamcalc::Steamcalc ps_pair ( steamcalc::units::Pressure ( _p * mega * pascal ), steamcalc::units::SpecificHeatCapacity ( _s * kilo * joule / ( kilogram*kelvin ) ) );
    return getValues ( ps_pair );
}

/**
 * \brief Calculate pressure - specific volume pair using Steamcalc lib
 */
boost::python::tuple steamcalc_pv ( const double& _p, const double& _v ) {

    if (steamcalc::python::checkPressure ( _p ) ||  steamcalc::python::checkOther ( _v )) return steamcalc_none ();

    steamcalc::Steamcalc pv ( steamcalc::units::Pressure ( _p * mega * pascals ), steamcalc::units::SpecificVolume ( _v * cubic_meter_per_kilogram ) );

    return getValues ( pv );
}

/**
 * \brief Calculate pressure - specific internal energy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_pu ( const double& _p, const double& _u ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _u )) return steamcalc_none ();

    steamcalc::Steamcalc pu ( steamcalc::units::SpecificEnergy ( _u * kilo * joule/kilogram ),  steamcalc::units::Pressure ( _p * mega * pascals ) );

    return getValues ( pu );
}

/**
 * \brief Calculate pressure - specific isobaric heat capacity pair using Steamcalc lib
 */
boost::python::tuple steamcalc_pcp ( const double& _p, const double& _cp ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _cp )) return steamcalc_none ();

    steamcalc::Steamcalc pcp ( steamcalc::units::SpecificHeatCapacity ( _cp * kilo * joule / ( kilogram*kelvin ) ), steamcalc::units::Pressure ( _p * mega * pascals ) );

    return getValues ( pcp );
}

// /**
//  * \brief Calculate pressure - specific isochoric heat capacity pair using Steamcalc lib
//  */
// boost::python::tuple steamcalc_pcv ( const double& p, const double& cv ) {
//
//     steamcalc::Steamcalc pcp;
//
//     return getValues ( pcp );
// }

/**
 * \brief Calculate pressure - speed of sound pair using Steamcalc lib
 */
boost::python::tuple steamcalc_pw ( const double& _p, const double& _w ) {

    if (steamcalc::python::checkPressure ( _p ) || steamcalc::python::checkOther ( _w )) return steamcalc_none ();

    steamcalc::Steamcalc pw ( steamcalc::units::Pressure ( _p * mega * pascals ), steamcalc::units::Velocity ( _w * meter_per_second ) );

    return getValues ( pw );
}

/**
 * \brief Calculate temperature - specific enthalpy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_th ( const double& _t, const double& _h ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _h )) return steamcalc_none ();
    
    steamcalc::Steamcalc th ( steamcalc::units::Temperature ( _t * kelvin ), steamcalc::units::SpecificEnergy ( _h * kilo * joules_per_kilogram) );

    return getValues ( th );
}

/**
 * \brief Calculate temperature - specific entropy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_ts ( const double& _t, const double& _s ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _s )) return steamcalc_none ();

    steamcalc::Steamcalc ts ( steamcalc::units::Temperature ( _t * kelvin ), steamcalc::units::SpecificHeatCapacity ( _s * kilo * joule / ( kilogram*kelvin ) ) );

    return getValues ( ts );
}

/**
 * \brief Calculate temperature - specific volume pair using Steamcalc lib
 */
boost::python::tuple steamcalc_tv ( const double& _t, const double& _v ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _v )) return steamcalc_none ();

    steamcalc::Steamcalc tv ( steamcalc::units::Temperature ( _t * kelvin ), steamcalc::units::SpecificVolume ( _v * cubic_meter_per_kilogram ) );

    return getValues ( tv );
}

/**
 * \brief Calculate temperature - specific internal energy pair using Steamcalc lib
 */
boost::python::tuple steamcalc_tu ( const double& _t, const double& _u ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _u )) return steamcalc_none ();

    steamcalc::Steamcalc tu ( steamcalc::units::SpecificEnergy ( _u * kilo * joule/kilogram ), steamcalc::units::Temperature ( _t * kelvin ) );

    return getValues ( tu );
}

/**
 * \brief Calculate temperature - specific isobaric heat capacity pair using Steamcalc lib
 */
boost::python::tuple steamcalc_tcp ( const double& _t, const double& _cp ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _cp )) return steamcalc_none ();

    steamcalc::Steamcalc tcp ( steamcalc::units::SpecificHeatCapacity ( _cp * kilo * joule / ( kilogram*kelvin ) ), steamcalc::units::Temperature ( _t * kelvin ) );

    return getValues ( tcp );
}

/**
 * \brief Calculate temperature - specific isochoric heat capacity pair using Steamcalc lib
 */
// boost::python::tuple steamcalc_tcv ( const double& T, const double& cv ) {
//
//     steamcalc::Steamcalc tcv;
//
//     return getValues ( tcv );
// }

/**
 * \brief Calculate temperature - speed of sound pair using Steamcalc lib
 */
boost::python::tuple steamcalc_tw ( const double& _t, const double& _w ) {

    if (steamcalc::python::checkTemperature ( _t ) || steamcalc::python::checkOther ( _w )) return steamcalc_none ();

    steamcalc::Steamcalc tw ( steamcalc::units::Temperature ( _t * kelvin ), steamcalc::units::Velocity ( _w * meter_per_second ) );

    return getValues ( tw );
}

BOOST_PYTHON_MODULE ( _steamcalc ) {
    
    using namespace boost::python;

    const bool show_user_defined = true;
    const bool show_py_signatures = true;
    const bool show_cpp_signatures = true;

    docstring_options ( show_user_defined, show_py_signatures, show_cpp_signatures );

    boost::python::to_python_converter<steamcalc::units::Pressure, steamcalc::python::UnitsToPyNum<steamcalc::units::Pressure>>();
    boost::python::to_python_converter<steamcalc::units::Temperature, steamcalc::python::UnitsToPyNum<steamcalc::units::Temperature>>();
    boost::python::to_python_converter<steamcalc::units::MassDensity, steamcalc::python::UnitsToPyNum<steamcalc::units::MassDensity>>();
    boost::python::to_python_converter<steamcalc::units::SpecificEnergy, steamcalc::python::UnitsToPyNum<steamcalc::units::SpecificEnergy>>();
    boost::python::to_python_converter<steamcalc::units::SpecificHeatCapacity, steamcalc::python::UnitsToPyNum<steamcalc::units::SpecificHeatCapacity>>();
    boost::python::to_python_converter<steamcalc::units::SpecificVolume, steamcalc::python::UnitsToPyNum<steamcalc::units::SpecificVolume>>();
    boost::python::to_python_converter<steamcalc::units::Velocity, steamcalc::python::UnitsToPyNum<steamcalc::units::Velocity>>();

    def (
        "steamcalc_none",
        steamcalc_none,
        "Steamcalc none function to set property values to zero. Input: p[MPa],  T[K]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_pt",
        steamcalc_pt,
        (
            arg ( "p" ),
            arg ( "t" )
        ),
        "Steamcalc p, T function to calculate property values. Input: p[MPa],  T[K]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_rhot",
        steamcalc_rhot,
        (
            arg ( "rho" ),
            arg ( "t" )
        ),
        "Steamcalc rho, T function to calculate property values. Input: rho[kg/m^3],  T[K]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_prho",
        steamcalc_prho,
        (
            arg ( "p" ),
            arg ( "rho" )
        ),
        "Steamcalc rho, T function to calculate property values. Input: p[MPa], rho[kg/m^3]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_ph",
        steamcalc_ph,
        (
            arg ( "p" ),
            arg ( "h" )
        ),
        "Steamcalc p, h function to calculate property values. Input: p[MPa],  h[J/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_ps",
        steamcalc_ps,
        (
            arg ( "p" ),
            arg ( "s" )
        ),
        "Steamcalc p, s function to calculate property values. Input: p[MPa],  s[J/(kgK)]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_pv",
        steamcalc_pv,
        (
            arg ( "p" ),
            arg ( "v" )
        ),
        "Steamcalc p, v function to calculate property values. Input: p[MPa],  v[m^3/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_pu",
        steamcalc_pu,
        (
            arg ( "p" ),
            arg ( "u" )
        ),
        "Steamcalc p, u function to calculate property values. Input: p[MPa],  u[J/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_pcp",
        steamcalc_pcp,
        (
            arg ( "p" ),
            arg ( "cp" )
        ),
        "Steamcalc p, cp function to calculate property values. Input: p[MPa],  cp[J/(kgK)]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

//     def (
//         "steamcalc_pcv",
//         steamcalc_pcv,
//         (
//             arg ( "p" ),
//             arg ( "cv" )
//         )
//     );

    def (
        "steamcalc_pw",
        steamcalc_pw,
        (
            arg ( "p" ),
            arg ( "w" )
        ),
        "Steamcalc p, w function to calculate property values. Input: p[MPa],  w[m/s]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_th",
        steamcalc_th,
        (
            arg ( "t" ),
            arg ( "h" )
        ),
        "Steamcalc t, h function to calculate property values. Input: t[K],  h[J/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_ts",
        steamcalc_ts,
        (
            arg ( "t" ),
            arg ( "s" )
        ),
        "Steamcalc t, s function to calculate property values. Input: t[K],  s[J/(kgK)]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_tv",
        steamcalc_tv,
        (
            arg ( "t" ),
            arg ( "v" )
        ),
        "Steamcalc t, v function to calculate property values. Input: t[K],  v[m^3/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_tu",
        steamcalc_tu,
        (
            arg ( "t" ),
            arg ( "u" )
        ),
        "Steamcalc t, u function to calculate property values. Input: t[K],  u[J/kg]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

    def (
        "steamcalc_tcp",
        steamcalc_tcp,
        (
            arg ( "t" ),
            arg ( "cp" )
        ),
        "Steamcalc t, cp function to calculate property values. Input: t[K],  cp[J/(kgK)]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );

//     def (
//         "steamcalc_tcv",
//         steamcalc_tcv,
//         (
//             arg ( "t" ),
//             arg ( "cv" )
//         )
//     );

    def (
        "steamcalc_tw",
        steamcalc_tw,
        (
            arg ( "t" ),
            arg ( "w" )
        ),
        "Steamcalc t, w function to calculate property values. Input: t[K],  w[m/s]. Output as follows: p[MPa], T[K], h[kJ/kg], u[kJ/kg], s[kJ/(kg*K)], cp[kJ/(kg*K)],  cv[kJ/(kg*K)],  v[kg/m^3], rho[m^3/kg], w[m/s]"
    );
}
