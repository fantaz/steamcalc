#ifndef STEAMCALC_PYTHON_UNITSTOPYNUM_H
#define STEAMCALC_PYTHON_UNITSTOPYNUM_H
# include "boost/python.hpp"
# include "core.h"

namespace steamcalc {
namespace python {

/**
 * \brief Converts unit to raw value
 */
template<typename T>
struct UnitsToPyNum;

/**
 * \brief  Converts unit to raw value
 */
template<typename T>
struct UnitsToPyNum
{    
    /**
     * \brief Predefined convert method to be called from converter template
     */
    static PyObject* convert(const T& _quantity){
        return boost::python::incref ( 
            boost::python::object ( 
                _quantity.value() 
              ).ptr() );
    }
};

/**
 * \brief  Converts pressure unit to raw value, value is represented in MPa
 */
template<>
struct UnitsToPyNum<units::Pressure>
{    
    /**
     * \brief Predefined convert method to be called from converter template
     */
    static PyObject* convert(const units::Pressure& _quantity){
        return boost::python::incref ( 
            boost::python::object ( 
                _quantity.value() / units::Dimensionless(1. * mega).value()
              ).ptr() );
      }
};

/**
 * \brief Converts specific energy unit to raw value, value is represented in kJ/kg
 */
template<>
struct UnitsToPyNum<units::SpecificHeatCapacity>
{    
    /**
     * \brief Predefined convert method to be called from converter template
     */
    static PyObject* convert(const units::SpecificHeatCapacity& _quantity){
        return boost::python::incref ( 
            boost::python::object ( 
                _quantity.value() / units::Dimensionless(1. * kilo).value()
              ).ptr() );
      }
};

/**
 * \brief Converts specific heat capacity unit to raw value, value is represented in kJ/kg*K
 */
template<>
struct UnitsToPyNum<units::SpecificEnergy>
{    
    /**
     * \brief Predefined convert method to be called from converter template
     */
    static PyObject* convert(const units::SpecificEnergy& _quantity){
        return boost::python::incref ( 
            boost::python::object ( 
                _quantity.value() / units::Dimensionless(1. * kilo).value()
              ).ptr() );
      }
};
}
}

#endif // STEAMCALC_PYTHON_UNITSTOPYNUM_H
