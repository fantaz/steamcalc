#include "basebackwarder.h"

namespace steamcalc {

    template<typename FT,  typename ST>
    BaseBackwarder<FT, ST>::BaseBackwarder ( const FirstType& _first, const SecondType& _second ) :
    m_first ( _first ),
    m_second ( _second ),
    m_pressure_scalar ( units::Pressure ( 1.*pascal ) ),
    m_temperature_scalar ( units::Temperature ( 1*kelvin ) ),
    m_density_scalar ( units::MassDensity ( 1*kilogram_per_cubic_meter ) ),
    m_tolerance ( boost::math::tools::eps_tolerance<double> ( 35 ) ),
    m_max_iterations ( 500 ) {

    }
    
template<typename FT,  typename ST>
 const boost::math::tools::eps_tolerance< double > BaseBackwarder<FT, ST>::getTolerance() {
    return m_tolerance;
    }

    template class BaseBackwarder<Property::Pressure, Property::Density>;
    template class BaseBackwarder<Property::Pressure, Property::Temperature>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificEnthalpy>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificEntropy>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificInternalEnergy>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificIsobaricHeatCapcity>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificIsochoricHeatCapacity>;
    template class BaseBackwarder<Property::Pressure, Property::SpecificVolume>;
    template class BaseBackwarder<Property::Pressure, Property::SpeedOfSound>;

    template class BaseBackwarder<Property::Temperature, Property::Pressure>;
    template class BaseBackwarder<Property::Temperature, Property::Density>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificEnthalpy>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificEntropy>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificInternalEnergy>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificIsobaricHeatCapcity>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificIsochoricHeatCapacity>;
    template class BaseBackwarder<Property::Temperature, Property::SpecificVolume>;
    template class BaseBackwarder<Property::Temperature, Property::SpeedOfSound>;

    template class BaseBackwarder<Property::Density, Property::Pressure>;
    template class BaseBackwarder<Property::Density, Property::Temperature>;
    template class BaseBackwarder<Property::Density, Property::SpecificEnthalpy>;
    template class BaseBackwarder<Property::Density, Property::SpecificEntropy>;
    template class BaseBackwarder<Property::Density, Property::SpecificInternalEnergy>;
    template class BaseBackwarder<Property::Density, Property::SpecificIsobaricHeatCapcity>;
    template class BaseBackwarder<Property::Density, Property::SpecificIsochoricHeatCapacity>;
    template class BaseBackwarder<Property::Density, Property::SpecificVolume>;
    template class BaseBackwarder<Property::Density, Property::SpeedOfSound>;
}
