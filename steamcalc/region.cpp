#include "region.h"
#include "regions.h"
#include <boost/math/tools/minima.hpp>
#include "gibbs_free_energy_index.h"
#include "ptchecker.h"
#include "boost/bind.hpp"



namespace steamcalc { // namespace begging

    template<typename T>
    std::unique_ptr<SteamProperties> Region<T>::properties ( /*const units::Pressure& _pressure, const units::Temperature& _temperature */ ) {
        //Creation of properties unique pointer
        std::unique_ptr<SteamProperties> data (
            new SteamProperties (
                pressure(),
                temperature(),
                specEnthalpy(),
                specEntropy(),
                specVolume(),
                specInterEnergy(),
                specIsobaricHeatCap(),
                specIsochoricHeatCap(),
                speedOfSound(), 
                density()
            )
        );
        return std::move ( data );
    }

// template<>
// std::unique_ptr<SteamProperties> Region<Four>::properties ( const units::Temperature& _temperature )
//     {
//
//     SaturationLine region_state;
//     //Creation of properties unique pointer
//     std::unique_ptr<SteamProperties> data (
//         new SteamProperties (
//             region_state ( _temperature ),
//             _temperature,
//             0,
//             0,
//             0,
//             0,
//             0,
//             0,
//             0
//         )
//     );
//     return std::move ( data );
//     }

    template class Region<One>;
    template class Region<Two>;
    template class Region<Three>;
// template class Region<Four>;
    template class Region<Five>;

}// end namespace


