/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Backward equation template class
 */
#ifndef STEAMCALC_BACKWARDER_H
#define STEAMCALC_BACKWARDER_H
# include "propertytraits.h"
# include "region.h"
# include "core.h"
# include "densitybackwarder.h"
# include "temperaturebackwarder.h"
# include "pressurebackwarder.h"


/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc {

/**
 * \brief Solution for backward equations provided in IAPWS-IF97 standard
 */
template<typename R, typename FT, typename ST,  size_t V, template<typename R, typename FT, typename ST, size_t V> class BackwardPolicy>                                        
class Backwarder : public BackwardPolicy<R, FT,  ST, V> {
public:
    
    using BackwardPolicy<R, FT, ST, V>::BackwardPolicy;
    
    using BackwardPolicy<R, FT, ST, V>::backward;
    
};    
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_BACKWARDER_H
