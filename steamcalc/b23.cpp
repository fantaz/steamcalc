#include "b23.h"
#include "functions.h"
#include "boost/units/cmath.hpp"

namespace steamcalc{

	B23::B23() :
		m_reducing_p(1*1e6*pascal),
		m_reducing_T(1*kelvin),
		m_lower_p_bound(16.5292*mega*pascal),
		m_higer_p_bound(100*mega*pascal),
		m_lower_T_bound(623.15*kelvin),
		m_higer_T_bound(863.15*kelvin),
		n( 
			{
				0.34805185628969e3,
				-0.11671859879975e1,
				0.10192970039326e-2, 
				0.57254459862746e3,
				0.13918839778870e2
			}
		)
	{
		/*nothing to do*/
	}

	units::Pressure B23::operator()(const units::Temperature& _input_val )
	{
		if ((_input_val<=m_lower_T_bound) && (_input_val>=m_higer_T_bound)) return 0*pascal;
		units::Dimensionless theta = functions::reduce<units::Temperature>(_input_val, m_reducing_T);
		return (n[0]+n[1]*theta+n[2]*theta*theta)*m_reducing_p;
	}

	units::Temperature B23::operator()(const units::Pressure& _input_val )
	{
		if ((_input_val<=m_lower_p_bound) && (_input_val>=m_higer_p_bound)) return 0*kelvin;
		units::Dimensionless pi = functions::reduce<units::Pressure>(_input_val, m_reducing_p);
		return ( n[3] + boost::units::sqrt( (pi-n[4])/n[2] ) ) * m_reducing_T;
	}

}