#include "steamproperties.h"

namespace steamcalc {

    SteamProperties::SteamProperties() :
    m_pressure ( 0 ),
    m_temperature ( 0 ),
    m_spec_enthalpy ( 0 ),
    m_spec_entropy ( 0 ),
    m_spec_volume ( 0 ),
    m_spec_internal_energy ( 0 ),
    m_spec_isobaric_heat_cap ( 0 ),
    m_spec_isochoric_heat_cap ( 0 ),
    m_speed_of_sound ( 0 ), 
    m_density(0)
    {
        /*invalid state*/
    }


    SteamProperties::SteamProperties ( units::Pressure
    _pressure, units::Temperature _temperature,
    units::SpecificEnergy _spec_enthalpy,
    units::SpecificHeatCapacity _spec_entropy,
    units::SpecificVolume _spec_volume, units::SpecificEnergy
    _spec_internal_energy, units::SpecificHeatCapacity
    _spec_isobaric_heat_cap, units::SpecificHeatCapacity
    _spec_isochoric_heat_cap, units::Velocity _speed_of_sound,
    units::MassDensity _density ) :
    m_pressure ( _pressure ), m_temperature ( _temperature ),
    m_spec_enthalpy ( _spec_enthalpy ), m_spec_entropy ( _spec_entropy ),
    m_spec_volume ( _spec_volume ), m_spec_internal_energy ( _spec_internal_energy ),
    m_spec_isobaric_heat_cap ( _spec_isobaric_heat_cap ),
    m_spec_isochoric_heat_cap ( _spec_isochoric_heat_cap ),
    m_speed_of_sound ( _speed_of_sound ),
    m_density ( _density ) {
        /* valid state */
    }

}
