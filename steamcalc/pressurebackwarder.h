/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Pressure backward equation header declarations
 */
#ifndef STEAMCALC_PRESSUREBACKWARDER_H
#define STEAMCALC_PRESSUREBACKWARDER_H
# include "core.h"
# include "basebackwarder.h"
# include "backwardhelper.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

/**
 * \brief Implementation for pressure backward equation according to definitions at IAPWS-IF97 standard
 * 
 * Templates parameters are:
 * R - region type
 * FT - first input type
 * ST - second input type
 * V - version of backward equation,  default set to zero
 * 
 */
template<typename R, typename FT, typename ST, size_t V = 0>
class PressureBackwarder : public BackwardHelper<R, V>, public BaseBackwarder<FT, ST>
{
public:
    
    /**
     * \brief Using BaseBackwarder ctor
     */
    using BaseBackwarder<FT, ST>::BaseBackwarder;
    
    /**
     * \brief Calculates density backward equation
     */
    units::Pressure backward();
};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_PRESSUREBACKWARDER_H
