#include "steamcalc.h"
#include "region.h"
#include "steamproperties.h"
#include "regions.h"
#include "ptchecker.h"
#include "backwarder.h"
#include "boost/math/tools/roots.hpp"
#include "b2bc.h"

namespace steamcalc { // namespace begging

    Steamcalc::Steamcalc() {
        m_properties = std::unique_ptr<SteamProperties> ( new SteamProperties() );
    }


    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {

        /*#if STEAMCALC_VERSION >=  CURRENT_STEAMCALC_VERSION

                std::cout << "nesto" << std::endl;

        #endif  */

        m_properties = createRegion ( _pressure,  _temperature );
    }

    Steamcalc::Steamcalc ( const units::MassDensity& _density, const units::Temperature& _temperature ) {
        check<Property::Density> ( _temperature, _density );
    }

    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::SpecificVolume& _spec_volume ) {
        check<Property::SpecificVolume> ( _pressure, _spec_volume );
    }

    Steamcalc::Steamcalc ( const units::SpecificHeatCapacity& _spec_isobaric_heat_cap, const units::Temperature& _temperature ) {
        check<Property::SpecificIsobaricHeatCapcity> ( _temperature, _spec_isobaric_heat_cap );
    }

    Steamcalc::Steamcalc ( const units::SpecificEnergy& _spec_internl_energy, const units::Temperature& _temperature ) {
        check<Property::SpecificInternalEnergy> ( _temperature,  _spec_internl_energy );
    }

    Steamcalc::Steamcalc ( const units::Temperature& _temperature, const units::Velocity& _speed_of_sound ) {
        check<Property::SpeedOfSound> ( _temperature,  _speed_of_sound );
    }

    Steamcalc::Steamcalc ( const units::Temperature& _temperature, const units::SpecificVolume& _spec_volume ) {
        check<Property::SpecificVolume> ( _temperature,  _spec_volume );
    }

    Steamcalc::Steamcalc ( const units::Temperature& _temperature, const units::SpecificHeatCapacity& _spec_entropy ) {
        check<Property::SpecificEntropy> ( _temperature,  _spec_entropy );
    }

    Steamcalc::Steamcalc ( const units::Temperature& _temperature, const units::SpecificEnergy& _spec_enthalpy ) {
        check<Property::SpecificEnthalpy> ( _temperature,  _spec_enthalpy );
    }

    Steamcalc::Steamcalc ( const units::SpecificHeatCapacity& _spec_isobaric_heat_cap, const units::Pressure& _pressure ) {
        check<Property::SpecificIsobaricHeatCapcity> ( _pressure,  _spec_isobaric_heat_cap );
    }

    Steamcalc::Steamcalc ( const units::SpecificEnergy& _spec_internl_energy, const units::Pressure& _pressure ) {
        check<Property::SpecificInternalEnergy> ( _pressure,  _spec_internl_energy );
    }

    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::Velocity& _speed_of_sound ) {
        check<Property::SpeedOfSound> ( _pressure,  _speed_of_sound );
    }

    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::MassDensity& _density ) {
        check<Property::Density> ( _pressure,  _density );
    }

    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::SpecificHeatCapacity& _spec_entropy ) {
        units::Temperature temperature ( 0 );
        PTChecker<One> one ( temperature );
        PTChecker<Two> two ( temperature );
        PTChecker<Three> three ( temperature, _pressure );
        PTChecker<Five> five;
        B2BC b2bc;
        if ( one.check ( _pressure ) ) {
            temperature = Backwarder<One, Property::Pressure, Property::SpecificEntropy,  0,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            if ( recheck<Property::SpecificEntropy> ( _spec_entropy, _pressure, temperature ) ) return;
        }
        if ( two.check ( _pressure ) ) {
            if ( _pressure < units::Pressure ( 4*mega*pascals ) ) {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEntropy,  0,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            } else if ( _spec_entropy >= units::SpecificHeatCapacity ( 5.85 * kilo * joules_per_kilogram_and_kelvin ) ) {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEntropy,  1,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            } else {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEntropy,  2,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            }
            if ( recheck<Property::SpecificEntropy> ( _spec_entropy, _pressure, temperature ) ) return;
        }
        if ( three.check ( _pressure ) ) {
            temperature = Backwarder<Three, Property::Pressure, Property::SpecificEntropy,  0,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            if ( recheck<Property::SpecificEntropy> ( _spec_entropy, _pressure, temperature ) ) return;
        }
        if ( five.check ( _pressure ) ) {
            temperature = Backwarder<Five, Property::Pressure, Property::SpecificEntropy,  0,  TemperatureBackwarder> ( _pressure, _spec_entropy ).backward();
            if ( recheck<Property::SpecificEntropy> ( _spec_entropy, _pressure, temperature ) ) return;
        }
    }

    Steamcalc::Steamcalc ( const units::Pressure& _pressure, const units::SpecificEnergy& _spec_enthalpy ) {
        units::Temperature temperature ( 0 );
        PTChecker<One> one ( temperature );
        PTChecker<Two> two ( temperature );
        PTChecker<Three> three ( temperature, _pressure );
        PTChecker<Five> five;
        B2BC b2bc;
        units::Pressure lower_p = b2bc ( _spec_enthalpy );
        units::SpecificEnergy lower_h = b2bc ( _pressure );
        if ( one.check ( _pressure ) ) {
            temperature = Backwarder<One, Property::Pressure, Property::SpecificEnthalpy, 0, TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            if ( recheck<Property::SpecificEnthalpy> ( _spec_enthalpy, _pressure, temperature ) ) return;
        }
        if ( two.check ( _pressure ) ) {
            if ( _pressure < units::Pressure ( 4*mega*pascals ) ) {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEnthalpy,  0,  TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            } else if ( _pressure <=  lower_p && _spec_enthalpy >= lower_h ) {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEnthalpy,  1,  TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            } else {
                temperature = Backwarder<Two, Property::Pressure, Property::SpecificEnthalpy,  2,  TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            }
            if ( recheck<Property::SpecificEnthalpy> ( _spec_enthalpy, _pressure, temperature ) ) return;
        }
        if ( three.check ( _pressure ) ) {
            temperature = Backwarder<Three, Property::Pressure, Property::SpecificEnthalpy,  0,  TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            if ( recheck<Property::SpecificEnthalpy> ( _spec_enthalpy, _pressure, temperature ) ) return;
        }
        if ( five.check ( _pressure ) ) {
            temperature = Backwarder<Five, Property::Pressure, Property::SpecificEnthalpy,  0,  TemperatureBackwarder> ( _pressure, _spec_enthalpy ).backward();
            if ( recheck<Property::SpecificEnthalpy> ( _spec_enthalpy, _pressure, temperature ) ) return;
        }
    }

    units::Pressure Steamcalc::pressure() {
        return m_properties->m_pressure;
    }

    units::Temperature Steamcalc::temperature() {
        return m_properties->m_temperature;
    }

    units::SpecificEnergy Steamcalc::specificEnthalpy() {
        return m_properties->m_spec_enthalpy;
    }

    units::SpecificHeatCapacity Steamcalc::specificEntropy() {
        return m_properties->m_spec_entropy;
    }

    units::SpecificEnergy Steamcalc::specificInternalEnergy() {
        return m_properties->m_spec_internal_energy;
    }

    units::SpecificHeatCapacity Steamcalc::specificIsobaricHeatCapacity() {
        return m_properties->m_spec_isobaric_heat_cap;
    }

    units::SpecificHeatCapacity Steamcalc::specificIsochoricHeatCapacity() {
        return m_properties->m_spec_isochoric_heat_cap;
    }

    units::Velocity Steamcalc::speedOfSound() {
        return m_properties->m_speed_of_sound;
    }

    units::SpecificVolume Steamcalc::specificVolume() {
        return m_properties->m_spec_volume;
    }

    units::MassDensity Steamcalc::density() {
        return m_properties->m_density;
    }

    std::unique_ptr< SteamProperties > Steamcalc::createRegion ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        if ( PTChecker<One> ( _temperature ).check ( _pressure ) && PTChecker<One> ( _temperature ).check ( _temperature ) ) {
            return Region<One> ( _pressure, _temperature ).properties ();
        }
        if ( PTChecker<Three> ( _temperature, _pressure ).check ( _pressure ) && PTChecker<Three> ( _temperature, _pressure ).check ( _temperature ) ) {
            return Region<Three> ( Backwarder<Three, Property::Temperature, Property::Pressure, 0,  DensityBackwarder> ( _temperature, _pressure ).backward(), _temperature ).properties ();
        }
        if ( PTChecker<Two> ( _temperature ).check ( _pressure ) && PTChecker<Two> ( _temperature ).check ( _temperature ) ) {
            return Region<Two> ( _pressure, _temperature ).properties ();
        }
//         if ( PTChecker<Four> ( ).check ( _pressure ) && PTChecker<Four> ( ).check ( _temperature ) ) {
//             return Region<Four> ( _pressure, _temperature ).properties ();
//           }
        if ( PTChecker<Five> ( ).check ( _pressure ) && PTChecker<Five> ( ).check ( _temperature ) ) {
            return Region<Five> ( _pressure, _temperature ).properties ();
        }

        return std::move ( std::unique_ptr<SteamProperties> ( new SteamProperties() ) );
    }

    template<typename ST, typename T>
    void Steamcalc::check ( const units::Pressure& _pressure,  const T& _second ) {
        units::Temperature temperature ( 0 );
        PTChecker<One> one ( temperature );
        PTChecker<Two> two ( temperature );
        PTChecker<Three> three ( temperature, _pressure );
        PTChecker<Five> five;
        if ( one.check ( _pressure ) ) {
            temperature = Backwarder<One, Property::Pressure, ST,  0,  TemperatureBackwarder> ( _pressure, _second ).backward();
            if ( recheck<ST> ( _second, _pressure, temperature ) ) return;
        }
        if ( two.check ( _pressure ) ) {
            temperature = Backwarder<Two, Property::Pressure, ST,  0,  TemperatureBackwarder> ( _pressure, _second ).backward();
            if ( recheck<ST> ( _second, _pressure, temperature ) ) return;
        }
        if ( three.check ( _pressure ) ) {
            temperature = Backwarder<Three, Property::Pressure, ST,  0,  TemperatureBackwarder> ( _pressure, _second ).backward();
            if ( recheck<ST> ( _second, _pressure, temperature ) ) return;
        }
        if ( five.check ( _pressure ) ) {
            temperature = Backwarder<Five, Property::Pressure, ST,  0,  TemperatureBackwarder> ( _pressure, _second ).backward();
            if ( recheck<ST> ( _second, _pressure, temperature ) ) return;
        }
        return ;
    }

    template<typename ST, typename T>
    void Steamcalc::check ( const units::Temperature& _temperature,  const T& _second )  {
        units::Pressure pressure ( 0 );
        PTChecker<One> one ( _temperature );
        PTChecker<Two> two ( _temperature );
        PTChecker<Three> three ( _temperature, pressure );
        PTChecker<Five> five;
        if ( one.check ( _temperature ) ) {
            pressure = Backwarder<One, Property::Temperature, ST,  0,  PressureBackwarder> ( _temperature, _second ).backward();
            if ( recheck<ST> ( _second, pressure, _temperature ) ) return;
        }
        if ( two.check ( _temperature ) ) {
            pressure = Backwarder<Two, Property::Temperature, ST,  0,  PressureBackwarder> ( _temperature, _second ).backward();
            if ( recheck<ST> ( _second, pressure, _temperature ) ) return;
        }
        if ( five.check ( _temperature ) ) {
            pressure = Backwarder<Five, Property::Temperature, ST,  0,  PressureBackwarder> ( _temperature, _second ).backward();
            if ( recheck<ST> ( _second, pressure, _temperature ) ) return;
        }
        if ( _temperature > three.getTemperatureBounds().first ) {
            pressure = Backwarder<Three, Property::Temperature, ST,  0,  PressureBackwarder> ( _temperature, _second ).backward();
            if ( recheck<ST> ( _second, pressure, _temperature ) ) return;
        }
    }

    template<typename ST, typename T>
    bool Steamcalc::recheck ( const T& _value, const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        // calculate properties with provided values
        m_properties = createRegion ( _pressure, _temperature );
        // Initiate eps tolerance
        boost::math::tools::eps_tolerance<double> eps_tolerance ( 15 );
        if ( eps_tolerance ( _value.value(), PropertyGetter<ST>().getProperty ( m_properties ).value() ) == false ) {
            // delete inserted properties
            m_properties = std::unique_ptr<SteamProperties> ( new SteamProperties() );
            return false;
        }
        return true;
    }

    template<>
    units::SpecificEnergy Steamcalc::PropertyGetter<Property::SpecificEnthalpy, units::SpecificEnergy>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_enthalpy;
    }

    template<>
    units::SpecificHeatCapacity Steamcalc::PropertyGetter<Property::SpecificEntropy,  units::SpecificHeatCapacity>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_entropy;
    }
    template<>
    units::SpecificEnergy Steamcalc::PropertyGetter<Property::SpecificInternalEnergy, units::SpecificEnergy>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_internal_energy;
    }
    template<>
    units::SpecificHeatCapacity Steamcalc::PropertyGetter<Property::SpecificIsobaricHeatCapcity,  units::SpecificHeatCapacity>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_isobaric_heat_cap;
    }
    template<>
    units::SpecificHeatCapacity Steamcalc::PropertyGetter<Property::SpecificIsochoricHeatCapacity,  units::SpecificHeatCapacity>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_isochoric_heat_cap;
    }
    template<>
    units::Velocity Steamcalc::PropertyGetter<Property::SpeedOfSound,  units::Velocity>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_speed_of_sound;
    }
    template<>
    units::SpecificVolume Steamcalc::PropertyGetter<Property::SpecificVolume,  units::SpecificVolume>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_spec_volume;
    }
    template<>
    units::MassDensity Steamcalc::PropertyGetter<Property::Density,  units::MassDensity>::getProperty ( const std::unique_ptr<SteamProperties>& _properties ) {
        return _properties->m_density;
    }

}                                                           // namespace end
