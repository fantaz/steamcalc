/*
 *
 */

#include "ptchecker.h"
#include "region.h"
#include <iostream>
# include "regions/one.h"
# include "regions/two.h"
# include "regions/three.h"
# include "regions/saturationline.h"
# include "regions/five.h"
#include "b23.h"


namespace steamcalc {

    template<>
    PTChecker<One>::PTChecker (
        const units::Temperature& _temperature,
        const units::Pressure& _pressure
    ) :
    m_press_bounds ( {{SaturationLine().operator() ( _temperature ), ( 100. * 1e6 * pascal ) }} ),
    m_temp_bounds ( {{ (273.15*kelvin),(623.15*kelvin)}} ) {
    }

    template<>
    PTChecker<Two>::PTChecker (
        const units::Temperature& _temperature,
        const units::Pressure& _pressure
    ) :
    m_press_bounds ( {
        {0. * pascal,SaturationLine().operator() ( _temperature ) },
        {0. * pascal, B23().operator() ( _temperature ) },
        {0. * pascal,100.*1e6*pascals},

    } ),
    m_temp_bounds ( {
        {273.15*kelvin,623.15*kelvin},
        {623.15*kelvin,863.15*kelvin},
        {863.15*kelvin,1073.15*kelvin}

    } ) {
    }

    template<>
    PTChecker<Three>::PTChecker (
        const units::Temperature& _temperature,
        const units::Pressure& _pressure
    ) :
    m_press_bounds (
    {
        {
            ( B23().operator() ( _temperature ) ), //we're getting press here by B23 equation
            ( 100.*1e6* pascal )
        }
    }
    ),
    m_temp_bounds (
    {
        {
            ( 623.15*kelvin ),
            ( B23().operator() ( _pressure ) ) //we're getting temp here by B23 equation
        }
    }
    ) {

    }

    template<>
    PTChecker<Four>::PTChecker (
        const units::Temperature& _temperature,
        const units::Pressure& _pressure
    ) :
    m_press_bounds ( {{611.213*pascal, 22064000 * pascal}} ),
    m_temp_bounds ( {{273.15*kelvin,647.096*kelvin}} ) {

    }


    template<>
    PTChecker<Five>::PTChecker (
        const units::Temperature& _temperature,
        const units::Pressure& _pressure
    ) :
    m_press_bounds ( {{0.*pascal, ( 50.* 1e6 * pascal ) }} ),
    m_temp_bounds ( {{1073.15*kelvin,2273.15*kelvin}} ) {
    }


    //////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////CHECK METHODS////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
    bool PTChecker<T>::check ( const units::Pressure& _pressure ) {
        return ( _pressure >= m_press_bounds[0].first ) == ( _pressure <= m_press_bounds[0].second ) ? true : false;
    }

    template<>
    bool PTChecker<Two>::check ( const units::Pressure& _pressure ) {
        if ( ( _pressure > m_press_bounds[0].first ) && ( _pressure <= m_press_bounds[0].second ) ) return true;
        if ( ( _pressure > m_press_bounds[1].first ) && ( _pressure <= m_press_bounds[1].second ) ) return true;
        if ( ( _pressure > m_press_bounds[2].first ) && ( _pressure <= m_press_bounds[2].second ) ) return true;
        return false;
    }

    template<>
    bool PTChecker<Five>::check ( const units::Pressure& _pressure ) {
        return ( _pressure > m_press_bounds[0].first ) && ( _pressure <= m_press_bounds[0].second ) ? true : false;
    }

    template<typename T>
    bool PTChecker<T>::check ( const units::Temperature& _temperature ) {
        return ( _temperature >= m_temp_bounds[0].first ) && ( _temperature <= m_temp_bounds[0].second ) ? true : false;
    }

    template<>
    bool PTChecker<Two>::check ( const units::Temperature& _temperature ) {
        if ( ( _temperature >= m_temp_bounds[0].first ) && ( _temperature <= m_temp_bounds[0].second ) ) return true;
        if ( ( _temperature > m_temp_bounds[1].first ) && ( _temperature <= m_temp_bounds[1].second ) ) return true;
        if ( ( _temperature > m_temp_bounds[2].first ) && ( _temperature <= m_temp_bounds[2].second ) ) return true;
        return false;
    }

    template<typename T>
    bool PTChecker<T>::check ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        return check ( _pressure ) && check ( _temperature ) ? true : false;
    }

    //Specialized check method for region Two
    template<>
    bool PTChecker<Two>::check ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        if ( ( ( _pressure > m_press_bounds[0].first ) && ( _pressure <= m_press_bounds[0].second ) ) && ( ( _temperature >= m_temp_bounds[0].first ) && ( _temperature <= m_temp_bounds[0].second ) ) ) return true;
        if ( ( ( _pressure > m_press_bounds[1].first ) && ( _pressure <= m_press_bounds[1].second ) ) && ( ( _temperature > m_temp_bounds[1].first ) && ( _temperature <= m_temp_bounds[1].second ) ) ) return true;
        if ( ( ( _pressure > m_press_bounds[2].first ) && ( _pressure <= m_press_bounds[2].second ) ) && ( ( _temperature > m_temp_bounds[2].first ) && ( _temperature <= m_temp_bounds[2].second ) ) ) return true;
        return false;
    }

    //Special check for region 4
    //Need to implement algorithm |x-y|<=t*x| && |x-y|<=|t*y| where x,y are tested values and t is tolerance
    template<>
    bool PTChecker<Four>::check ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        if ( check ( _pressure ) && check ( _temperature ) ) {
            SaturationLine sl;
            units::Pressure sl_press = sl ( _temperature );
            units::Temperature sl_temp = sl ( _pressure );
            if ( ( _pressure == sl_press ) && ( _temperature == sl_temp ) ) return true;
        }
        return false;
    }

    template<>
    bool PTChecker<Five>::check ( const units::Pressure& _pressure, const units::Temperature& _temperature ) {
        return check ( _pressure ) && check ( _temperature ) ? true : false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////GET METHODS////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
std::pair< units::Pressure, units::Pressure > PTChecker<T>::getPressureBounds( const units::Temperature& _temperature ) const
    {
        return m_press_bounds[0];
    }

    template<>
    std::pair< units::Pressure, units::Pressure > PTChecker<Two>::getPressureBounds ( const units::Temperature& _temperature ) const
    {
        if ( ( _temperature > m_temp_bounds[0].first ) && ( _temperature <= m_temp_bounds[0].second ) ) return m_press_bounds[0];
        if ( ( _temperature > m_temp_bounds[1].first ) && ( _temperature <= m_temp_bounds[1].second ) ) return m_press_bounds[1];
        if ( ( _temperature > m_temp_bounds[2].first ) && ( _temperature <= m_temp_bounds[2].second ) ) return m_press_bounds[2];
        return std::make_pair ( units::Pressure ( 0*pascal ),units::Pressure ( 0*pascal ) );
    }

    template<typename T>
std::pair<units::Temperature,units::Temperature> PTChecker<T>::getTemperatureBounds( const units::Pressure& _pressure ) const
    {
        return m_temp_bounds[0];
    }

    template<>
    std::pair<units::Temperature,units::Temperature> PTChecker<Two>::getTemperatureBounds ( const units::Pressure& _pressure ) const
    {
        if ( ( _pressure > m_press_bounds[0].first ) && ( _pressure <= m_press_bounds[0].second ) ) return m_temp_bounds[0];
        if ( ( _pressure > m_press_bounds[1].first ) && ( _pressure <= m_press_bounds[1].second ) ) return m_temp_bounds[1];
        if ( ( _pressure > m_press_bounds[2].first ) && ( _pressure <= m_press_bounds[2].second ) ) return m_temp_bounds[2];
        return std::make_pair ( units::Temperature ( 0*kelvin ),units::Temperature ( 0*kelvin ) );
    }



    template class PTChecker<One>;
    template class PTChecker<Two>;
    template class PTChecker<Three>;
    template class PTChecker<Four>;
    template class PTChecker<Five>;
}


