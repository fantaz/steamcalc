/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Temperature backward equation solution
 */
#ifndef STEAMCALC_TEMPERATUREBACKWARDER_H
# define STEAMCALC_TEMPERATUREBACKWARDER_H
# include "core.h"
# include "basebackwarder.h"
# include "backwardhelper.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

/**
 * \brief Temperature backward equation declaration
 * 
 * \param R - region template parameter
 * \param FT - first property type template parameter
 * \param ST - second property type template parameter
 * \param V - verison of equation template value parameter
 */
template<typename R, typename FT, typename ST, size_t V = 0>
class TemperatureBackwarder : public BackwardHelper<R, V>, public BaseBackwarder<FT, ST>
{
public:
    using BaseBackwarder<FT, ST>::BaseBackwarder;
    
    /**
     * \brief Backward equation
     * 
     * \return temperature value
     */
    units::Temperature backward();
};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_TEMPERATUREBACKWARDER_H
