/*
 *
 */
#include "two.h"
#include "../gibbs_free_energy_index.h"
#include "one.h"
#include "../ptchecker.h"
#include "saturationline.h"

namespace steamcalc {
    Two::Two ( const units::Pressure& _pressure, const units::Temperature& _temperature ) :
    m_pressure ( _pressure ),
    m_temperature ( _temperature ),
    m_reducing_pressure ( 1*mega*pascals ),
    m_reducing_temperature ( 540*kelvin ),
    m_pressure_t(611.657*pascal),
    m_pressure_metastable_line(10 * mega * pascals),
    m_spec_enthalpy_t_liquid(0.611783 * joules_per_kilogram),
    m_x(0.5),
    m_R ( 0.461526*kilo*joules/ ( kelvin*kilogram ) ),
    m_calculated() {
        m_pi = functions::reduce ( m_pressure,  m_reducing_pressure );
        m_tau = functions::reduce_inversed ( m_temperature,  m_reducing_temperature );
        // Pressure borders are predefined constant values,  temperature borders need to be calculated
        if ( m_pressure > m_pressure_t && m_pressure < m_pressure_metastable_line) {
            // We need to get h' and h'',  h' is assumed to be ht(h at triple point,  which is border value of metastable state)
            // h'' is calculated by region one where pressure = m_pressure and temperature is temperature at saturated line
            units::SpecificEnergy h_vapor= One( m_pressure, SaturationLine().backward(m_pressure) ).specEnthalpy();
            if ( h_vapor < functions::saturatedVapor(m_spec_enthalpy_t_liquid, m_x)) {
                // Here we set coefficients values according to metastable state
                m_ideal_coefficients = gfe_ideal_rtwo_mstable;
                m_residual_coefficients = gfe_residual_rtwo_mstable;
            }
        }
        m_ideal_coefficients = gfe_ideal_rtwo;
        m_residual_coefficients = gfe_residual_rtwo;
        
    }

    units::Pressure Two::pressure() {
        return m_pressure;
    }

    units::Temperature Two::temperature() {
        return m_temperature;
    }

    units::SpecificVolume Two::specVolume() {
        return ( m_pi* ( gammaIPi() + gammaRPi() ) ) * m_R * m_temperature / m_pressure /* meter * meter * meter / joule*/;

    }

    units::SpecificEnergy Two::specInterEnergy() {
        return ( ( m_tau * ( gammaITau() + gammaRTau() ) ) - ( m_pi* ( gammaIPi() + gammaRPi() ) ) ) * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Two::specEntropy() {
        return ( ( m_tau * ( gammaITau() + gammaRTau() ) ) - ( gammaI() + gammaR() ) ) * m_R;
    }

    units::SpecificEnergy Two::specEnthalpy() {
        return ( m_tau * ( gammaITau() + gammaRTau() ) ) * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Two::specIsobaricHeatCap() {
        return ( ( -1 ) * pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() ) ) * m_R;
    }

    units::SpecificHeatCapacity Two::specIsochoricHeatCap() {
        return ( ( ( -1 ) * pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() ) ) - ( pow ( ( 1+m_pi*gammaRPi()-m_pi*m_tau*gammaRPiTau() ), 2 ) / ( 1 - pow ( m_pi, 2 ) * gammaRPiPi() ) ) ) * m_R;
    }

    units::Velocity Two::speedOfSound() {
        return sqrt (
            (
                (
                    1 + 2 * m_pi * gammaRPi() + pow ( m_pi, 2 ) * pow ( gammaRPi(), 2 )
                )
                / (
                    (
                        1 - pow ( m_pi, 2 ) * gammaRPiPi()
                    )
                    + (
                        pow ( ( 1 + m_pi * gammaRPi() - m_pi * m_tau * gammaRPiTau() ),  2 )
                        / ( pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() ) )
                    )
                )
            )
            * m_R * m_temperature
        );
    }

    units::MassDensity Two::density() {
        return 1./specVolume();
    }

    units::Dimensionless Two::gammaI() {
        if ( m_calculated.find ( "gi" ) ==m_calculated.end() ) {
            m_calculated["gi"] = log ( m_pi ) + functions::sum(m_ideal_coefficients, 
                [&] (
                    units::Dimensionless init,
                    const std::array<double,2>& item
            ) {
                return init+=item[1] * pow ( m_tau, item[0] );
            }
            );
        }
        return m_calculated["gi"];
    }


    units::Dimensionless Two::gammaIPi() {
        return 1 / m_pi;
    }

    units::Dimensionless Two::gammaIPiPi() {
        return -1/pow ( m_pi, 2 );
    }

    units::Dimensionless Two::gammaITau() {
        if ( m_calculated.find ( "git" ) ==m_calculated.end() ) {
            m_calculated["git"] = functions::sum(m_ideal_coefficients, [&] (
                                          units::Dimensionless init,
                                          const std::array<double,2>& item
            ) {
                return init+=item[1] * item[0] * pow ( m_tau, item[0]-1 );
            }
                                  );
        }
        return m_calculated["git"];
    }


    units::Dimensionless Two::gammaITauTau() {
        if ( m_calculated.find ( "gitt" ) ==m_calculated.end() ) {
            m_calculated["gitt"] = functions::sum(m_ideal_coefficients, 
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,2>& item
            ) {
                return init+=item[1] * item[0] * ( item[0] -1 ) * pow ( m_tau, item[0]-2 );
            }
                                   );
        }
        return m_calculated["gitt"];
    }


    units::Dimensionless Two::gammaIPiTau() {
        return 0;
    }

    units::Dimensionless Two::gammaR() {
        if ( m_calculated.find ( "gr" ) ==m_calculated.end() ) {
            m_calculated["gr"] = functions::sum(m_residual_coefficients, 
                                     [&] (
                                         units::Dimensionless init,
                                         const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *pow ( m_tau - 0.5, item[1] );
            } );
        }
        return m_calculated["gr"];
    }

    units::Dimensionless Two::gammaRPi() {
        if ( m_calculated.find ( "grp" ) ==m_calculated.end() ) {
            m_calculated["grp"] = functions::sum(m_residual_coefficients, 
                                      [&] (
                                          units::Dimensionless init,
                                          const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] *pow ( m_pi, item[0]-1 ) *pow ( m_tau - 0.5, item[1] );
            } );
        }
        return m_calculated["grp"];
    }

    units::Dimensionless Two::gammaRPiPi() {
        if ( m_calculated.find ( "grpp" ) ==m_calculated.end() ) {
            m_calculated["grpp"] = functions::sum(m_residual_coefficients,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] * ( item[0] - 1 ) *pow ( m_pi, item[0]-2 ) *pow ( m_tau - 0.5, item[1] );
            } );
        }
        return m_calculated["grpp"];
    }

    units::Dimensionless Two::gammaRTau() {
        if ( m_calculated.find ( "grt" ) ==m_calculated.end() ) {
            m_calculated["grt"] = functions::sum(m_residual_coefficients,
                                      [&] (
                                          units::Dimensionless init,
                                          const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *item[1] *pow ( m_tau - 0.5, item[1]-1 );
            } );
        }
        return m_calculated["grt"];
    }

    units::Dimensionless Two::gammaRTauTau() {
        if ( m_calculated.find ( "grtt" ) ==m_calculated.end() ) {
            m_calculated["grtt"] = functions::sum(m_residual_coefficients,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *item[1]* ( item[1]-1 ) *pow ( m_tau - 0.5, item[1]-2 );
            } );
        }
        return m_calculated["grtt"];
    }

    units::Dimensionless Two::gammaRPiTau() {
        if ( m_calculated.find ( "grpt" ) ==m_calculated.end() ) {
            m_calculated["grpt"] = functions::sum(m_residual_coefficients,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] *pow ( m_pi, item[0]-1 ) *item[1] *pow ( m_tau - 0.5, item[1]-1 );
            } );
        }
        return m_calculated["grpt"];
    }

}
