#include "one.h"
#include "../gibbs_free_energy_index.h"

namespace steamcalc {
	
	
	One::One(const units::Pressure& _pressure, const units::Temperature& _temperature) 
	: 	
		m_pressure(_pressure),
		m_temperature(_temperature),
		m_reducing_pressure(16.53 * mega * pascal ), 
		m_reducing_temperature(1386 * kelvin ),
		m_pressure_t(611.657*pascal),
		m_temperature_t(273.16*kelvin),
		m_spec_enthalpy_t(0.611783*joule/kilogramme),
		m_spec_entropy_t(0*joule/kilogramme/kelvin),
		m_spec_intern_energ_t(0*joules/kilogramme),
		m_R(0.461526*kilo*joules/(kelvin*kilogram)),
		m_calculated()
	{
		m_pi=functions::reduce<units::Pressure>(_pressure, m_reducing_pressure);
		m_tau = functions::reduce_inversed<units::Temperature>(_temperature, m_reducing_temperature);
	}

	units::Pressure One::pressure()
	{
		return m_pressure;
	}

	units::Temperature One::temperature()
	{
		return m_temperature;
	}

	units::SpecificVolume One::specVolume()
	{
		units::SpecificVolume tmp = 0;
	    tmp = m_pi*gammaPi()*((m_R*m_temperature)/m_pressure)*meter*meter*meter/joule*pascal;
		return tmp;
	}
	//Ovo je ok
	units::SpecificEnergy One::specInterEnergy()
	{
		if ((m_pressure == m_pressure_t) && (m_temperature == m_temperature_t)) return m_spec_intern_energ_t;
		units::SpecificEnergy tmp = 0;
		tmp = (m_tau*gammaTau()-m_pi*gammaPi())*m_R*m_temperature;
		return tmp;
	}

	units::SpecificHeatCapacity One::specEntropy()
	{
		if ((m_pressure == m_pressure_t) && (m_temperature == m_temperature_t)) return m_spec_entropy_t;
		units::SpecificHeatCapacity tmp = 0;
	    tmp = (m_tau*gammaTau()-gamma())*m_R;
		return tmp;
	}

	units::SpecificEnergy One::specEnthalpy()
	{
		if ((m_pressure == m_pressure_t) && (m_temperature == m_temperature_t)) return m_spec_enthalpy_t;
		units::SpecificEnergy tmp = 0;
	    tmp = m_tau*gammaTau()*m_R*m_temperature;
		return tmp;
	}

	units::SpecificHeatCapacity One::specIsobaricHeatCap()
	{
		units::SpecificHeatCapacity tmp = 0;
	    tmp = -pow((m_tau),2)*gammaTauTau()*m_R;
		return tmp;
	}

	units::SpecificHeatCapacity One::specIsochoricHeatCap()
	{

		units::SpecificHeatCapacity tmp = 0;
	    units::Dimensionless prvi_clan = -1 * pow(m_tau,2) * gammaTauTau();
	    units::Dimensionless drugi_clan_brojnik=  gammaPi() - (m_tau * gammaPiTau());
	    units::Dimensionless drugi_clan_nazivnik= gammaPiPi();
	    tmp = (prvi_clan + ( pow(drugi_clan_brojnik,2) / drugi_clan_nazivnik)) * m_R;
		return tmp;
	}

	units::Velocity One::speedOfSound()
	{
		units::Velocity tmp=0;
	    units::Dimensionless brojnik =  pow(gammaPi(),2) ;
	    units::Dimensionless nazivnik_brojnik =  gammaPi()-(m_tau*gammaPiTau() );
	    units::Dimensionless nazivnik_nazivnik = pow(m_tau, 2) * gammaTauTau();
	    units::Dimensionless nazivnik = pow( nazivnik_brojnik, 2) / nazivnik_nazivnik - gammaPiPi();
	    tmp = sqrt( ( brojnik / nazivnik ) * m_R * m_temperature ); //* (kilogram * meter )/ (joule * seconds)
		return tmp;
	}
	
    units::MassDensity One::density() {
        return 1./specVolume();
    }

	
	//Basic method derivatives
	
	units::Dimensionless One::gamma()
	{
		if (m_calculated.find("g")==m_calculated.end()){
			m_calculated["g"] = functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
			{  
				return init += item[2]*pow(( 7.1 - m_pi), item[0])*pow(( m_tau - 1.222), item[1]);
			});
		}
		return m_calculated["g"];
	}

	units::Dimensionless One::gammaPi()
	{		//gammaPi
		if (m_calculated.find("g_p")==m_calculated.end()){
			m_calculated["g_p"] = functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
		{ 
			return init+= -1*item[2] * item[0] * pow((7.1-m_pi),(item[0]-1)) * pow((m_tau-1.222),item[1]);
		});
		}
		return m_calculated["g_p"];
	}

	units::Dimensionless One::gammaPiPi()
	{
		if (m_calculated.find("g_pp")==m_calculated.end()){
			m_calculated["g_pp"] = functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
			{ 
				return init+=item[2]*item[0]*(item[0]-1)*pow((7.1-m_pi), (item[0]-2))*pow((m_tau-1.222),item[1]);
			});
		}
		return m_calculated["g_pp"];
	}

	units::Dimensionless One::gammaTau()
	{
		if (m_calculated.find("g_t")==m_calculated.end()){
			m_calculated["g_t"]= functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
			{ 
				return init+=item[2]*pow((7.1-m_pi), item[0])*item[1]*pow((m_tau-1.222),(item[1]-1));
			});
		}
			return m_calculated["g_t"];
	}

	units::Dimensionless One::gammaTauTau()
	{
		if (m_calculated.find("g_tt")==m_calculated.end()){
			m_calculated["g_tt"] = functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
			{ 
				return init+=item[2] * pow( (7.1 - m_pi), item[0]) * item[1] * ( item[1] - 1) * pow( (m_tau-1.222), (item[1] - 2));
			});
		}
		return m_calculated["g_tt"];
	}

	units::Dimensionless One::gammaPiTau()
	{
		if (m_calculated.find("g_pt")==m_calculated.end()){
			m_calculated["g_pt"] = functions::sum(gfe_rone, [&](units::Dimensionless init, const std::array<double,3>& item)
			{ 
				return init+=(-item[2])*item[0]*pow((7.1-m_pi), (item[0]-1))*item[1]*pow((m_tau-1.222),(item[1]-1));
			});
		}
		return m_calculated["g_pt"];
	}
		
}//End namespace steamcalc