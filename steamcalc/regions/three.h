#ifndef STEAMCALC_THREE_H
#define STEAMCALC_THREE_H
#include "../core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

class Three
{
public:
    /**
     * \brief Constructor
     */
    Three(const units::MassDensity& _density, const units::Temperature& _temperature);

// protected:
    /**
     * \brief Get pressure value
     */
    units::Pressure pressure();

    /**
    * \brief Get temperature value
    */
    units::Temperature temperature();
    /**
     * \brief Calculates specific volume property for region three
     */
    units::SpecificVolume specVolume();
    /**
     * \brief Calculates specific internal energy property for region three
     */
    units::SpecificEnergy specInterEnergy ();

    /**
     * \brief Calculates specific entropy property for region three
     */
    units::SpecificHeatCapacity specEntropy();

    /**
     * \brief Calculates specific enthalpy property for region three
     */
    units::SpecificEnergy specEnthalpy();

    /**
     * \brief Calculates specific isobaric heat capacity property for region three
     */
    units::SpecificHeatCapacity specIsobaricHeatCap();

    /**
     * \brief Calculates specific isochoric heat capacity property for region three
     */
    units::SpecificHeatCapacity specIsochoricHeatCap();

    /**
     * \brief Calculates speed of sound property for region three
     */
    units::Velocity speedOfSound();
    
    /**
     * \brief Calculates density property for region three
     */
    units::MassDensity density();

private:
    //Basic methods

    /**
     * \brief Free Gibbs Energy derivative gamma(pi,tau)
     */
    units::Dimensionless p();
    /**
     * \brief Free Gibbs Energy derivative gamma_pi(pi,tau)
     */
    units::Dimensionless pDelta();
    /**
     * \brief Free Gibbs Energy derivative gamma_pi,pi(pi,tau)
     */
    units::Dimensionless pDeltaDelta();
    /**
     * \brief Free Gibbs Energy derivative gamma_tau(pi,tau)
     */
    units::Dimensionless pTau();
    /**
     * \brief Free Gibbs Energy derivative gamma_pi,tau(pi,tau)
     */
    units::Dimensionless pTauTau();
    /**
     * \brief Free Gibbs Energy derivative gamma_pi,tau(pi,tau)
     */
    units::Dimensionless pDeltaTau();


    //Region state properties and constants

    //! \brief Density quantity
    const units::MassDensity m_density;

    //! \brief Temperature quantity
    const units::Temperature m_temperature;

    //! \brief Critical density at triple point \ Reducing density quantity
    const units::MassDensity m_critic_density;

    //! \brief Critical temperature at triple point \ Reducing temperature quantity
    const units::Temperature m_critic_temperature;
    
    //! \brief Critical pressure at triple point
    const units::Pressure m_critic_pressure;

    //! \brief Specific gas constant of ordinary water
    const units::SpecificHeatCapacity m_R;

    //! \brief Reduced pressure value
    units::Dimensionless m_delta;

    //! \brief Inverse reduced temperature value
    units::Dimensionless m_tau;

    //! \brief Calculated basic equation properties container used to reduce multiple calculations
    std::map<std::string, units::Dimensionless> m_calculated;
};
}
/**
 * @} End of Doxygen Groups
 */
#endif                                                      // STEAMCALC_THREE_H
