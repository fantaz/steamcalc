#include "three.h"
#include "../gibbs_free_energy_index.h"

namespace steamcalc {
    Three::Three ( const units::MassDensity& _density, const units::Temperature& _temperature ) :
    m_density ( _density ),
    m_temperature ( _temperature ),
    m_critic_density ( 322*kilogram/meter/meter/meter ),
    m_critic_temperature ( 647.096*kelvin ),
    m_critic_pressure ( 22.064*mega*pascal ),
    m_R ( 0.461526*kilo*joule/kilogram/kelvin ) {
        m_delta = functions::reduce/*<units::MassDensity>*/ ( m_density, m_critic_density );
        m_tau = functions::reduce_inversed/*<units::Temperature>*/ ( m_temperature, m_critic_temperature );
    }

    units::Pressure Three::pressure() {
        return m_delta * pDelta() * m_density * m_R * m_temperature;
    }

    units::Temperature Three::temperature() {
        return m_temperature;
    }

    units::SpecificEnergy Three::specInterEnergy() {
        return m_tau * pTau() * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Three::specEntropy() {
        return ( m_tau * pTau() - p() ) * m_R;
    }

    units::SpecificEnergy Three::specEnthalpy() {
        return ( m_tau*pTau() + m_delta * pDelta() ) * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Three::specIsochoricHeatCap() {
        return ( -1 * pow ( m_tau, 2 ) * pTauTau() ) * m_R;
    }

    units::SpecificHeatCapacity Three::specIsobaricHeatCap() {
        return ( ( -1 * pow ( m_tau, 2 ) * pTauTau() ) + ( pow ( ( m_delta*pDelta() - m_delta * m_tau * pDeltaTau() ), 2 ) / ( (2 * m_delta*pDelta()) + (pow ( m_delta, 2 ) * pDeltaDelta() ) ) ) ) * m_R;
    }

    units::Velocity Three::speedOfSound() {
        return sqrt ( ( ( 2 * m_delta*pDelta() + pow ( m_delta, 2 ) * pDeltaDelta() ) - pow ( ( m_delta*pDelta() - m_delta * m_tau * pDeltaTau() ), 2 ) / ( pow ( m_tau, 2 ) * pTauTau() ) ) * m_R * m_temperature );
    }

    units::SpecificVolume Three::specVolume() {
        return 1./m_density;

    }
    
    units::MassDensity Three::density() {
        return m_density;
    }



    units::Dimensionless Three::p() {
        if ( m_calculated.find ( "p" ) ==m_calculated.end() ) {
            m_calculated["p"] =
            hfe_rthree[0][2]
            * log ( m_delta )
            + std::accumulate (
                hfe_rthree.begin() +1,
                hfe_rthree.end(),
                units::Dimensionless(),
                [&] (
                    units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2]*pow ( m_delta, item[0] ) *pow ( m_tau, item[1] );
            }
            );
        }
        return m_calculated["p"];
    }

    units::Dimensionless Three::pDelta() {
        if ( m_calculated.find ( "pD" ) == m_calculated.end() ) {
            m_calculated["pD"] =
                hfe_rthree[0][2] / m_delta
                + functions::sum(hfe_rthree,
                    [&] (
                        units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2]*item[0] *pow ( m_delta, item[0]-1 ) *pow ( m_tau, item[1] );
            }
                );
        }
        return m_calculated["pD"];
    }

    units::Dimensionless Three::pDeltaDelta() {
        if ( m_calculated.find ( "pDD" ) == m_calculated.end() ) {
            m_calculated["pDD"] =
                ( -1 ) * hfe_rthree[0][2] / pow ( m_delta, 2 )
                + functions::sum(hfe_rthree,
                    [&] (                                               // inside sum
                        units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2]*item[0] * ( item[0]-1 ) *pow ( m_delta, item[0]-2 ) *pow ( m_tau, item[1] );
            }
                );
        }
        return m_calculated["pDD"];
    }

    units::Dimensionless Three::pTau() {
        if ( m_calculated.find ( "pT" ) == m_calculated.end() ) {
            m_calculated["pT"] = functions::sum(hfe_rthree,
                                     [&] (                                               // inside sum
                                         units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2] *pow ( m_delta, item[0] ) * item[1] * pow ( m_tau, item[1]-1 );
            }
                                 );
        }
        return m_calculated["pT"];
    }

    units::Dimensionless Three::pTauTau() {
        if ( m_calculated.find ( "pTT" ) == m_calculated.end() ) {
            m_calculated["pTT"] = functions::sum(hfe_rthree,
                                      [&] (                                           // inside sum
                                          units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2] *pow ( m_delta, item[0] ) * item[1] * ( item[1] - 1) *pow ( m_tau, item[1]-2 );
            }
                                  );
        }
        return m_calculated["pTT"];
    }
    units::Dimensionless Three::pDeltaTau() {
        if ( m_calculated.find ( "pDT" ) == m_calculated.end() ) {
            m_calculated["pDT"] = functions::sum(hfe_rthree,
                                      [&] (                                           // inside sum
                                          units::Dimensionless init,
            const std::array<double,3>& item ) {
                return init += item[2] * item[0] *pow ( m_delta, item[0]-1 ) * item[1] *pow ( m_tau, item[1]-1 );
            }
                                  );
        }
        return m_calculated["pDT"];
    }

}                                                           // namespace end
