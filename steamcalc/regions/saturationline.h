#ifndef SATURATIONLINE_H
#define SATURATIONLINE_H

#include "../core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc{

/**
 * \brief Saturation line equation implementation
 */
class SaturationLine
{
public:
	/**
	 * \brief Ctor for Saturation Equation class
	 */
	SaturationLine();
	
	/**
	 * \brief This overloaded operator is basic equation for Region Four defined in IAPWS-IF97 standard
	 */
	units::Pressure operator()(const units::Temperature& _temperature_s);

	/**
	 * \brief This overloaded operator is backward equation for Region Four defined in IAPWS-IF97 standard
	 */	
	units::Temperature operator()(const units::Pressure& _pressure_s);
	
	/**
	 * \brief Basic equation for Region Four defined in IAPWS-IF97 standard
	 */
	units::Pressure basic(const units::Temperature& _temperature_s);
	
	/**
	 * \brief Backward equation for Region Four defined in IAPWS-IF97 standard
	 */	
	units::Temperature backward(const units::Pressure& _pressure_s);
	
private:	
	/**
	 * \brief Supplementary method for backward equation
	 */
	units::Dimensionless beta(const units::Pressure& _pressure);
	
	/**
	 * \brief Supplementary method for basic equation
	 */
	units::Dimensionless vartheta(const steamcalc::units::Temperature& _temperature);
	
	
	//! \brief Reducing pressure quantity
	const units::Pressure m_reducing_press;
	
	//! \brief Reducing temperature quantity
	const units::Temperature m_reducing_temp;
	
	//! \brief Indexes
	std::vector<units::Dimensionless> m_indexes;
};

	using Four = SaturationLine;
}
/*! @} End of Doxygen Groups*/

#endif // SATURATIONLINE_H
