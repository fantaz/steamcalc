#include "saturationline.h"
#include "boost/units/conversion.hpp"

namespace steamcalc {
	SaturationLine::SaturationLine() : 
		m_reducing_press(1 * mega * pascal), 
		m_reducing_temp(1 * kelvin), 
		m_indexes(
			{
				0.11670521452767e4,
				-0.72421316703206e6,
				-0.17073846940092e2,
				0.12020824702470e5,
				-0.32325550322333e7,
				0.14915108613530e2,
				-0.48232657361591e4,
				0.40511340542057e6,
				-0.23855557567849,
				0.65017534844798e3
			}
		)
	{
		//Nothing to do here
	}

	units::Dimensionless SaturationLine::beta(const units::Pressure& _pressure)
	{
		return sqrt(sqrt(functions::reduce<units::Pressure>(_pressure, m_reducing_press)));

	}
	
	units::Dimensionless SaturationLine::vartheta(const units::Temperature& _temperature)
	{
		units::Dimensionless reduced_temp = functions::reduce<units::Temperature>(_temperature, m_reducing_temp);
		return reduced_temp + m_indexes[8] / (reduced_temp - m_indexes[9]);
	}
	
	units::Temperature SaturationLine::operator()(const units::Pressure& _pressure_s)
	{
		return backward(_pressure_s);
	}

	units::Pressure SaturationLine::operator()(const units::Temperature& _temperature_s)
	{
		return basic(_temperature_s);
	}
	
	units::Pressure SaturationLine::basic(const units::Temperature& _temperature_s)
	{
		units::Dimensionless _vartheta = vartheta(_temperature_s);
		units::Dimensionless A=pow(_vartheta,2)+m_indexes[0]*_vartheta+m_indexes[1];
		units::Dimensionless B=m_indexes[2]*pow(_vartheta,2)+m_indexes[3]*_vartheta+m_indexes[4];
		units::Dimensionless C=m_indexes[5]*pow(_vartheta,2)+m_indexes[6]*_vartheta+m_indexes[7];
		units::Dimensionless tmp=2*C/(-B+sqrt((pow(B,2)-4*A*C)));
		return pow(tmp,4) * m_reducing_press;
	}
	
	units::Temperature SaturationLine::backward(const units::Pressure& _pressure_s)
	{
		units::Dimensionless _beta = beta(_pressure_s);
		units::Dimensionless E = pow(_beta,2)+m_indexes[2]*_beta+m_indexes[5];
		units::Dimensionless F = m_indexes[0]*pow(_beta,2)+m_indexes[3]*_beta+m_indexes[6];
		units::Dimensionless G = m_indexes[1]*pow(_beta,2)+m_indexes[4]*_beta+m_indexes[7];
		units::Dimensionless D = (2*G) / (- F - sqrt(pow(F,2)-4*E*G));
		units::Dimensionless tmp = pow((m_indexes[9]+D),2) - 4 * (m_indexes[8] + m_indexes[9]*D);
		return ((m_indexes[9]+ D - sqrt(tmp) ) / 2) * m_reducing_temp;
	}

}//namespace end

