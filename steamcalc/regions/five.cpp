/*
 *
 */

#include "five.h"
#include "../gibbs_free_energy_index.h"

namespace steamcalc {

    Five::Five ( const units::Pressure& _pressure, const units::Temperature& _temperature ) :
    m_pressure ( _pressure ),
    m_temperature ( _temperature ),
    m_reducing_pressure ( 1*mega*pascal ),
    m_reducing_temperature ( 1000 * kelvin ),
    m_R ( 0.461526*kilo*joules/ ( kelvin*kilogram ) ),
    m_calculated() {
        m_pi = functions::reduce ( m_pressure,  m_reducing_pressure );
        m_tau = functions::reduce_inversed ( m_temperature,  m_reducing_temperature );
    }

    units::Pressure Five::pressure() {
        return m_pressure;
    }

    units::Temperature Five::temperature() {
        return m_temperature;
    }

    units::SpecificVolume Five::specVolume() {
        return ( m_pi * ( gammaIPi() +gammaRPi() ) ) * m_R * m_temperature / m_pressure;
    }

    units::SpecificEnergy Five::specInterEnergy() {
        return ( ( m_tau * ( gammaITau() + gammaRTau() ) )- ( m_pi * ( gammaIPi() + gammaRPi() ) ) ) * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Five::specEntropy() {
        return ( ( m_tau * ( gammaITau() + gammaRTau() ) )- ( ( gammaI() + gammaR() ) ) ) * m_R;
    }

    units::SpecificEnergy Five::specEnthalpy() {
        return ( m_tau * ( gammaITau() + gammaRTau() ) ) * m_R * m_temperature;
    }

    units::SpecificHeatCapacity Five::specIsobaricHeatCap() {
        return ( ( -1 ) * pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() ) ) * m_R;
    }

    units::SpecificHeatCapacity Five::specIsochoricHeatCap() {
        units::Dimensionless first_part = ( -1 ) * pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() );
        units::Dimensionless numerator = pow ( ( 1 + m_pi * gammaRPi() - m_pi * m_tau * gammaRPiTau() ), 2 );
        units::Dimensionless denominator = 1 - pow ( m_pi, 2 ) * gammaRPiPi();
        return ( first_part - ( numerator / denominator ) ) * m_R;
    }

    units::Velocity Five::speedOfSound() 
    {
        units::Dimensionless numerator = 1 + 2 * m_pi * gammaRPi() + pow ( m_pi, 2 ) * pow ( gammaRPi(), 2 );
        units::Dimensionless denom_num = pow ( ( 1 + m_pi * gammaRPi() - m_pi * m_tau * gammaRPiTau() ), 2 );
        units::Dimensionless denom_denom = pow ( m_tau, 2 ) * ( gammaITauTau() + gammaRTauTau() );
        units::Dimensionless denominator = 1 - pow ( m_pi, 2 ) * gammaRPiPi() + denom_num / denom_denom;
        return sqrt ( ( numerator / denominator ) * m_R * m_temperature * kilogramme / joule ) * meter / seconds;
    }
    
    units::MassDensity Five::density() {
        return 1./specVolume();
    }


    units::Dimensionless Five::gammaI() {
        if ( m_calculated.find ( "gi" ) == m_calculated.end() ) {
            m_calculated["gi"] = log ( m_pi ) + std::accumulate (
                gfe_ideal_rfive.begin(),
                gfe_ideal_rfive.end(),
                units::Dimensionless(),
                [&] (
                    units::Dimensionless init,
                    const std::array<double,2>& item
            ) {
                return init+=item[1] * pow ( m_tau, item[0] );
            }
            );
        }
        return m_calculated["gi"];
    }

    units::Dimensionless Five::gammaIPi() {
        return 1/m_pi;
    }

    units::Dimensionless Five::gammaIPiPi() {
        return -1/pow ( m_pi, 2 );
    }

    units::Dimensionless Five::gammaITau() {
        if ( m_calculated.find ( "git" ) ==m_calculated.end() ) {
            m_calculated["git"] = functions::sum(gfe_ideal_rfive,
                                      [&] (
                                          units::Dimensionless init,
                                          const std::array<double,2>& item
            ) {
                return init+=item[1] * item[0] * pow ( m_tau, item[0]-1 );
            }
                                  );
        }
        return m_calculated["git"];
    }


    units::Dimensionless Five::gammaITauTau() {
        if ( m_calculated.find ( "gitt" ) ==m_calculated.end() ) {
            m_calculated["gitt"] = functions::sum(gfe_ideal_rfive,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,2>& item
            ) {
                return init+=item[1] * item[0] * ( item[0] -1 ) * pow ( m_tau, item[0]-2 );
            }
                                   );
        }
        return m_calculated["gitt"];
    }


    units::Dimensionless Five::gammaIPiTau() {
        return 0;
    }

    units::Dimensionless Five::gammaR() {
        if ( m_calculated.find ( "gr" ) ==m_calculated.end() ) {
            m_calculated["gr"] =  functions::sum(gfe_residual_rfive,
                                     [&] (
                                         units::Dimensionless init,
                                         const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *pow ( m_tau, item[1] );
            } );
        }
        return m_calculated["gr"];
    }

    units::Dimensionless Five::gammaRPi() {
        if ( m_calculated.find ( "grp" ) ==m_calculated.end() ) {
            m_calculated["grp"] =  functions::sum(gfe_residual_rfive,
                                      [&] (
                                          units::Dimensionless init,
                                          const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] *pow ( m_pi, item[0]-1 ) *pow ( m_tau, item[1] );
            } );
        }
        return m_calculated["grp"];
    }

    units::Dimensionless Five::gammaRPiPi() {
        if ( m_calculated.find ( "grpp" ) ==m_calculated.end() ) {
            m_calculated["grpp"] =  functions::sum(gfe_residual_rfive,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] * ( item[0] - 1 ) *pow ( m_pi, item[0]-2 ) *pow ( m_tau, item[1] );
            } );
        }
        return m_calculated["grpp"];
    }

    units::Dimensionless Five::gammaRTau() {
        if ( m_calculated.find ( "grt" ) ==m_calculated.end() ) {
            m_calculated["grt"] =  functions::sum(gfe_residual_rfive,
                                      [&] (
                                          units::Dimensionless init,
                                          const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *item[1] *pow ( m_tau, item[1]-1 );
            } );
        }
        return m_calculated["grt"];
    }

    units::Dimensionless Five::gammaRTauTau() {
        if ( m_calculated.find ( "grtt" ) ==m_calculated.end() ) {
            m_calculated["grtt"] = functions::sum(gfe_residual_rfive,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*pow ( m_pi, item[0] ) *item[1]* ( item[1]-1 ) *pow ( m_tau, item[1]-2 );
            } );
        }
        return m_calculated["grtt"];
    }

    units::Dimensionless Five::gammaRPiTau() {
        if ( m_calculated.find ( "grpt" ) ==m_calculated.end() ) {
            m_calculated["grpt"] = functions::sum(gfe_residual_rfive,
                                       [&] (
                                           units::Dimensionless init,
                                           const std::array<double,3>& item
            ) {
                return init += item[2]*item[0] *pow ( m_pi, item[0]-1 ) *item[1] *pow ( m_tau, item[1]-1 );
            } );
        }
        return m_calculated["grpt"];
    }


}
