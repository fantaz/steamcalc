/**
 * 
 * 
 */
#ifndef STEAMCALC_FIVE_H
#define STEAMCALC_FIVE_H
#include "../core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

  /**
   * @brief Fifth steam state
   * 
   * Calculates fifth region state according to IAWPS97
   */
  class Five
  {
  public:

    /**
    * \brief Constructor
    */
    Five(const units::Pressure& _pressure, const units::Temperature& _temperature);

//   protected:
    /**
    * \brief Get pressure value
    */
    units::Pressure pressure();

    /**
    * \brief Get temperature value
    */

    units::Temperature temperature();
    /**
    * \brief Calculates specific volume property for region five
    */
    units::SpecificVolume specVolume();
    /**
    * \brief Calculates specific internal energy property for region five
    */
    units::SpecificEnergy specInterEnergy ();

    /**
    * \brief Calculates specific entropy property for region five
    */
    units::SpecificHeatCapacity specEntropy();

    /**
    * \brief Calculates specific enthalpy property for region five
    */
    units::SpecificEnergy specEnthalpy();

    /**
    * \brief Calculates specific isobaric heat capacity property for region five
    */
    units::SpecificHeatCapacity specIsobaricHeatCap();

    /**
    * \brief Calculates specific isochoric heat capacity property for region five
    */
    units::SpecificHeatCapacity specIsochoricHeatCap();

    /**
    * \brief Calculates speed of sound property for region five
    */
    units::Velocity speedOfSound();
    
      /**
       * \brief Calculates density property for region five
       */
      units::MassDensity density();

  private:
    //Basic methods

      /**
       * \brief Free Gibbs Energy derivative residual part gamma(pi, tau)
       */
      units::Dimensionless gammaR();
      /**
       * \brief Free Gibbs Energy derivative residual part gamma_pi(pi,tau)
       */
      units::Dimensionless gammaRPi();
      /**
       * \brief Free Gibbs Energy derivative residual part gamma_pi,pi(pi,tau)
       */
      units::Dimensionless gammaRPiPi();
      /**
       * \brief Free Gibbs Energy derivative residual part gamma_tau(pi,tau)
       */
      units::Dimensionless gammaRTau();
      /**
       * \brief Free Gibbs Energy derivative residual part gamma_pi,tau(pi,tau)
       */
      units::Dimensionless gammaRTauTau();
      /**
       * \brief Free Gibbs Energy derivative residual part gamma_pi,tau(pi,tau)
       */
      units::Dimensionless gammaRPiTau();

      /**
       * \brief Free Gibbs Energy derivative ideal part gamma(pi,tau)
       */
      units::Dimensionless gammaI();
      /**
       * \brief Free Gibbs Energy derivative ideal part gamma_pi(pi,tau)
       */
      units::Dimensionless gammaIPi();
      /**
       * \brief Free Gibbs Energy derivative ideal part gamma_pi,pi(pi,tau)
       */
      units::Dimensionless gammaIPiPi();
      /**
       * \brief Free Gibbs Energy derivative ideal part gamma_tau(pi,tau)
       */
      units::Dimensionless gammaITau();
      /**
       * \brief Free Gibbs Energy derivative ideal part gamma_pi,tau(pi,tau)
       */
      units::Dimensionless gammaITauTau();
      /**
       * \brief Free Gibbs Energy derivative ideal part gamma_pi,tau(pi,tau)
       */
      units::Dimensionless gammaIPiTau();

    //Region state properties and constants

    //! \brief Pressure quantity
    const units::Pressure m_pressure;

    //! \brief Temperature quantity
    const units::Temperature m_temperature;

    //! \brief Reducing pressure quantity
    const units::Pressure m_reducing_pressure;

    //! \brief Reducing temperature quantity
    const units::Temperature m_reducing_temperature;

    //! \brief Specific gas constant of ordinary water
    const units::SpecificHeatCapacity m_R;

    //! \brief Reduced pressure value
    units::Dimensionless m_pi;

    //! \brief Inverse reduced temperature value
    units::Dimensionless m_tau;

    //! \brief Calculated basic equation properties container used to reduce multiple calculations
    std::map<std::string, units::Dimensionless> m_calculated;

  };
}
/**
 * @} End of Doxygen Groups
 */
#endif                                                      // STEAMCALC_FIVE_H
