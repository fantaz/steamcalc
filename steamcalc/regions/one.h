/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * RegionOne class header declarations for first steam state defined by IAPWS 97 standard
 */
#ifndef ONE_H
#define ONE_H
#include "../core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

	/**
	* \brief First steam state region class defined with Gibbs free energy
	*/
	class One
	{
	public:

		/**
		* \brief Constructor
		*/
		One(const units::Pressure& _pressure, const units::Temperature& _temperature);

//     protected:
		/**
		* \brief Get pressure value
		*/
		units::Pressure pressure();

		/**
		* \brief Get temperature value
		*/

		units::Temperature temperature();
		/**
		* \brief Calculates specific volume property for region one
		*/
		units::SpecificVolume specVolume();
		/**
		* \brief Calculates specific internal energy property for region one
		*/
		units::SpecificEnergy specInterEnergy ();

		/**
		* \brief Calculates specific entropy property for region one
		*/
		units::SpecificHeatCapacity specEntropy();

		/**
		* \brief Calculates specific enthalpy property for region one
		*/
		units::SpecificEnergy specEnthalpy();

		/**
		* \brief Calculates specific isobaric heat capacity property for region one
		*/
		units::SpecificHeatCapacity specIsobaricHeatCap();

		/**
		* \brief Calculates specific isochoric heat capacity property for region one
		*/
		units::SpecificHeatCapacity specIsochoricHeatCap();

		/**
		* \brief Calculates speed of sound property for region one
		*/
		units::Velocity speedOfSound();
		
        /**
        * \brief Calculates density property for region one
        */
        units::MassDensity density();
		
	private:
		//Basic methods
		
		/**
		 * \brief Free Gibbs Energy derivative gamma(pi,tau)
		 */
		units::Dimensionless gamma();
		/**
		 * \brief Free Gibbs Energy derivative gamma_pi(pi,tau)
		 */
		units::Dimensionless gammaPi();
		/**
		 * \brief Free Gibbs Energy derivative gamma_pi,pi(pi,tau)
		 */
		units::Dimensionless gammaPiPi();
		/**
		 * \brief Free Gibbs Energy derivative gamma_tau(pi,tau)
		 */
		units::Dimensionless gammaTau();
		/**
		 * \brief Free Gibbs Energy derivative gamma_pi,tau(pi,tau)
		 */
		units::Dimensionless gammaTauTau();
		/**
		 * \brief Free Gibbs Energy derivative gamma_pi,tau(pi,tau)
		 */
		units::Dimensionless gammaPiTau();
		
		//Region state properties and constants
		
		//! \brief Pressure quantity
		const units::Pressure m_pressure;
		
		//! \brief Temperature quantity
		const units::Temperature m_temperature;
		
		//! \brief Reducing pressure quantity
		const units::Pressure m_reducing_pressure;
		
		//! \brief Reducing temperature quantity
		const units::Temperature m_reducing_temperature;
		
		//! \brief Pressure at the triple point
		const units::Pressure m_pressure_t;
		
		//! \brief Temperature at the triple point
		const units::Temperature m_temperature_t;
		
		//! \brief Specific enthalpy at the tripe point 
		const units::SpecificEnergy m_spec_enthalpy_t;
		
		//! Specific Entropy at the triple point
		const units::SpecificHeatCapacity m_spec_entropy_t;
		
		//!Specific Internal Energy at the triple point
		const units::SpecificEnergy m_spec_intern_energ_t;
		
		//! \brief Specific gas constant of ordinary water
		const units::SpecificHeatCapacity m_R;
		
		//! \brief Reduced pressure value
		units::Dimensionless m_pi;
		
		//! \brief Inverse reduced temperature value
		units::Dimensionless m_tau;
		
		//! \brief Calculated basic equation properties container used to reduce multiple calculations
		std::map<std::string, units::Dimensionless> m_calculated;
	};
}
/**
 * @} End of Doxygen Groups
 */
#endif // ONE_H
