/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Region class Two header declarations for second steam state defined by IAPWS 97 standard
 */
#ifndef TWO_H
#define TWO_H
#include "../core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc{

class Two
{
public:

	/**
	* \brief Constructor
	*/
	Two(const units::Pressure& _pressure, const units::Temperature& _temperature);

// protected:
	/**
	* \brief Get pressure value
	*/
	units::Pressure pressure();

	/**
	* \brief Get temperature value
	*/

	units::Temperature temperature();
	/**
	* \brief Calculates specific volume property for region two
	*/
	units::SpecificVolume specVolume();
	/**
	* \brief Calculates specific internal energy property for region two
	*/
	units::SpecificEnergy specInterEnergy ();

	/**
	* \brief Calculates specific entropy property for region two
	*/
	units::SpecificHeatCapacity specEntropy();

	/**
	* \brief Calculates specific enthalpy property for region two
	*/
	units::SpecificEnergy specEnthalpy();

	/**
	* \brief Calculates specific isobaric heat capacity property for region two
	*/
	units::SpecificHeatCapacity specIsobaricHeatCap();

	/**
	* \brief Calculates specific isochoric heat capacity property for region two
	*/
	units::SpecificHeatCapacity specIsochoricHeatCap();

	/**
	* \brief Calculates speed of sound property for region two
	*/
	units::Velocity speedOfSound();
	
    /**
     * \brief Calculates density property for region two
     */
    units::MassDensity density();

private:
	//Basic methods

	/**
	* \brief Free Gibbs Energy derivative residual part gamma(pi, tau)
	*/
	units::Dimensionless gammaR();
	/**
	* \brief Free Gibbs Energy derivative residual part gamma_pi(pi,tau)
	*/
	units::Dimensionless gammaRPi();
	/**
	* \brief Free Gibbs Energy derivative residual part gamma_pi,pi(pi,tau)
	*/
	units::Dimensionless gammaRPiPi();
	/**
	* \brief Free Gibbs Energy derivative residual part gamma_tau(pi,tau)
	*/
	units::Dimensionless gammaRTau();
	/**
	* \brief Free Gibbs Energy derivative residual part gamma_pi,tau(pi,tau)
	*/
	units::Dimensionless gammaRTauTau();
	/**
	* \brief Free Gibbs Energy derivative residual part gamma_pi,tau(pi,tau)
	*/
	units::Dimensionless gammaRPiTau();
	
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma(pi,tau)
	 */
	units::Dimensionless gammaI();
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma_pi(pi,tau)
	 */
	units::Dimensionless gammaIPi();
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma_pi,pi(pi,tau)
	 */
	units::Dimensionless gammaIPiPi();
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma_tau(pi,tau)
	 */
	units::Dimensionless gammaITau();
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma_pi,tau(pi,tau)
	 */
	units::Dimensionless gammaITauTau();
	/**
	 * \brief Free Gibbs Energy derivative ideal part gamma_pi,tau(pi,tau)
	 */
	units::Dimensionless gammaIPiTau();

	//Region state properties and constants

	//! \brief Pressure quantity
	const units::Pressure m_pressure;

	//! \brief Temperature quantity
	const units::Temperature m_temperature;

    //! \brief Reducing pressure quantity
    const units::Pressure m_reducing_pressure;

    //! \brief Reducing temperature quantity
    const units::Temperature m_reducing_temperature;
    
    //! \brief Pressure at the triple point
    const units::Pressure m_pressure_t;
    
    //! \brief Upper metastable pressure boundary
    const units::Pressure m_pressure_metastable_line;
    
    //! \brief Specific enthalpy of saturated liquid at triple point
    const units::SpecificEnergy m_spec_enthalpy_t_liquid;
    
    //! \brief Equilibrium quantity of metastable-vapor state
    const units::Dimensionless m_x;
    
    //! Ideal coefficients table
    std::vector<std::array<double,2>> m_ideal_coefficients;
    
    //! Residual coefficients table
    std::vector<std::array<double,3>> m_residual_coefficients;

	//! \brief Specific gas constant of ordinary water
	const units::SpecificHeatCapacity m_R;

	//! \brief Reduced pressure value
	units::Dimensionless m_pi;

	//! \brief Inverse reduced temperature value
	units::Dimensionless m_tau;

	//! \brief Calculated basic equation properties container used to reduce multiple calculations
	std::map<std::string, units::Dimensionless> m_calculated;
};

}
/*! @} End of Doxygen Groups*/
#endif // TWO_H
