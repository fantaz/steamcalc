/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Backward equations helper base class used to fill constant values
 */
#ifndef STEAMCALC_BASEBACKWARDER_H
# define STEAMCALC_BASEBACKWARDER_H
# include "core.h"
# include "propertytraits.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

/**
 * \brief Base class for backward policy template classes
 */
template<typename FT,  typename ST>
class BaseBackwarder
{
protected:
    //! \brief Defining property type for first parameter of backward method
    using FirstType = typename PropertyTraits<FT>::Type;
    
    //! \brief Defining property type for second parameter of backward method
    using SecondType = typename PropertyTraits<ST>::Type;
    
    //! \brief First input value is stored, to define correct type user must use Property namespace classes
    const FirstType m_first;
    
    //! \brief Second input value is stored, to define correct type user must use Property namespace classes
    const SecondType m_second;
    
    //! \brief Pressure scalar is used to simplify initialization pressure variable
    const units::Pressure m_pressure_scalar;
    
    //! \brief Temperature scalar is used to simplify initialization temperature variable
    const units::Temperature m_temperature_scalar;
    
    //! \brief Mass density scalar is used to simplify initialization density variable
    const units::MassDensity m_density_scalar;

    //! \brief Tolerance setting for root finding method 
    boost::math::tools::eps_tolerance<double> m_tolerance;

    //! \brief Maximum iteration settings for root finding method
    boost::uintmax_t m_max_iterations;
    
public:
    /**
     * \brief Ctor for filling some of fundamental values for executing backward equations
     */
    BaseBackwarder(const FirstType& _first, const SecondType& _second);
    
    /**
     * \brief Gets tolerance
     */
    const boost::math::tools::eps_tolerance< double > getTolerance();
};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_BASEBACKWARDER_H
