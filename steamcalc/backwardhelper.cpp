#include "backwardhelper.h"
#include "regions.h"
#include "ptchecker.h"

namespace steamcalc {

    template<typename T, size_t V>
    BackwardHelper<T, V>::BackwardHelper() {

    }


    template<>
    BackwardHelper<One, 0 >::BackwardHelper() :
    m_density_critic ( 322*kilogram/ ( meter*meter*meter ) ),
    m_temp_critic ( 647.096*kelvin ),
    m_press_critic ( 22.064*mega*pascal ),
    m_pressure_reduce ( 1*mega*pascal ),
    m_temperature_reduce ( 1*kelvin ),
    m_spec_enthalpy_reduce ( 2500*kilo*joules_per_kilogram ),
    m_spec_entropy_reduce ( 1*kilo*Joule_per_kilogram_and_kelvin ) {
    }

    template<>
    BackwardHelper<Two, 0 >::BackwardHelper() :
    m_density_critic ( 322*kilogram/ ( meter*meter*meter ) ),
    m_temp_critic ( 647.096*kelvin ),
    m_press_critic ( 22.064*mega*pascal ),
    m_pressure_reduce ( 1*mega*pascal ),
    m_temperature_reduce ( 1*kelvin ),
    m_spec_enthalpy_reduce ( 2000*kilo*joules_per_kilogram ),
    m_spec_entropy_reduce ( 2*kilo*Joule_per_kilogram_and_kelvin ) {
    }

    template<>
    BackwardHelper<Two, 1 >::BackwardHelper() :
    m_density_critic ( 322*kilogram/ ( meter*meter*meter ) ),
    m_temp_critic ( 647.096*kelvin ),
    m_press_critic ( 22.064*mega*pascal ),
    m_pressure_reduce ( 1*mega*pascal ),
    m_temperature_reduce ( 1*kelvin ),
    m_spec_enthalpy_reduce ( 2000*kilo*joules_per_kilogram ),
    m_spec_entropy_reduce ( 0.7853*kilo*Joule_per_kilogram_and_kelvin ) {
    }

    template<>
    BackwardHelper<Two, 2 >::BackwardHelper() :
    m_density_critic ( 322*kilogram/ ( meter*meter*meter ) ),
    m_temp_critic ( 647.096*kelvin ),
    m_press_critic ( 22.064*mega*pascal ),
    m_pressure_reduce ( 1*mega*pascal ),
    m_temperature_reduce ( 1*kelvin ),
    m_spec_enthalpy_reduce ( 2000*kilo*joules_per_kilogram ),
    m_spec_entropy_reduce ( 2.9251*kilo*Joule_per_kilogram_and_kelvin ) {
    }

    template<typename T, size_t V>
    std::pair< units::Pressure, units::Pressure > BackwardHelper<T, V>::getBounds ( const units::Temperature& _temperature ) {
        PTChecker<T> bounds ( _temperature );
        return bounds.getPressureBounds ( _temperature );
    }

    template<typename T, size_t V>
    std::pair< units::Temperature, units::Temperature > BackwardHelper<T, V>::getBounds ( const units::Pressure& _pressure ) {
        PTChecker<T> bounds;
        return bounds.getTemperatureBounds ( _pressure );
    }

    template class BackwardHelper<One, 0u>;
    template class BackwardHelper<Two, 0u>;
    template class BackwardHelper<Three, 0u>;
    template class BackwardHelper<Five, 0u>;

    template class BackwardHelper<One, 1u>;
    template class BackwardHelper<Two, 1u>;
//     template class BackwardHelper<Three, 1u>;
//     template class BackwardHelper<Five, 1u>;

//     template class BackwardHelper<One, 2u>;
    template class BackwardHelper<Two, 2u>;
//     template class BackwardHelper<Three, 2u>;
//     template class BackwardHelper<Five, 2u>;

//     template class BackwardHelper<One, 3u>;
    template class BackwardHelper<Two, 3u>;
//     template class BackwardHelper<Three, 3u>;
//     template class BackwardHelper<Five, 3u>;

}
