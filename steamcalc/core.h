/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file is used to contain necessary internal lib headers to reduce compile time inside steamcalc lib
 */
#ifndef CORE_H
#define CORE_H

// Set steamcalc lib version if not defined
# ifndef STEAMCALC_VERSION
#   define STEAMCALC_VERSION CURRENT_STEAMCALC_VERSION
# endif /* STEAMCALC_VERSION */

//Boost lib includes
#include "boost.h"

//Standard template library includes
#include "stl.h"

//Local includes
#include "units/custom_type_definitions.h"

#include "functions.h"

#endif // CORE_H
