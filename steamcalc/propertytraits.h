/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Property type declaration
 */
#ifndef STEAMCALC_PROPERTYTYPE_H
#define STEAMCALC_PROPERTYTYPE_H
# include "core.h"
/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc{
	
/*!
 *  \addtogroup property
 *  @{
 */
namespace Property{
    /**
    * \brief Property class alias for pressure property
    */
	class Pressure;
	
    /**
    * \brief Property class alias for temperature property
    */
	class Temperature;
	
    /**
    * \brief Property class alias for specific enthalpy property
    */
	class SpecificEnthalpy;
	
    /**
    * \brief Property class alias for specific entropy property
    */
	class SpecificEntropy;
	
    /**
    * \brief Property class alias for specific volume property
    */
	class SpecificVolume;
	
    /**
    * \brief Property class alias for specific internal energy property
    */
	class SpecificInternalEnergy;
	
    /**
    * \brief Property class alias for specific isochoric heat capacity property
    */
	class SpecificIsochoricHeatCapacity;
        
    /**
 * \brief Property class alias for specific isobaric heat capacity property
    */
	class SpecificIsobaricHeatCapcity;
        
    /**
    * \brief Property class alias for speed of sound property
    */
	class SpeedOfSound;
        
    /**
    * \brief Property class alias for density property
    */
	class Density;
}
/**
 * @} End of Doxygen Groups
 */

/**
 * \brief Property traits class to match correct property to correct type
 */
template<typename T>
struct PropertyTraits;

template<>
struct PropertyTraits<Property::Pressure>{ using Type = units::Pressure;};
template<>
struct PropertyTraits<Property::Temperature>{ using Type = units::Temperature;};
template<>
struct PropertyTraits<Property::SpecificEnthalpy>{ using Type = units::SpecificEnergy;};
template<>
struct PropertyTraits<Property::SpecificEntropy>{ using Type = units::SpecificHeatCapacity;};
template<>
struct PropertyTraits<Property::SpecificInternalEnergy>{ using Type = units::SpecificEnergy;};
template<>
struct PropertyTraits<Property::SpecificVolume>{ using Type = units::SpecificVolume;};
template<>
struct PropertyTraits<Property::SpecificIsobaricHeatCapcity>{ using Type = units::SpecificHeatCapacity;};
template<>
struct PropertyTraits<Property::SpecificIsochoricHeatCapacity>{ using Type = units::SpecificHeatCapacity;};
template<>
struct PropertyTraits<Property::SpeedOfSound>{ using Type = units::Velocity;};
template<>
struct PropertyTraits<Property::Density>{ using Type = units::MassDensity;};
}
/**
 * @} End of Doxygen Groups
 */
#endif                                                      //STEAMCALC_PROPERTYTYPE_H