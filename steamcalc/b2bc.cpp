#include "b2bc.h"

namespace steamcalc {

    B2BC::B2BC() :
    m_h_r ( 1*kilo*joule / kilogram ),
    m_p_r ( 1 * mega * pascal ),
    n ( {
        0.90584278514723e3,
        -0.67955786399241,
        0.12809002730136e-3,
        0.26526571908428e4,
        0.45257578905948e1
    } ) {
    }

    units::Pressure B2BC::operator() ( const units::SpecificEnergy& _input_val ) {
        units::Dimensionless eta = functions::reduce ( _input_val, m_h_r );

        return ( n[0] + (n[1] * eta) + (n[2] * pow ( eta, 2 )) ) * m_p_r;
    }


    units::SpecificEnergy B2BC::operator() ( const units::Pressure& _input_val )  {
        units::Dimensionless pi = functions::reduce ( _input_val, m_p_r );

        return ( n[3] + sqrt ( ( pi - n[4] ) / n[2] ) ) * m_h_r;
    }


}
