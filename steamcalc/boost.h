/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file is used to contain necessary BOOST lib header includes to reduce compile time inside steamcalc lib
 */
#ifndef BOOST_H
#define BOOST_H

#include <boost/units/io.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/io.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include "boost/units/operators.hpp"
#include <boost/units/pow.hpp>
#include <boost/units/conversion.hpp>
#include "boost/units/cmath.hpp"

#endif // BOOST_H
