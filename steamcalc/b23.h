/**
 * @file b23.h
 * 
 * @brief This file contains header implementations for B23 equation defined in IAPAWS-97 standard
 * 
 */
#ifndef STEAMCALC_B23_H
#define STEAMCALC_B23_H

#include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc {

    /**
     * \brief This class represent B23 equation defined at IAPAWS-97 standard
     */
    class B23{
    public:
        /**
        * \brief Constructor needed to load values
        */
        B23();
        
        /**
         * \brief Calculate pressure boundary value
         */
        units::Pressure operator()(const units::Temperature& _input_val);
        
        /**
         * \brief Calculate temperature pressure value
         */
        units::Temperature operator()(const units::Pressure& _input_val);
        
    private:
	
        ///\brief Reducing pressure constant
        const units::Pressure m_reducing_p;
        
        ///\brief Reducing temperature constant
        const units::Temperature m_reducing_T;
        
        /// \brief Lower pressure bound
        const units::Pressure m_lower_p_bound;
        
        /// \brief Higher pressure bound
        const units::Pressure m_higer_p_bound;
        
        /// \brief Lower pressure bound
        const units::Temperature m_lower_T_bound;
        
        /// \brief Higher pressure bound
        const units::Temperature m_higer_T_bound;
        
        ///\brief B23 equation coefficients 
        const std::vector<units::Dimensionless> n;
	
    };
}
/*! @} End of Doxygen Groups*/
#endif // STEAMCALC_B23_H

