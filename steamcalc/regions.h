/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file is used to contain necessary REGIONS lib headers includes to reduce compile time inside steamcalc lib
 */
#ifndef REGIONS_H
# define REGIONS_H

# include "regions/one.h"
# include "regions/two.h"
# include "regions/three.h"
# include "regions/saturationline.h"
# include "regions/five.h"

#endif                                                      // REGIONS_H