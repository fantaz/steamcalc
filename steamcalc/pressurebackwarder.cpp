#include "pressurebackwarder.h"
#include "regions.h"
#include "ptchecker.h"
#include "densitybackwarder.h"
namespace steamcalc {

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).density().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
        ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).density().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::Density>::backward() {
        return Three ( m_second, m_first ).pressure();
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).density().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specVolume().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specVolume().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificVolume>::backward() {
        return Three ( 1./m_second, m_first ).pressure();
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specVolume().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specEnthalpy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specEnthalpy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).specEnthalpy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specEnthalpy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }


    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificEntropy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specEntropy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificEntropy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specEntropy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificEntropy>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).specEntropy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificEntropy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specEntropy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specInterEnergy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specInterEnergy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificInternalEnergy>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).specInterEnergy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specInterEnergy().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specIsobaricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specIsobaricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificIsobaricHeatCapcity>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).specIsobaricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specIsobaricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).specIsochoricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).specIsochoricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpecificIsochoricHeatCapacity>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).specIsochoricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).specIsochoricHeatCap().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<One, Property::Temperature, Property::SpeedOfSound>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( _tested * m_pressure_scalar,  m_first ).speedOfSound().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Two, Property::Temperature, Property::SpeedOfSound>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( _tested * m_pressure_scalar,  m_first ).speedOfSound().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Three, Property::Temperature, Property::SpeedOfSound>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( m_first, _tested * m_pressure_scalar );
            return m_second.value() - Three ( density.backward(),  m_first ).speedOfSound().value();
        },
        getBounds(m_first).first.value(),
        getBounds(m_first).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template<>
    units::Pressure PressureBackwarder<Five, Property::Temperature, Property::SpeedOfSound>::backward() {
        std::pair<units::Pressure,  units::Pressure> bounds = getBounds(m_first);
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( _tested * m_pressure_scalar,  m_first ).speedOfSound().value();
        },
        bounds.first.value(),
        bounds.second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_pressure_scalar;
    }

    template class PressureBackwarder<One, Property::Temperature,  Property::Density, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::Density, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::Density, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::Density, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificVolume, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificVolume, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificVolume, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificVolume, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificEnthalpy, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificEnthalpy, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificEnthalpy, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificEnthalpy, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificEntropy, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificEntropy, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificEntropy, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificEntropy, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificInternalEnergy, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificInternalEnergy, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificInternalEnergy, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificInternalEnergy, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificIsobaricHeatCapcity, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpecificIsochoricHeatCapacity, 0>;

    template class PressureBackwarder<One, Property::Temperature,  Property::SpeedOfSound, 0>;
    template class PressureBackwarder<Two, Property::Temperature,  Property::SpeedOfSound, 0>;
    template class PressureBackwarder<Three, Property::Temperature,  Property::SpeedOfSound, 0>;
    template class PressureBackwarder<Five, Property::Temperature,  Property::SpeedOfSound, 0>;

}
