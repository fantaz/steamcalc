/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file contain set of helper or wrapper functions used by steamcalc library
 */
#ifndef STEAMCALC_FUNCTIONS_FUNCTIONS_H
#define STEAMCALC_FUNCTIONS_FUNCTIONS_H
#include "units/custom_type_definitions.h"
# include "boost/math/tools/roots.hpp"

/*!
 *  \addtogroup steamcalc
 *  @{
 *
 * steamcalc interfaces and implementations
 */
namespace steamcalc {

/*!
*  \addtogroup functions
*  @{
*
* functions interfaces and implementations
*/
namespace functions {

const double err {
    1e-6
};                                                          // absolute error used for tests

using PolicySettings = boost::math::policies::policy<
                       boost::math::policies::domain_error< boost::math::policies::error_policy_type::ignore_error >
                       >;

/**
* \brief Reduce given values
*/
template<typename T>
inline units::Dimensionless reduce(const T& _val, const T& _reducing_val) {
    return _val/_reducing_val;
}

/**
* \brief Inverse reduce of given values
*/
template<typename T>
inline units::Dimensionless reduce_inversed(const T& _val, const T& _reducing_val) {
    return _reducing_val/_val;
}

/**
* \brief Get saturated vapor state value
*/
template<typename T>
inline T saturatedVapor(const T& _saturated_liquid, const units::Dimensionless& _equlibrium) {
    return _saturated_liquid/_equlibrium;
}

/**
 * \brief Root finding function
 */
template<typename F>
inline double find_root(F _f, const double& _lower_bound,  const double& _upper_bound, boost::math::tools::eps_tolerance<double>& _tolerance,  boost::uintmax_t& _max_iterations) {
    double a, b;
    if (_lower_bound>_upper_bound) {
        b = _lower_bound;
        a = _upper_bound;
    } else {
        a = _lower_bound;
        b = _upper_bound;
    }
    if (a == 0) a = err;                          //std::numeric_limits< double >::min();
    std::pair<double, double> result = boost::math::tools::toms748_solve(_f, a, b, _tolerance, _max_iterations,  PolicySettings());
    return (result.first + result.second)/2;
}

/**
 * \brief Summing function
 */
template<typename A, typename F>
inline units::Dimensionless sum(const A& _container, F _f){
  return std::accumulate( _container.begin(), _container.end(), units::Dimensionless(), _f);
}

}/*! @} End of Doxygen Groups*/
}/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_FUNCTIONS_FUNCTIONS_H
