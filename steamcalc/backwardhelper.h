/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file is used to contain backward helper information
 */
#ifndef STEAMCALC_BACKWARDHELPER_H
#define STEAMCALC_BACKWARDHELPER_H

#include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc {

/**
 * \brief Helper class to fill constant values for backward equation implementations
 */
template<typename T, size_t V = 0>
class BackwardHelper
{    
public:
    /**
     * \brief Ctor fill all constant values
     */    
    BackwardHelper();

protected:
    
    /**
    * \brief Helper method to calculate right bounds for given pair of values
    * 
    * Method uses given values to calculate and return wanted value
    * 
    * \return std::pair< Temperature, Temperature>
    */
    std::pair<units::Temperature, units::Temperature> getBounds( const units::Pressure& _pressure);
    
    /**
    * \brief Helper method to calculate right bounds for given pair of values
    * 
    * Method uses given values to calculate and return wanted value
    * 
    * \return std::pair< Pressure, Pressure>
    */
    std::pair<units::Pressure, units::Pressure> getBounds(const units::Temperature& _temperature );
    
    //! \brief Molar gas constant
    const units::MassDensity m_density_critic;

    //! \brief Critical temperature
    const units::Temperature m_temp_critic;

    //! \brief Critical pressure 
    const units::Pressure m_press_critic;

    //! \brief Reducing pressure
    const units::Pressure m_pressure_reduce;

    //! \brief Reducing temperature
    const units::Temperature m_temperature_reduce;

    //! \brief Reducing specific enthalpy
    const units::SpecificEnergy m_spec_enthalpy_reduce;

    //! \brief Reducing specific entropy
    const units::SpecificHeatCapacity m_spec_entropy_reduce;

};
}
/*! @} End of Doxygen Groups*/
#endif
