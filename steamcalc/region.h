/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Region template class header declarations
 */
#ifndef REGION_H
#define REGION_H
#include "core.h"
# include "steamproperties.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc{
       
  /**
   * \brief Region declare interface for all regions defined at IAPWS 97 standard
   * 
   * Template parameter T is one of five regions which contains specific set of formulas for every property
   */
	template<typename T>
	class Region : public T
	{
	public:
     
        /**
        * \brief Using base class constructor
        */
        using T::T;

		////////////// BASIC EQUATION WRAPPERS //////////////////////////
		/**
		* \brief Universal properties method, returns Calculated properties for all regions
		* 
		* \param _pressure input pressure
		* \param _temperature input temperature
		* 
		* \return std::unique_ptr to SteamProperties data object
		*/
		std::unique_ptr<SteamProperties> properties();
		
	private:
     
    using T::pressure;
    
    using T::temperature;
    
    using T::specVolume;
    
    using T::specInterEnergy;
    
    using T::specEntropy;
    
    using T::specEnthalpy;
    
    using T::specIsobaricHeatCap;
    
    using T::specIsochoricHeatCap;
    
    using T::speedOfSound;
    
    using T::density;

	};
     
}
/*! @} End of Doxygen Groups*/

#endif // REGION_H
