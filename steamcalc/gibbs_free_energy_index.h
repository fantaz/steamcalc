/**
 * @file
 * @author Mate <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * This file contains Gibbs free index table for calculating steam properties.
 * 
 * Pattern used to implement indexs is used as is because stl lambda functions
 * Pattern is static const std::vector<array<T1,T2>>
 */

#ifndef GIBBS_FREE_ENERGY_INDEX_H
#define GIBBS_FREE_ENERGY_INDEX_H
#include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc{

  /**
  * \brief Gibbs free energy index table for region 1
  */
  static const std::vector<std::array<double,3>> gfe_rone
  (
    {
      std::array<double,3>({0,-2, 0.14632971213167 }),
      std::array<double,3>({0,-1, -0.84548187169114 }),
      std::array<double,3>({0,0,-0.37563603672040e1 }),
      std::array<double,3>({0,1,0.33855169168385e1 }),
      std::array<double,3>({0,2,-0.95791963387872 }),
      std::array<double,3>({0,3,0.15772038513228 }),
      std::array<double,3>({0,4,-0.16616417199501e-1 }),
      std::array<double,3>({0,5,0.81214629983568e-3 }),
      std::array<double,3>({1,-9, 0.28319080123804e-3 }),
      std::array<double,3>({1,-7, -0.60706301565874e-3 }),
      std::array<double,3>({1,-1, -0.18990068218419e-1 }),
      std::array<double,3>({1,0,-0.32529748770505e-1 }),
      std::array<double,3>({1,1,-0.21841717175414e-1 }),
      std::array<double,3>({1,3,-0.52838357969930e-4 }),
      std::array<double,3>({2,-3, -0.47184321073267e-3 }),
      std::array<double,3>({2,0,-0.30001780793026e-3 }),
      std::array<double,3>({2,1,0.47661393906987e-4 }),
      std::array<double,3>({2,3,-0.44141845330846e-5 }),
      std::array<double,3>({2,17,-0.72694996297594e-15 }),
      std::array<double,3>({3,-4, -0.31679644845054e-4 }),
      std::array<double,3>({3,0,-0.28270797985312e-5 }),
      std::array<double,3>({3,6,-0.85205128120103e-9 }),
      std::array<double,3>({4,-5, -0.22425281908000e-5 }),
      std::array<double,3>({4,-2, -0.65171222895601e-6 }),
      std::array<double,3>({4,10,-0.14341729937924e-12 }),
      std::array<double,3>({5,-8, -0.40516996860117e-6 }),
      std::array<double,3>({8,-11, -0.12734301741641e-8 }),
      std::array<double,3>({8,-6, -0.17424871230634e-9 }),
      std::array<double,3>({21,-29,-0.68762131295531e-18 }),
      std::array<double,3>({23,-31,0.14478307828521e-19 }),
      std::array<double,3>({29,-38, 0.26335781662795e-22 }),
      std::array<double,3>({30,-39, -0.11947622640071e-22 }),
      std::array<double,3>({31,-40,0.18228094581404e-23 }),
      std::array<double,3>({32,-41, -0.93537087292458e-25})
    }
  );
  
  static const std::vector<std::array<double,3>> tph_one(
  {
		std::array<double,3>({0, 0, -0.23872489924521e+3}),
		std::array<double,3>({0, 1, 0.40421188637945e+3}),
		std::array<double,3>({0, 2, 0.11349746881718e+3}),
		std::array<double,3>({0, 6, -0.58457616048039e+1}),
		std::array<double,3>({0, 22, -0.15285482413140e-3}),
		std::array<double,3>({0, 32, -0.10866707695377e-5}),
		std::array<double,3>({1, 0, -0.13391744872602e+2}),
		std::array<double,3>({1, 1, 0.43211039183559e+2}),
		std::array<double,3>({1, 2, -0.54010067170506e+2}),
		std::array<double,3>({1, 3, 0.30535892203916e+2}),
		std::array<double,3>({1, 4, -0.65964749423638e+1}),
		std::array<double,3>({1, 10, 0.93965400878363e-2}),
		std::array<double,3>({1, 32, 0.11573647505340e-6}),
		std::array<double,3>({2, 10, -0.25858641282073e-4}),
		std::array<double,3>({2, 32, -0.40644363084799e-8}),
		std::array<double,3>({3, 10, 0.66456186191635e-7}),
		std::array<double,3>({3, 32, 0.80670734103027e-10}),
		std::array<double,3>({4, 32, -0.93477771213947e-12}),
		std::array<double,3>({5, 32, 0.58265442020601e-14}),
		std::array<double,3>({6, 32, -0.15020185953503e-16})
		});
  
  static const std::vector<std::array<double,3>> tps_one(
	{
		std::array<double,3>({0,0,0.17478268058307e+3}),
		std::array<double,3>({0,1,0.34806930892873e+2}),
		std::array<double,3>({0,2,0.65292584978455e+1}),
		std::array<double,3>({0,3,0.33039981775489}),
		std::array<double,3>({0,11,-0.19281382923196e-6}),
		std::array<double,3>({0,31,-0.24909197244573e-22}),
		std::array<double,3>({1,0,-0.26107636489332}),
		std::array<double,3>({1,1,0.22592965981586}),
		std::array<double,3>({1,2,-0.64256463395226e-1}),
		std::array<double,3>({1,3,0.78876289270526e-2}),
		std::array<double,3>({1,12,0.35672110607366e-9}),
		std::array<double,3>({1,31,0.17332496994895e-23}),
		std::array<double,3>({2,0,0.56608900654837e-3}),
		std::array<double,3>({2,1,-0.32635483139717e-3}),
		std::array<double,3>({2,2,0.44778286690632e-4}),
		std::array<double,3>({2,9,-0.51322156908507e-9}),
		std::array<double,3>({2,31,-0.42522657042207e-25}),
		std::array<double,3>({3,10,0.26400441360689e-12}),
		std::array<double,3>({3,32,0.78124600459723e-28}),
		std::array<double,3>({4,32,-0.30732199903668e-30})
	});
  
  
    /**
  * \brief Gibbs free energy index table for ideal-gas part for regin 2
  */
  static const std::vector<std::array<double,2>>   gfe_ideal_rtwo
  (
    {
    std::array<double,2>({ 0,  - 0.96927686500217e1}),
    std::array<double,2>({ 1,  0.10086655968018e2}),
    std::array<double,2>({ -5, -0.56087911283020e-2}),
    std::array<double,2>({ -4, 0.71452738081455e-1}),
    std::array<double,2>({ -3, -0.40710498223928 }),
    std::array<double,2>({ -2, 0.14240819171444e1}),
    std::array<double,2>({ -1, -0.43839511319450e1}),
    std::array<double,2>({ 2, -0.28408632460772 }),
    std::array<double,2>({ 3, 0.21268463753307e-1})
    }
  );

static const std::vector<std::array<double,2>>   gfe_ideal_rtwo_mstable
    (
        {
            std::array<double,2>({ 0,  -0.96937268393049e1}),
            std::array<double,2>({ 1,  0.10087275970006e2}),
            std::array<double,2>({ -5, -0.56087911283020e-2}),
            std::array<double,2>({ -4, 0.71452738081455e-1}),
            std::array<double,2>({ -3, -0.40710498223928 }),
            std::array<double,2>({ -2, 0.14240819171444e1}),
            std::array<double,2>({ -1, -0.43839511319450e1}),
            std::array<double,2>({ 2, -0.28408632460772 }),
            std::array<double,2>({ 3, 0.21268463753307e-1})
        }
    );
/**
 * \brief Index table for backward equation for subregion 2a p-h
 */
static const std::vector<std::array<double,3>>   tph_two_a
    (
        {
        std::array<double,3>({0, 0, 0.10898952318288e4}),
        std::array<double,3>({0, 1, 0.84951654495535e3}),
        std::array<double,3>({0, 2, -0.10781748091826e3}),
        std::array<double,3>({0, 3, 0.33153654801263e2}),
        std::array<double,3>({0, 7, -0.74232016790248e1}),
        std::array<double,3>({0, 20,  0.11765048724356e2}),
        std::array<double,3>({1, 0, 0.18445749355790e1}),
        std::array<double,3>({1, 1, -0.41792700549624e1}),
        std::array<double,3>({1, 2, 0.62478196935812e1}),
        std::array<double,3>({1, 3, -0.17344563108114e2}),
        std::array<double,3>({1, 7, -0.20058176862096e3}),
        std::array<double,3>({1, 9, 0.27196065473796e3}),
        std::array<double,3>({1, 11, -0.45511318285818e3}),
        std::array<double,3>({1, 18, 0.30919688604755e4}),
        std::array<double,3>({1, 44, 0.25226640357872e6}),
        std::array<double,3>({2, 0, -0.61707422868339e-2}),
        std::array<double,3>({2, 2, -0.31078046629583}),
        std::array<double,3>({2, 7, 0.11670873077107e2}),
        std::array<double,3>({2, 36, 0.12812798404046e9}),
        std::array<double,3>({2, 38, -0.98554909623276e9}),
        std::array<double,3>({2, 40, 0.28224546973002e10}),
        std::array<double,3>({2, 42, -0.35948971410703e10}),
        std::array<double,3>({2, 44, 0.17227349913197e10}),
        std::array<double,3>({3, 24, -0.13551334240775e5}),
        std::array<double,3>({3, 44, 0.12848734664650e8}),
        std::array<double,3>({4, 12, 0.13865724283226e1}),
        std::array<double,3>({4, 32, 0.23598832556514e6}),
        std::array<double,3>({4, 44, -0.13105236545054e8}),
        std::array<double,3>({5, 32, 0.73999835474766e4}),
        std::array<double,3>({5, 36, -0.55196697030060e6}),
        std::array<double,3>({5, 42, 0.37154085996233e7}),
        std::array<double,3>({6, 34, 0.19127729239660e5}),
        std::array<double,3>({6, 44, -0.41535164835634e6}),
        std::array<double,3>({7, 28, -0.62459855192507e2})
        }
    );  
  
/**
 * \brief Index table for backward equation for subregion 2b p-h
 */
static const std::vector<std::array<double,3>>   tph_two_b
    (
        {
        std::array<double,3>({0, 0,0.14895041079516e4}),
        std::array<double,3>({0,1,0.74307798314034e3}),
        std::array<double,3>({0,2,-0.97708318797837e2}),
        std::array<double,3>({0,12,0.24742464705674e1}),
        std::array<double,3>({0,18,-0.63281320016026}),
        std::array<double,3>({0,24,0.11385952129658e1}),
        std::array<double,3>({0,28,-0.47811863648625}),
        std::array<double,3>({0,40,0.85208123431544e-2}),
        std::array<double,3>({1,0,0.93747147377932}),
        std::array<double,3>({1,2,0.33593118604916e1}),
        std::array<double,3>({1,6,0.33809355601454e1}),
        std::array<double,3>({1,12,0.16844539671904}),
        std::array<double,3>({1,18,0.73875745236695}),
        std::array<double,3>({1,24,-0.47128737436186}),
        std::array<double,3>({1,28,0.15020273139707}),
        std::array<double,3>({1,40,-0.21764114219750e-2}),
        std::array<double,3>({2,2,-0.21810755324761e-1}),
        std::array<double,3>({2,8,-0.10829784403677}),
        std::array<double,3>({2,18,-0.46333324635812e-1}),
        std::array<double,3>({2,40,0.71280351959551e-4}),
        std::array<double,3>({3,1,0.11032831789999e-3}),
        std::array<double,3>({3,2,0.18955248387902e-3}),
        std::array<double,3>({3,12,0.30891541160537e-2}),
        std::array<double,3>({3,24,0.13555504554949e-2}),
        std::array<double,3>({4,2,0.28640237477456e-6}),
        std::array<double,3>({4,12,-0.10779857357512e-4}),
        std::array<double,3>({4,18,-0.76462712454814e-4}),
        std::array<double,3>({4,24,0.14052392818316e-4}),
        std::array<double,3>({4,28,-0.31083814331434e-4}),
        std::array<double,3>({4,40,-0.10302738212103e-5}),
        std::array<double,3>({5,18,0.28217281635040e-6}),
        std::array<double,3>({5,24,0.12704902271945e-5}),
        std::array<double,3>({5,40,0.73803353468292e-7}),
        std::array<double,3>({6,28,-0.11030139238909e-7}),
        std::array<double,3>({7,2,-0.81456365207833e-13}),
        std::array<double,3>({7,28,-0.25180545682962e-10}),
        std::array<double,3>({9,1,-0.17565233969407e-17}),
        std::array<double,3>({9,40,0.86934156344163e-14})
        }
    ); 

/**
 * \brief Index table for backward equation for subregion 2c p-h
 */
static const std::vector<std::array<double,3>>   tph_two_c
(
    {
        std::array<double,3>({-7, 0, -0.32368398555242e13}),
        std::array<double,3>({-7, 4, 0.73263350902181e13}),
        std::array<double,3>({-6, 0, 0.35825089945447e12}),
        std::array<double,3>({-6, 2, -0.58340131851590e12}),
        std::array<double,3>({-5, 0, -0.10783068217470e11}),
        std::array<double,3>({-5, 2, 0.20825544563171e11}),
        std::array<double,3>({-2, 0, 0.61074783564516e6}),
        std::array<double,3>({-2, 1, 0.85977722535580e6}),
        std::array<double,3>({-1, 0, -0.25745723604170e5}),
        std::array<double,3>({-1, 2, 0.31081088422714e5}),
        std::array<double,3>({0,  0, 0.12082315865936e4}),
        std::array<double,3>({0,  1, 0.48219755109255e3}),
        std::array<double,3>({1,  4, 0.37966001272486e1}),
        std::array<double,3>({1,  8, -0.10842984880077e2}),
        std::array<double,3>({2,  4, -0.45364172676660e-1}),
        std::array<double,3>({6,  0, 0.14559115658698e-12}),
        std::array<double,3>({6,  1, 0.11261597407230e-11}),
        std::array<double,3>({6,  4, -0.17804982240686e-10}),
        std::array<double,3>({6, 10, 0.12324579690832e-6}),
        std::array<double,3>({6, 12, -0.11606921130984e-5}),
        std::array<double,3>({6, 16, 0.27846367088554e-4}),
        std::array<double,3>({6, 20, -0.59270038474176e-3}),
        std::array<double,3>({6, 22, 0.12918582991878e-2})
    }
); 

/**
 * \brief Index table for backward equation for subregion 2a p-s
 */
static const std::vector<std::array<double,3>>   tps_two_a
(
{
    std::array<double,3>({-1.5,-24,-0.39235983861984e6}),
    std::array<double,3>({-1.5,-23,0.51526573827270e6}),
    std::array<double,3>({-1.5,-19,0.40482443161048e5}),
    std::array<double,3>({-1.5,-13,-0.32193790923902e3}),
    std::array<double,3>({-1.5,-11,0.96961424218694e2}),
    std::array<double,3>({-1.5,-10,-0.22867846371773e2}),
    std::array<double,3>({-1.25,-19,-0.44942914124357e6}),
    std::array<double,3>({-1.25,-15,-0.50118336020166e4}),
    std::array<double,3>({-1.25,-6,0.35684463560015}),
    std::array<double,3>({-1.0,-26,0.44235335848190e5}),
    std::array<double,3>({-1.0,-21,-0.13673388811708e5}),
    std::array<double,3>({-1.0,-17,0.42163260207864e6}),
    std::array<double,3>({-1.0,-16,0.22516925837475e5}),
    std::array<double,3>({-1.0,-9,0.47442144865646e3}),
    std::array<double,3>({-1.0,-8,-0.14931130797647e3}),
    std::array<double,3>({-0.75,-15,-0.19781126320452e6}),
    std::array<double,3>({-0.75,-14,-0.23554399470760e5}),
    std::array<double,3>({-0.5,-26,-0.19070616302076e5}),
    std::array<double,3>({-0.5,-13,0.55375669883164e5}),
    std::array<double,3>({-0.5,-9,0.38293691437363e4}),
    std::array<double,3>({-0.5,-7,-0.60391860580567e3}),
    std::array<double,3>({-0.25,-27,0.19363102620331e4}),
    std::array<double,3>({-0.25,-25,0.42660643698610e4}),
    std::array<double,3>({-0.25,-11,-0.59780638872718e4}),
    std::array<double,3>({-0.25,-6,-0.70401463926862e3}),
    std::array<double,3>({0.25,1,0.33836784107553e3}),
    std::array<double,3>({0.25,4,0.20862786635187e2}),
    std::array<double,3>({0.25,8,0.33834172656196e-1}),
    std::array<double,3>({0.25,11,-0.43124428414893e-4}),
    std::array<double,3>({0.5,0,0.16653791356412e3}),
    std::array<double,3>({0.5,1,-0.13986292055898e3}),
    std::array<double,3>({0.5,5,-0.78849547999872}),
    std::array<double,3>({0.5,6,0.72132411753872e-1}),
    std::array<double,3>({0.5,10,-0.59754839398283e-2}),
    std::array<double,3>({0.5,14,-0.12141358953904e-4}),
    std::array<double,3>({0.5,16,0.23227096733871e-6}),
    std::array<double,3>({0.75,0,-0.10538463566194e2}),
    std::array<double,3>({0.75,4,0.20718925496502e1}),
    std::array<double,3>({0.75,9,-0.72193155260427e-1}),
    std::array<double,3>({0.75,17,0.20749887081120e-6}),
    std::array<double,3>({1.0,7,-0.18340657911379e-1}),
    std::array<double,3>({1.0,18,0.29036272348696e-6}),
    std::array<double,3>({1.25,3,0.21037527893619}),
    std::array<double,3>({1.25,15,0.25681239729999e-3}),
    std::array<double,3>({1.5,5,-0.12799002933781e-1}),
    std::array<double,3>({1.5,18,-0.82198102652018e-5})
}
); 

/**
 * \brief Index table for backward equation for subregion 2b p-s
 */
static const std::vector<std::array<double,3>>   tps_two_b
(
    {
        std::array<double,3>({-6,0,0.31687665083497e6}),
        std::array<double,3>({-6,11,0.20864175881858e2}),
        std::array<double,3>({-5,0,-0.39859399803599e6}),
        std::array<double,3>({-5,11,-0.21816058518877e2}),
        std::array<double,3>({-4,0,0.22369785194242e6}),
        std::array<double,3>({-4,1,-0.27841703445817e4}),
        std::array<double,3>({-4,11,0.99207436071480e1}),
        std::array<double,3>({-3,0,-0.75197512299157e5}),
        std::array<double,3>({-3,1,0.29708605951158e4}),
        std::array<double,3>({-3,11,-0.34406878548526e1}),
        std::array<double,3>({-3,12,0.38815564249115}),
        std::array<double,3>({-2,0,0.17511295085750e5}),
        std::array<double,3>({-2,1,-0.14237112854449e4}),
        std::array<double,3>({-2,6,0.10943803364167e1}),
        std::array<double,3>({-2,10,0.89971619308495}),
        std::array<double,3>({-1,0,-0.33759740098958e4}),
        std::array<double,3>({-1,1,0.47162885818355e3}),
        std::array<double,3>({-1,5,-0.19188241993679e1}),
        std::array<double,3>({-1,8,0.41078580492196}),
        std::array<double,3>({-1,9,-0.33465378172097}),
        std::array<double,3>({0,0,0.13870034777505e4}),
        std::array<double,3>({0,1,-0.40663326195838e3}),
        std::array<double,3>({0,2,0.41727347159610e2}),
        std::array<double,3>({0,4,0.21932549434532e1}),
        std::array<double,3>({0,5,-0.10320050009077e1}),
        std::array<double,3>({0,6,0.35882943516703}),
        std::array<double,3>({0,9,0.52511453726066e-2}),
        std::array<double,3>({1,0,0.12838916450705e2}),
        std::array<double,3>({1,1,-0.28642437219381e1}),
        std::array<double,3>({1,2,0.56912683664855}),
        std::array<double,3>({1,3,-0.99962954584931e-1}),
        std::array<double,3>({1,7,-0.32632037778459e-2}),
        std::array<double,3>({1,8,0.23320922576723e-3}),
        std::array<double,3>({2,0,-0.15334809857450}),
        std::array<double,3>({2,1,0.29072288239902e-1}),
        std::array<double,3>({2,5,0.37534702741167e-3}),
        std::array<double,3>({3,0,0.17296691702411e-2}),
        std::array<double,3>({3,1,-0.38556050844504e-3}),
        std::array<double,3>({3,3,-0.35017712292608e-4}),
        std::array<double,3>({4,0,-0.14566393631492e-4}),
        std::array<double,3>({4,1,0.56420857267269e-5}),
        std::array<double,3>({5,0,0.41286150074605e-7}),
        std::array<double,3>({5,1,-0.20684671118824e-7}),
        std::array<double,3>({5,2,0.16409393674725e-8})
    }
); 

/**
 * \brief Index table for backward equation for subregion 2c p-s
 */
static const std::vector<std::array<double,3>>   tps_two_c
(
    {
        std::array<double,3>({-2,0,0.90968501005365e3}),
        std::array<double,3>({-2,1,0.24045667088420e4}),
        std::array<double,3>({-1,0,-0.59162326387130e3}),
        std::array<double,3>({0,0,0.54145404128074e3}),
        std::array<double,3>({0,1,-0.27098308411192e3}),
        std::array<double,3>({0,2,0.97976525097926e3}),
        std::array<double,3>({0,3,-0.46966772959435e3}),
        std::array<double,3>({1,0,0.14399274604723e2}),
        std::array<double,3>({1,1,-0.19104204230429e2}),
        std::array<double,3>({1,3,0.53299167111971e1}),
        std::array<double,3>({1,4,-0.21252975375934e2}),
        std::array<double,3>({2,0,-0.31147334413760}),
        std::array<double,3>({2,1,0.60334840894623}),
        std::array<double,3>({2,2,-0.42764839702509e-1}),
        std::array<double,3>({3,0,0.58185597255259e-2}),
        std::array<double,3>({3,1,-0.14597008284753e-1}),
        std::array<double,3>({3,5,0.56631175631027e-2}),
        std::array<double,3>({4,0,-0.76155864584577e-4}),
        std::array<double,3>({4,1,0.22440342919332e-3}),
        std::array<double,3>({4,4,-0.12561095013413e-4}),
        std::array<double,3>({5,0,0.63323132660934e-6}),
        std::array<double,3>({5,1,-0.20541989675375e-5}),
        std::array<double,3>({5,2,0.36405370390082e-7}),
        std::array<double,3>({6,0,-0.29759897789215e-8}),
        std::array<double,3>({6,1,0.10136618529763e-7}),
        std::array<double,3>({7,0,0.59925719692351e-11}),
        std::array<double,3>({7,1,-0.20677870105164e-10}),
        std::array<double,3>({7,3,-0.20874278181886e-10}),
        std::array<double,3>({7,4,0.10162166825089e-9}),
        std::array<double,3>({7,5,-0.16429828281347e-9})

    }
); 
  
  /**
   * \brief Gibbs free energy index table for residual part for region 2
   */
  static const std::vector<std::array<double,3>>   gfe_residual_rtwo
  (
    {
    std::array<double,3>({ 1, 0, -0.17731742473213e-2}),
    std::array<double,3>({ 1, 1, -0.17834862292358e-1}),
    std::array<double,3>({ 1, 2, -0.45996013696365e-1}),
    std::array<double,3>({ 1, 3, -0.57581259083432e-1}),
   std::array<double,3>({ 1, 6, -0.50325278727930e-1}),
    std::array<double,3>({ 2, 1, -0.33032641670203e-4}),
   std::array<double,3>({ 2, 2, -0.18948987516315e-3}),
    std::array<double,3>({ 2, 4, -0.39392777243355e-2}),
    std::array<double,3>({ 2, 7, -0.43797295650573e-1}),
    std::array<double,3>({ 2, 36, -0.26674547914087e-4}),
    std::array<double,3>({ 3, 0, 0.20481737692309e-7}),
    std::array<double,3>({ 3, 1, 0.43870667284435e-6}),
    std::array<double,3>({ 3, 3, -0.32277677238570e-4}),
    std::array<double,3>({ 3, 6, -0.15033924542148e-2}),
    std::array<double,3>({ 3, 35, -0.40668253562649e-1}),
    std::array<double,3>({ 4, 1, -0.78847309559367e-9}),
    std::array<double,3>({ 4, 2, 0.12790717852285e-7}),
    std::array<double,3>({ 4, 3, 0.48225372718507e-6}),
    std::array<double,3>({ 5, 7, 0.22922076337661e-5}),
    std::array<double,3>({ 6, 3, -0.16714766451061e-10}),
    std::array<double,3>({ 6, 16, -0.21171472321355e-2}),
    std::array<double,3>({ 6, 35, -0.23895741934104e2}),
    std::array<double,3>({ 7, 0, -0.59059564324270e-17}),
    std::array<double,3>({ 7, 11, -0.12621808899101e-5}),
    std::array<double,3>({ 7, 25, -0.38946842435739e-1}),
    std::array<double,3>({ 8, 8, 0.11256211360459e-10}),
    std::array<double,3>({ 8, 36, -0.82311340897998e1}),
    std::array<double,3>({ 9, 13, 0.19809712802088e-7}),
    std::array<double,3>({ 10, 4, 0.10406965210174e-18}),
    std::array<double,3>({ 10, 10, -0.10234747095929e-12}),
    std::array<double,3>({ 10, 14, -0.10018179379511e-8}),
    std::array<double,3>({ 16, 29, -0.80882908646985e-10}),
    std::array<double,3>({ 16, 50, 0.10693031879409}),
    std::array<double,3>({ 18, 57, -0.33662250574171}),
    std::array<double,3>({ 20, 20, 0.89185845355421e-24}),
    std::array<double,3>({ 20, 35, 0.30629316876232e-12}),
    std::array<double,3>({ 20, 48, -0.42002467698208e-5}),
    std::array<double,3>({ 21, 21, -0.59056029685639e-25}),
    std::array<double,3>({ 22, 53, 0.37826947613457e-5}),
    std::array<double,3>({ 23, 39, -0.12768608934681e-14}),
    std::array<double,3>({ 24, 26, 0.73087610595061e-28}),
    std::array<double,3>({ 24, 40, 0.55414715350778e-16}),
    std::array<double,3>({ 24, 58, -0.94369707241210e-6})
    }
  );

/**
 * \brief Gibbs free energy index table for residual part for region 2
 */
static const std::vector<std::array<double,3>>   gfe_residual_rtwo_mstable
(
    {
        std::array<double,3>({ 1, 0, -0.73362260186506e-2}),
        std::array<double,3>({ 1, 2, -0.88223831943146e-1}),
        std::array<double,3>({ 1, 5, -0.72334555213245e-1}),
        std::array<double,3>({ 1, 11, -0.40813178534455e-2}),
        std::array<double,3>({ 2, 1, 0.20097803380207e-2}),
        std::array<double,3>({ 2, 7, -0.53045921898642e-1}),
        std::array<double,3>({ 2, 16, -0.76190409086970e-2}),
        std::array<double,3>({ 3, 4, -0.63498037657313e-2}),
        std::array<double,3>({ 3, 16, -0.86043093028588e-1}),
        std::array<double,3>({ 4, 7, 0.75321581522770e-2}),
        std::array<double,3>({ 4, 10, -0.79238375446139e-2}),
        std::array<double,3>({ 5, 9, -0.22888160778447e-3}),
        std::array<double,3>({ 5, 10, -0.26456501482810e-2})
    }
);
  
    /**
   * \brief Gibbs free energy index table for ideal part for region 5
   */
static const std::vector<std::array<double,2>>   gfe_ideal_rfive
  (
    {
      std::array<double,2>({ 0, -0.13179983674201e2}),
      std::array<double,2>({ 1, 0.68540841634434e1}),
      std::array<double,2>({ -3, -0.24805148933466e-1}),
      std::array<double,2>({ -2, 0.36901534980333}),
      std::array<double,2>({ -1, -0.31161318213925e1}),
      std::array<double,2>({ 2, -0.32961626538917})
    }
  );

    /**
   * \brief Gibbs free energy index table for residual part for region 5
   */
static const std::vector<std::array<double,3>>   gfe_residual_rfive
  (
    {
      std::array<double,3>({ 1, 1, 0.15736404855259e-2}),
      std::array<double,3>({  1, 2, 0.90153761673944e-3}),
      std::array<double,3>({  1, 3, -0.50270077677648e-2}),
      std::array<double,3>({ 2, 3, 0.22440037409485e-5}),
      std::array<double,3>({ 2, 9, -0.41163275453471e-5}),
      std::array<double,3>({ 3, 7, 0.37919454822955e-7})
    }
  ); 

static const std::vector<std::array<double,3>> hfe_rthree
(
    {
        std::array<double,3>({0,0,0.10658070028513e1}), 
        std::array<double,3>({0,0,-0.15732845290239e2}), 
        std::array<double,3>({0,1,0.20944396974307e2}), 
        std::array<double,3>({0,2,-0.76867707878716e1}), 
        std::array<double,3>({0,7,0.26185947787954e1}), 
        std::array<double,3>({0,10,-0.28080781148620e1}), 
        std::array<double,3>({0,12,0.12053369696517e1}), 
        std::array<double,3>({0,23,-0.84566812812502e-2}), 
        std::array<double,3>({1,2,-0.12654315477714e1}), 
        std::array<double,3>({1,6,-0.11524407806681e1}), 
        std::array<double,3>({1,15,0.88521043984318}), 
        std::array<double,3>({1,17,-0.64207765181607}), 
        std::array<double,3>({2,0,0.38493460186671}), 
        std::array<double,3>({2,2,-0.85214708824206}), 
        std::array<double,3>({2,6,0.48972281541877e1}), 
        std::array<double,3>({2,7,-0.30502617256965e1}), 
        std::array<double,3>({2,22,0.39420536879154e-1}), 
        std::array<double,3>({2,26,0.12558408424308}), 
        std::array<double,3>({3,0,-0.27999329698710}), 
        std::array<double,3>({3,2,0.13899799569460e1}), 
        std::array<double,3>({3,4,-0.20189915023570e1}), 
        std::array<double,3>({3,16,-0.82147637173963e-2}), 
        std::array<double,3>({3,26,-0.47596035734923}), 
        std::array<double,3>({4,0,0.43984074473500e-1}), 
        std::array<double,3>({4,2,-0.44476435428739}), 
        std::array<double,3>({4,4,0.90572070719733}), 
        std::array<double,3>({4,26,0.70522450087967}), 
        std::array<double,3>({5,1,0.10770512626332}), 
        std::array<double,3>({5,3,-0.32913623258954}), 
        std::array<double,3>({5,26,-0.50871062041158}), 
        std::array<double,3>({6,0,-0.22175400873096e-1}), 
        std::array<double,3>({6,2,0.94260751665092e-1}), 
        std::array<double,3>({6,26,0.16436278447961}), 
        std::array<double,3>({7,2,-0.13503372241348e-1}), 
        std::array<double,3>({8,26,-0.14834345352472e-1}), 
        std::array<double,3>({9,2,0.57922953628084e-3}), 
        std::array<double,3>({9,26,0.32308904703711e-2}), 
        std::array<double,3>({10,0,0.80964802996215e-4}), 
        std::array<double,3>({10,1,-0.16557679795037e-3}), 
        std::array<double,3>({11,26,-0.44923899061815e-4})
    }
  );
  
}
/*! @} End of Doxygen Groups*/
#endif                                                      // GIBBS_FREE_ENERGY_INDEX_H
