/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * This file contains custom type definitions of used boost units
 */
#ifndef CUSTOMDEFINITIONS_H
#define CUSTOMDEFINITIONS_H

#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si.hpp>
#include "boost/units/quantity.hpp"
#include <boost/units/systems/si/volume.hpp>
// #include <boost/concept_check.hpp>


#include "si/volume_flow_rate.hpp"
#include "si/mass_flow_rate.hpp"
#include "si/specific_volume.hpp"
#include "si/specific_heat_capacity.hpp"
#include "si/specific_energy.hpp"

//**************used namspaceses*************************
using namespace boost::units;
using namespace boost::units::si;

/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc{
  
    /*!
     *  \addtogroup units
     *  @{
     * 
     * units interfaces and implementations  
     */
  namespace units {
    
    //! \brief Quantity dimensionless, unit none
    using Dimensionless = quantity<dimensionless>;
    
    //! \brief Quantity pressure, unit pascal/pascals
    using Pressure = quantity<pressure>;
    
    //! \brief Quantity temperature, unit kelvin
    using Temperature = quantity<temperature>;
    
    //! \brief Quantity velocity, unit meter per second
    using Velocity =quantity<velocity>;
    
    //! \brief Quantity specific energy, unit joule per kilogram
    using SpecificEnergy = quantity<specific_energy>;
    
    //! \brief Quantity specific volume, unit meter^3 per kilogram
    using SpecificVolume = quantity<specific_volume>;
    
    //! \brief Quantity specific volume, unit meter^3 per kilogram
    using SpecificHeatCapacity = quantity<specific_heat_capacity>;
    
    //! \brief Quantity mass density volume, unit kilogram per meter^3
    using MassDensity = quantity<mass_density>;
    
    //! \brief Quantity volume flow rate, unit meter^3 per second
    using VolumeFlowRate = quantity<volume_flow_rate>;
    
    //! \brief Quantity mass flow rate, unit kilogram per second
    using MassFlowRate = quantity<mass_flow_rate>;
    
  }/*! @} End of Doxygen Groups*/
}/*! @} End of Doxygen Groups*/

#endif