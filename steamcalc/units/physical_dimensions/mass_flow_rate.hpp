#ifndef BOOST_UNITS_MASS_FLOW_RATE_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_MASS_FLOW_RATE_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/time.hpp>
#include <boost/units/physical_dimensions/mass.hpp>

namespace boost {

	namespace units {

		/// derived dimension for mass density : M T^-1
		typedef derived_dimension<mass_base_dimension,1,time_base_dimension,-1>::type mass_flow_rate_dimension;            

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_MASS_FLOW_RATE_DERIVED_DIMENSION_HPP