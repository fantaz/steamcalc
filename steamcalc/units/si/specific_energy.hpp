#ifndef BOOST_UNITS_SI_SPECIFIC_ENERGY_HPP
#define BOOST_UNITS_SI_SPECIFIC_ENERGY_HPP

#include <boost/units/systems/si/base.hpp>
#include <boost/units/physical_dimensions/specific_energy.hpp>

namespace boost {

	namespace units { 

		namespace si {

			typedef unit<specific_energy_dimension,si::system>      specific_energy;

			BOOST_UNITS_STATIC_CONSTANT(Joules_per_kilogram,specific_energy); 
			BOOST_UNITS_STATIC_CONSTANT(joules_per_kilogram,specific_energy);    
			BOOST_UNITS_STATIC_CONSTANT(Joule_per_kilogram,specific_energy); 
			BOOST_UNITS_STATIC_CONSTANT(joule_per_kilogram,specific_energy); 
			BOOST_UNITS_STATIC_CONSTANT(J_kg,specific_energy);

		} // namespace si

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_SPECIFIC_ENERGY_HPP