#ifndef BOOST_UNITS_SI_VOLUME_FLOW_RATE_HPP
#define BOOST_UNITS_SI_VOLUME_FLOW_RATE_HPP

#include <boost/units/systems/si/base.hpp>
#include "../physical_dimensions/volume_flow_rate.hpp"

namespace boost {

	namespace units { 

		namespace si {

			typedef unit<volume_flow_rate_dimension,si::system>      volume_flow_rate;

			BOOST_UNITS_STATIC_CONSTANT(cubic_meter_per_second,volume_flow_rate);    
			BOOST_UNITS_STATIC_CONSTANT(cubic_meters_per_second,volume_flow_rate);   
			BOOST_UNITS_STATIC_CONSTANT(cubic_metre_per_second,volume_flow_rate);    
			BOOST_UNITS_STATIC_CONSTANT(cubic_metres_per_second,volume_flow_rate);
			BOOST_UNITS_STATIC_CONSTANT(m3_s,volume_flow_rate);

		} // namespace si

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_VOLUME_FLOW_RATE_HPP