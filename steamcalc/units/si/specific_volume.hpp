#ifndef BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP
#define BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP

#include <boost/units/systems/si/base.hpp>
#include <boost/units/physical_dimensions/volume.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/specific_volume.hpp>

namespace boost {

	namespace units { 

		namespace si {

			typedef unit<specific_volume_dimension,si::system>    specific_volume;

			BOOST_UNITS_STATIC_CONSTANT(cubic_meter_per_kilogram,specific_volume);    
			BOOST_UNITS_STATIC_CONSTANT(cubic_meters_per_kilogram,specific_volume);   
			BOOST_UNITS_STATIC_CONSTANT(cubic_metre_per_kilogram,specific_volume);    
			BOOST_UNITS_STATIC_CONSTANT(cubic_metres_per_kilogram,specific_volume);   
			BOOST_UNITS_STATIC_CONSTANT(m3_kg,specific_volume);   

		} // namespace si

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_SPECIFIC_VOLUME_HPP