#ifndef BOOST_UNITS_SI_SPECIFIC_HEAT_CAPACITY_HPP
#define BOOST_UNITS_SI_SPECIFIC_HEAT_CAPACITY_HPP

#include <boost/units/systems/si/base.hpp>
#include <boost/units/physical_dimensions/specific_heat_capacity.hpp>

namespace boost {

	namespace units { 

		namespace si {

			typedef unit<specific_heat_capacity_dimension,si::system>      specific_heat_capacity;

			BOOST_UNITS_STATIC_CONSTANT(Joules_per_kilogram_and_kelvin,specific_heat_capacity); 
			BOOST_UNITS_STATIC_CONSTANT(joules_per_kilogram_and_kelvin,specific_heat_capacity);    
			BOOST_UNITS_STATIC_CONSTANT(Joule_per_kilogram_and_kelvin,specific_heat_capacity); 
			BOOST_UNITS_STATIC_CONSTANT(joule_per_kilogram_and_kelvin,specific_heat_capacity); 
			BOOST_UNITS_STATIC_CONSTANT(J_kg_K,specific_heat_capacity);

		} // namespace si

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_SPECIFIC_HEAT_CAPACITY_HPP