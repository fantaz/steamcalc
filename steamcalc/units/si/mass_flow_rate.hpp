#ifndef BOOST_UNITS_SI_MASS_FLOW_RATE_HPP
#define BOOST_UNITS_SI_MASS_FLOW_RATE_HPP

#include <boost/units/systems/si/base.hpp>
#include "../physical_dimensions/mass_flow_rate.hpp"

namespace boost {

	namespace units { 

		namespace si {

			typedef unit<mass_flow_rate_dimension,si::system>      mass_flow_rate;

			BOOST_UNITS_STATIC_CONSTANT(kg_per_second,mass_flow_rate); 
			BOOST_UNITS_STATIC_CONSTANT(kilogram_per_second,mass_flow_rate);    
			BOOST_UNITS_STATIC_CONSTANT(kilograms_per_second,mass_flow_rate); 
			BOOST_UNITS_STATIC_CONSTANT(kilogramme_per_second,mass_flow_rate); 
			BOOST_UNITS_STATIC_CONSTANT(kilogrammes_per_second,mass_flow_rate);
			BOOST_UNITS_STATIC_CONSTANT(kg_s,mass_flow_rate);

		} // namespace si

	} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_MASS_FLOW_RATE_HPP