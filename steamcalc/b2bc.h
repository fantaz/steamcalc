/**
 * @file steamcalc.h
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * B2bc equation declarations
 */
#ifndef STEAMCALC_B2BC_H
#define STEAMCALC_B2BC_H
# include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc {

/**
 * \brief B2bc equation according to IAPWS-IF97 standard for boundary approximation between subregions 2b and 2c 
 */
class B2BC
{
public:
    /**
     * \brief Contructor needed to load values
     */
    B2BC();
    
    /**
     * \brief Calculate pressure boundary value
     */
    units::Pressure operator()(const units::SpecificEnergy& _input_val);

    /**
     * \brief Calculate temperature pressure value
     */
    units::SpecificEnergy operator()(const units::Pressure& _input_val);
    
private:
    
    /// \brief Reducing special enthalpy constant
    units::SpecificEnergy m_h_r;
    
    /// \brief Reducing pressure constant
    units::Pressure m_p_r;
    
    /// \brief B2bc equation coefficients
    std::vector<double> n;    
};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_B2BC_H
