/**
 * @file steamcalc.h
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * Steamcalc class calculates steam properties on by given values
 */
#ifndef STEAMCALC_STEAMCALC_H
#define STEAMCALC_STEAMCALC_H
#include "core.h"
# include "steamproperties.h"
# include "propertytraits.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

/**
 * \brief Steamcalc class is main interface of steamcalc library
 *
 * Use given values to calculate steam properties
 * 
 * Input value pairs are:
 * - (p, T)
 * - (rho, T)
 * - (p, h)
 * - (p, s)
 * - (u, p)
 * - (cp, p)
 * - (p, w)
 * - (p, v)
 * - (T, h)
 * - (T, s)
 * - (u, T)
 * - (cp, T)
 * - (T, w)
 * - (T, v)
 */
class Steamcalc
{  
public:

    //! Default constructor, set all values to zero
    Steamcalc();

    //! Override constuctor calculate steam properties from given pressure (p) and temperature (t)
    Steamcalc (const units::Pressure& _pressure, const units::Temperature& _temperature);

    //! Override constuctor calculate steam properties from given pressure (p) and specific enthalpy (h)
    Steamcalc (const units::Pressure& _pressure, const units::SpecificEnergy& _spec_enthalpy);

    //! Override constuctor calculate steam properties from given pressure (p) and specific entropy (s)
    Steamcalc (const units::Pressure& _pressure, const units::SpecificHeatCapacity& _spec_entropy);

    //! Override constuctor calculate steam properties from given pressure (p) and specific volume (v)
    Steamcalc (const units::Pressure& _pressure, const units::SpecificVolume& _spec_volume);
    
    //! Override constuctor calculate steam properties from given density (ro) and temperature (T)
    Steamcalc (const units::Pressure& _pressure, const units::MassDensity& _density);
    
    //! Override constuctor calculate steam properties from given pressure (p) density (ro)
    Steamcalc (const units::Pressure& _pressure, const units::Velocity& _speed_of_sound);

    //! Override constuctor calculate steam properties from given specific internal energy (u) and pressure(p)
    Steamcalc (const units::SpecificEnergy& _spec_internl_energy, const units::Pressure& _pressure);

    //! Override constuctor calculate steam properties from given specific isobaric heat capacity (cp) and pressure(p)
    Steamcalc (const units::SpecificHeatCapacity& _spec_isobaric_heat_cap, const units::Pressure& _pressure);
    
    //! Override constuctor calculate steam properties from given density (ro) and temperature (T)
    Steamcalc (const units::MassDensity& _density, const units::Temperature& _temperature);

    //! Override constuctor calculate steam properties from given temperature (t) and specific enthalpy (h)
    Steamcalc (const units::Temperature& _temperature, const units::SpecificEnergy& _spec_enthalpy);

    //! Override constuctor calculate steam properties from given temperature (t) and specific entropy (s)
    Steamcalc (const units::Temperature& _temperature, const units::SpecificHeatCapacity& _spec_entropy);

    //! Override constuctor calculate steam properties from given temperature (t) and specific volume (v)
    Steamcalc (const units::Temperature& _temperature, const units::SpecificVolume& _spec_volume);
    
    //! Override constuctor calculate steam properties from given temperature (t) and specific volume (v)
    Steamcalc (const units::Temperature& _temperature, const units::Velocity& _speed_of_sound);

    //! Override constuctor calculate steam properties from given specific internal energy (u) and temperature (t)
    Steamcalc (const units::SpecificEnergy& _spec_internl_energy, const units::Temperature& _temperature);

    //! Override constuctor calculate steam properties from given specific isobaric heat capacity (cp) and temperature (t)
    Steamcalc (const units::SpecificHeatCapacity& _spec_isobaric_heat_cap, const units::Temperature& _temperature);

    
    //GET MEMBER METHODS

    //! Get pressure value (p)
    units::Pressure pressure();

    //! Get temprature value (t)
    units::Temperature temperature() ;

    //! Get specific enthalpy value (h)
    units::SpecificEnergy specificEnthalpy();

    //! Get specific enthropy value (s)
    units::SpecificHeatCapacity specificEntropy();

    //! Get specific volume value (v)
    units::SpecificVolume specificVolume();
    
    //! Get density value (v)
    units::MassDensity density();

    //! Get specific internal energy value (u)
    units::SpecificEnergy specificInternalEnergy();

    //! Get Specific Isobaric Heat Capacity (cp)
    units::SpecificHeatCapacity specificIsobaricHeatCapacity();

    //! Get Specific Isochoric Heat Capacity (cv)
    units::SpecificHeatCapacity specificIsochoricHeatCapacity();

    //! Get Speed of sound (w)
    units::Velocity speedOfSound();
    
private:
    
    /**
     * \brief Orthogonal check
     */
    template<typename ST, typename T = typename PropertyTraits<ST>::Type>
    void check ( const units::Pressure& _pressure,  const T& _second );
    
    /**
     * \brief Vertical check
     */
    template<typename ST, typename T = typename PropertyTraits<ST>::Type>
    void check ( const units::Temperature& _temperature,  const T& _second);
    
    /**
     * \brief Recalculate region properties and check if given input values are equal
     */
    template<typename ST, typename T = typename PropertyTraits<ST>::Type>
    bool recheck(const T& _value, const units::Pressure& _pressure, const units::Temperature& _temperature);
    
    /**
     * \brief Template helper class,  get correct value of property by given traits parameter
     */
    template<typename ST, typename T = typename PropertyTraits<ST>::Type>
    struct PropertyGetter {
        /**
         * \brief Return value of property
         */
        T getProperty(const std::unique_ptr<SteamProperties>& _properties);
    };
    

    
	/**
     * \brief Create Region from given parameters
     */
    std::unique_ptr<SteamProperties>  createRegion ( const units::Pressure& _pressure, const units::Temperature& _temperature );
	
    /// \brief Properties container
    std::unique_ptr<SteamProperties> m_properties;
    
};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_STEAMCALC_H

