/**
 * @file core.h
 * @author Mate Stulina <mstulina@hotmail.com>
 *
 * @section DESCRIPTION
 *
 * This file is used to contain necessary STL header includes to reduce compile time inside steamcalc lib
 */
#ifndef STL_H
#define STL_H

#include <map>
#include <vector>
#include <memory>
#include <utility>
#include <iterator>
#include <algorithm>

#endif // STL_H
