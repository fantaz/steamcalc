#include "densitybackwarder.h"
#include "regions.h"
#include "ptchecker.h"
#include <boost/math/tools/minima.hpp>
#include "gibbs_free_energy_index.h"

namespace steamcalc {

    template<>
    std::pair< double, double > DensityBackwarder<Three, Property::Temperature, Property::Pressure, 0>::getBounds() {
        std::pair<double, double> temp;
        PTChecker<Three> bounds(m_first, m_second );
        temp.first = 1. / Two ( m_second, bounds.getTemperatureBounds().second ).specVolume().value();
        temp.second = 1. / One ( m_second, bounds.getTemperatureBounds().first ).specVolume().value();
        return temp;
    }

    template<>
    units::MassDensity  DensityBackwarder<Three, Property::Temperature, Property::Pressure, 0>::backward () {
        std::pair<double, double> bounds = getBounds();
        return functions::find_root (
        [&] ( const double& _tested ) {
            return m_second.value() - Three ( units::MassDensity ( _tested * m_density_scalar ), m_first ).pressure().value();
        },
        bounds.first,
        bounds.second,
        m_tolerance,
        m_max_iterations )
        * m_density_scalar;
    }

    template class DensityBackwarder<Three, Property::Temperature, Property::Pressure, 0>;
}
