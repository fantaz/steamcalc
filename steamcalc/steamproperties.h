/**
 * \file steamproperties.h
 * 
 * \brief This file contains data model used to distribute region property quantities during the calculations defined by IAPWS 97 
standard 
 */
#ifndef STEAMPROPERTIES_H
#define STEAMPROPERTIES_H
//internal includes
#include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc
{

/**
 * \brief SteamProperties is steam property data container
 *
 * Contains all steam properties including constants. Default constructor sets
all to zero except values that are defined by IAPWS 97' standard, used only when library produce error
 */
struct SteamProperties
{
    //! \brief Default constructor, set all values to zero
    SteamProperties ();

    //! \brief Assign steam property values
    SteamProperties ( 
        units::Pressure _pressure, 
        units::Temperature _temperature,
        units::SpecificEnergy _spec_enthalpy,
        units::SpecificHeatCapacity _spec_entropy,
        units::SpecificVolume _spec_volume, 
        units::SpecificEnergy _spec_internal_energy,
        units::SpecificHeatCapacity _spec_isobaric_heat_cap,
        units::SpecificHeatCapacity _spec_isochoric_heat_cap, 
        units::Velocity _speed_of_sound,
        units::MassDensity _density
                    );

    /* Steam properties */

    //! \brief Pressure value
    const units::Pressure m_pressure;

    //! \brief Temperature value
    const units::Temperature m_temperature;

    //! \brief Specific enthalpy value
    const units::SpecificEnergy m_spec_enthalpy;

    //! \brief Specific entropy value
    const units::SpecificHeatCapacity m_spec_entropy;

    //! \brief Specific volume
    const units::SpecificVolume m_spec_volume;

    //! \brief Specific internal energy
    const units::SpecificEnergy m_spec_internal_energy;

    //! \brief Specific Isobaric Heat Capacity (cp)
    const units::SpecificHeatCapacity m_spec_isobaric_heat_cap;

    //! \brief Specific Isochoric Heat Capacity (cv)
    const units::SpecificHeatCapacity m_spec_isochoric_heat_cap;

    //! \brief Speed of sound (w)
    const units::Velocity m_speed_of_sound;
    
    //! \brief Speed of sound (ro)
    const units::MassDensity m_density;

};

}
/*! @} End of Doxygen Groups*/

#endif // STEAMPROPERTIES_H
