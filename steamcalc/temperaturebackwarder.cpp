#include "temperaturebackwarder.h"
#include "regions.h"
#include "ptchecker.h"
#include "gibbs_free_energy_index.h"
#include "densitybackwarder.h"

namespace steamcalc {

    template<typename R, typename FT, typename ST, size_t V>
    units::Temperature TemperatureBackwarder <R, FT, ST, V>::backward() {
        return units::Temperature ( 0 );
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificEnthalpy, 0>::backward () {
        units::Dimensionless eta = functions::reduce<units::SpecificEnergy> ( m_second, m_spec_enthalpy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );

        units::Dimensionless theta = std::accumulate (
            tph_one.begin(),
            tph_one.end(),
            units::Dimensionless ( 0 ),
        [&] ( units::Dimensionless init, const std::array<double,3>& item ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( eta + 1 ), item[1] );
        }
        );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificEntropy, 0>::backward () {
        units::Dimensionless sigma = functions::reduce<units::SpecificHeatCapacity> ( m_second, m_spec_entropy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );
        units::Dimensionless theta = std::accumulate (
                                         tps_one.begin(),
                                         tps_one.end(),
                                         units::Dimensionless ( 0 ),
                                         [&] ( units::Dimensionless init, const std::array<double,3>& item
        ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( sigma + 2 ), item[1] );
        } );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEnthalpy, 0>::backward () {
        units::Dimensionless eta = functions::reduce<units::SpecificEnergy> ( m_second, m_spec_enthalpy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );

        units::Dimensionless theta = std::accumulate (
                                         tph_two_a.begin(),                                  // Set begining and end of index table
                                         tph_two_a.end(),
                                         units::Dimensionless ( 0 ),
        [&] ( units::Dimensionless init, const std::array<double,3>& item ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( eta - 2.1 ), item[1] );
        }
                                     );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEnthalpy, 1>::backward () {
        units::Dimensionless eta = functions::reduce<units::SpecificEnergy> ( m_second, m_spec_enthalpy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );

        units::Dimensionless theta = std::accumulate (
                                         tph_two_b.begin(), // Set begining and end of index table
                                         tph_two_b.end(),
                                         units::Dimensionless ( 0 ),
        [&] ( units::Dimensionless init, const std::array<double,3>& item ) {
            return init += item[2]*pow ( pi-2, item[0] ) *pow ( ( eta - 2.6 ), item[1] );
        }
                                     );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEnthalpy, 2>::backward () {
        units::Dimensionless eta = functions::reduce<units::SpecificEnergy> ( m_second, m_spec_enthalpy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );

        units::Dimensionless theta = std::accumulate (
                                         tph_two_c.begin(),                              // Set begining and end of index table
                                         tph_two_c.end(),
                                         units::Dimensionless ( 0 ),
        [&] ( units::Dimensionless init, const std::array<double,3>& item ) {
            return init += item[2]*pow ( ( pi + 25 ), item[0] ) *pow ( ( eta - 1.8 ), item[1] );
        }
                                     );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEntropy, 0>::backward () {
        units::Dimensionless sigma = functions::reduce<units::SpecificHeatCapacity> ( m_second, m_spec_entropy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );
        units::Dimensionless theta = std::accumulate (
                                         tps_two_a.begin(),
                                         tps_two_a.end(),
                                         units::Dimensionless ( 0 ),
                                         [&] ( units::Dimensionless init, const std::array<double,3>& item
        ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( sigma - 2 ), item[1] );
        } );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEntropy, 1>::backward () {
        units::Dimensionless sigma = functions::reduce<units::SpecificHeatCapacity> ( m_second, m_spec_entropy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );
        units::Dimensionless theta = std::accumulate (
                                         tps_two_b.begin(),
                                         tps_two_b.end(),
                                         units::Dimensionless ( 0 ),
                                         [&] ( units::Dimensionless init, const std::array<double,3>& item
        ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( 10 - sigma ), item[1] );
        } );
        return theta*m_temperature_reduce;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEntropy, 2>::backward () {
        units::Dimensionless sigma = functions::reduce<units::SpecificHeatCapacity> ( m_second, m_spec_entropy_reduce );
        units::Dimensionless pi = functions::reduce<units::Pressure> ( m_first, m_pressure_reduce );
        units::Dimensionless theta = std::accumulate (
                                         tps_two_c.begin(),
                                         tps_two_c.end(),
                                         units::Dimensionless ( 0 ),
                                         [&] ( units::Dimensionless init, const std::array<double,3>& item
        ) {
            return init += item[2]*pow ( pi, item[0] ) *pow ( ( 2 - sigma ), item[1] );
        } );
        return theta*m_temperature_reduce;
    }


    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).specVolume().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }


    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).density().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).density().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_first.value() - Three ( m_second, _tested * m_temperature_scalar ).pressure().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::Density>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).density().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specVolume().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Three ( ( 1./ m_second ) , ( _tested * m_temperature_scalar ) ).specVolume().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificVolume>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specVolume().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    /*
        template<>
        units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEnthalpy>::backward() {
            return functions::find_root (
            [&] ( double _tested ) {
                return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specEnthalpy().value();
            },
            getBounds ( m_first ).first.value(),
            getBounds ( m_first ).second.value(),
            m_tolerance,
            m_max_iterations
                   ) * m_temperature_scalar;
        }*/

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).specEnthalpy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificEnthalpy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specEnthalpy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }


//     template<>
//     units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificEntropy>::backward() {
//         return functions::find_root (
//         [&] ( double _tested ) {
//             return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specEntropy().value();
//         },
//         getBounds ( m_first ).first.value(),
//         getBounds ( m_first ).second.value(),
//         m_tolerance,
//         m_max_iterations
//                ) * m_temperature_scalar;
//     }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificEntropy>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).specEntropy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificEntropy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specEntropy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).specInterEnergy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specInterEnergy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificInternalEnergy>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).specInterEnergy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificInternalEnergy>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specInterEnergy().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).specIsobaricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specIsobaricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificIsobaricHeatCapcity>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).specIsobaricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificIsobaricHeatCapcity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specIsobaricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).specIsochoricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).specIsochoricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpecificIsochoricHeatCapacity>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).specIsochoricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpecificIsochoricHeatCapacity>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).specIsochoricHeatCap().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<One, Property::Pressure, Property::SpeedOfSound>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - One ( m_first, _tested * m_temperature_scalar ).speedOfSound().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Two, Property::Pressure, Property::SpeedOfSound>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Two ( m_first, _tested * m_temperature_scalar ).speedOfSound().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Three, Property::Pressure, Property::SpeedOfSound>::backward() {

        return functions::find_root (
        [&] ( double _tested ) {
            DensityBackwarder<Three, Property::Temperature, Property::Pressure> density ( _tested * m_temperature_scalar,  m_first );
            return m_second.value() - Three ( density.backward(),  _tested * m_temperature_scalar ).speedOfSound().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }

    template<>
    units::Temperature TemperatureBackwarder<Five, Property::Pressure, Property::SpeedOfSound>::backward() {
        return functions::find_root (
        [&] ( double _tested ) {
            return m_second.value() - Five ( m_first, _tested * m_temperature_scalar ).speedOfSound().value();
        },
        getBounds ( m_first ).first.value(),
        getBounds ( m_first ).second.value(),
        m_tolerance,
        m_max_iterations
               ) * m_temperature_scalar;
    }


    template class TemperatureBackwarder<One, Property::Pressure, Property::SpecificEnthalpy, 0>;
    template class TemperatureBackwarder<One, Property::Pressure, Property::SpecificEntropy, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::Density, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::Density, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::Density, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::Density, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::SpecificVolume, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificVolume, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificVolume, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificVolume, 0>;

    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificEnthalpy, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificEnthalpy, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificEnthalpy, 0>;

    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificEntropy, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificEntropy, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificEntropy, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::SpecificInternalEnergy, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificInternalEnergy, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificInternalEnergy, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificInternalEnergy, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificIsobaricHeatCapcity, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificIsobaricHeatCapcity, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpecificIsochoricHeatCapacity, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpecificIsochoricHeatCapacity, 0>;

    template class TemperatureBackwarder<One, Property::Pressure,  Property::SpeedOfSound, 0>;
    template class TemperatureBackwarder<Two, Property::Pressure,  Property::SpeedOfSound, 0>;
    template class TemperatureBackwarder<Three, Property::Pressure,  Property::SpeedOfSound, 0>;
    template class TemperatureBackwarder<Five, Property::Pressure,  Property::SpeedOfSound, 0>;

}
