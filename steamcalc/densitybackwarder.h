/**
 * @file
 * @author Mate <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * This file contains necessary methods for density backward equation
 */
#ifndef STEAMCALC_DENSITYBACKWARDER_H
# define STEAMCALC_DENSITYBACKWARDER_H
# include "core.h"
# include "basebackwarder.h"


/*!
 *  \addtogroup steamcalc
 *  @{
 * 
 * steamcalc interfaces and implementations  
 */
namespace steamcalc {

/**
 * \brief Density backward equation declaration
 * 
 * \param R - region template parameter
 * \param FT - first property type template parameter
 * \param ST - second property type template parameter
 * \param V - verison of equation template value parameter
 */
template<typename R, typename FT, typename ST, size_t V = 0>
class DensityBackwarder : public BaseBackwarder<FT, ST>
{
public:
    /**
     * \brief Using BaseBackwarder ctor
     */
    using BaseBackwarder<FT, ST>::BaseBackwarder;

    /**
     * \brief Calculates density backward equation
     */
    units::MassDensity backward();

private:
    
    /**
     * \brief Helper method to calculate right bounds for given pair of values
     */
    std::pair<double, double> getBounds();

};
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_DENSITYBACKWARDER_H
