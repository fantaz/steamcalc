/**
 * @file
 * @author Mate Stulina <mstulina@hotmail.com>
 * 
 * @section DESCRIPTION
 * 
 * Headear declarations for boundary checker class which declares basic boundaries for water steam at IAPWS 97 standard for pressure and temperature
 */
#ifndef STEAMCALC_PTCHECKER_H
#define STEAMCALC_PTCHECKER_H
#include "core.h"

/*!
 *  \addtogroup steamcalc
 *  @{
 */
namespace steamcalc {

  /**
  * \brief Define region state defined by IAPWS 97 pressure/temperature boundaries
  */
  template<typename T>
  class PTChecker
  {
  public:
    /**
     * \brief Constructor initializes all values and bounds according IAPWS97 region state pressure/temperature boundaries
	 * 
	 * Constructor is used for template specialization to instantiate correct boundaries for defined region. This Constructor is used for regions which have fixed boundaries.
     */
	
	/**
	 * \brief Constructor initializes all values and bounds according IAPWS97 region state pressure/temperature boundaries
	 * 
	 * Constructor is used for template specialization to instantiate correct boundaries for defined region. This Constructor is used for regions which have pressure bounds dependent on temperature value.
	 *
	 * \param _temperature temperature value used to define region pressure bound dependent on temperature
	 */
	
	/** 
	 * \brief Constructor initializes all values and bounds according IAPWS97 region state pressure/temperature boundaries
	 * 
	 * Constructor is used for template specialization to instantiate correct boundaries for defined region. This Constructor is used for regions which have pressure bounds dependent on temperature value.
	 *
	 * \param _temperature temperature value used to define region pressure bound dependent on temperature
	 * \param _pressure pressure value used to define region temperature bound dependent on pressure
	 */
    PTChecker(
        const units::Temperature& _temperature = units::Temperature(0), 
        const units::Pressure& _pressure = units::Pressure(0)
    );
	
	/**
	 * \brief Checks if supplied pressure is inside region bounds
	 * 
	 * \return true if the given value is inside bounds else false
	 */
	bool check(const units::Pressure& _pressure);
	
	/**
	 * \brief Checks if supplied temperature is inside region bounds
	 * 
	 * \return true if the given value is inside bounds else false
	 */
	bool check(const units::Temperature& _temperature);
	
	/**
	 * \brief Checks if supplied pressure and temperature is inside region bounds
	 * 
	 * \return bool - true if the given values are inside bounds else false
	 */
	bool check(const units::Pressure& _pressure, const units::Temperature& _temperature);
	
	/**
	 * \brief Get pressure bounds for instantiated region depending on temperature value
	 * 
	 * \return std::pair {min, max} 
	 */
	std::pair<units::Pressure,units::Pressure> getPressureBounds(const units::Temperature& _temperature = units::Temperature(0) ) const;
    
	/** 
	 * \brief Get temperature bounds for instantiated region depending on pressure value
	 * 
	 * \return std::pair {min, max} 
	 */
	std::pair<units::Temperature,units::Temperature> getTemperatureBounds(const units::Pressure& _pressure = units::Pressure(0)) const;
	
  private:
	///\brief Vector of pressure bounds pairs
	const std::vector<std::pair<units::Pressure,units::Pressure>> m_press_bounds;

	/// \brief Vector of temperature bounds pairs
	const std::vector<std::pair< units::Temperature, units::Temperature >> m_temp_bounds; 
  };
}
/*! @} End of Doxygen Groups*/

#endif // STEAMCALC_PTCHECKER_H
